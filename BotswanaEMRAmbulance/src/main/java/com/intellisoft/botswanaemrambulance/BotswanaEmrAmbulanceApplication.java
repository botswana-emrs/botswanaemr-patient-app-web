package com.intellisoft.botswanaemrambulance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotswanaEmrAmbulanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BotswanaEmrAmbulanceApplication.class, args);
    }

}
