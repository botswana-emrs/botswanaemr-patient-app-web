package com.intellisoft.botswanaemrambulance.backoffice.controller;

import com.intellisoft.botswanaemrambulance.AmbulanceIncidentStatus;
import com.intellisoft.botswanaemrambulance.DbAmbulance;
import com.intellisoft.botswanaemrambulance.FormatterClass;
import com.intellisoft.botswanaemrambulance.Results;
import com.intellisoft.botswanaemrambulance.backoffice.service_impl.impl.BackofficeAmbulanceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@RequestMapping(value = "/api/v1/backoffice")
@RestController
public class BackofficeAmbulanceController {

    @Autowired
    private BackofficeAmbulanceServiceImpl backofficeAmbulanceService;

    private FormatterClass formatterClass = new FormatterClass();

    //Add Ambulance
    @PostMapping(path = "/create-ambulance")
    public ResponseEntity<?> addAmbulance(@RequestBody DbAmbulance dbAmbulance) {

        Results results = backofficeAmbulanceService.addAmbulance(dbAmbulance);
        return formatterClass.getResponse(results);

    }

    //Get Ambulance
    @GetMapping(value = "/{ambulanceId}")
    public ResponseEntity<?> getAmbulance(@PathVariable("ambulanceId") String ambulanceId){

        Results results = backofficeAmbulanceService.getAmbulance(ambulanceId);
        return formatterClass.getResponse(results);

    }

    //Get All Ambulances
    @GetMapping(value = "/{pageNo}/{itemNo}")
    public ResponseEntity<?> getPagedAmbulances(
            @PathVariable("pageNo") int pageNo,
            @PathVariable("itemNo") int itemNo,
            @Param("ambulanceStatus") String ambulanceStatus
    ){
        String status = "";
        if (ambulanceStatus != null){

            if (ambulanceStatus.equals("Available")){
                status = AmbulanceIncidentStatus.AVAILABLE.name();
            }
            if (ambulanceStatus.equals("Dispatched")){
                status = AmbulanceIncidentStatus.DISPATCHED.name();
            }
            if (ambulanceStatus.equals("Offline")){
                status = AmbulanceIncidentStatus.OFFLINE.name();
            }

        }else {
            status = AmbulanceIncidentStatus.AVAILABLE.name();
        }

        Results results = backofficeAmbulanceService.getAmbulances(pageNo,itemNo, status);
        return formatterClass.getResponse(results);

    }

    //Update Ambulance
    @PutMapping(value = "/update")
    public ResponseEntity<?> updateAmbulanceDetails(
            @RequestBody DbAmbulance dbAmbulance
    ){

        Results results = backofficeAmbulanceService.updateAmbulanceDetails(dbAmbulance);
        return formatterClass.getResponse(results);

    }

}
