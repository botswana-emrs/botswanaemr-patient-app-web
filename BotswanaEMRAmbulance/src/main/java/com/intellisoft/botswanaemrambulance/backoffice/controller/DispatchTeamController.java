package com.intellisoft.botswanaemrambulance.backoffice.controller;

import com.intellisoft.botswanaemrambulance.FormatterClass;
import com.intellisoft.botswanaemrambulance.Results;
import com.intellisoft.botswanaemrambulance.backoffice.service_impl.impl.DispatchTeamServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/api/v1/backoffice")
@RestController
public class DispatchTeamController {

    @Autowired
    private DispatchTeamServiceImpl dispatchTeamService;
    private FormatterClass formatterClass = new FormatterClass();

    //Update dispatchTeam from user
    @RequestMapping(value = "/update-dispatch/{dispatchTeamId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUserToDispatchTeam(@PathVariable("dispatchTeamId") String dispatchTeamId) {

        Results results = dispatchTeamService.updateUserToDispatchTeam(dispatchTeamId);
        return formatterClass.getResponse(results);

    }

    //Get dispatchTeam List
    @RequestMapping(value = "/dispatch/{pageNo}/{itemNo}", method = RequestMethod.GET)
    public ResponseEntity<?> getDispatchTeamsList(
            @PathVariable("pageNo") int pageNo,
            @PathVariable("itemNo") int itemNo) {

        Results results = dispatchTeamService.getDispatchTeamsList(pageNo, itemNo, "", "");
        return formatterClass.getResponse(results);

    }

    //Get dispatchTeam Details
    @RequestMapping(value = "/dispatch/{dispatchTeamId}", method = RequestMethod.GET)
    public ResponseEntity<?> getDispatchTeamDetails(@PathVariable("dispatchTeamId") String dispatchTeamId) {

        Results results = dispatchTeamService.getDispatchTeam(dispatchTeamId);
        return formatterClass.getResponse(results);

    }

    //Assign Driver to ambulance
    @RequestMapping(value = "/assign-driver/{ambulanceId}/{driverId}", method = RequestMethod.POST)
    public ResponseEntity<?> assignDriverToAmbulance(
            @PathVariable("ambulanceId") String ambulanceId,
            @PathVariable("driverId") String driverId) {

        Results results = dispatchTeamService.assignAmbulanceDriver(ambulanceId, driverId);
        return formatterClass.getResponse(results);

    }


}
