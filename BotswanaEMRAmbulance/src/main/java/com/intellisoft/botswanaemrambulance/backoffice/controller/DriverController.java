package com.intellisoft.botswanaemrambulance.backoffice.controller;

import com.intellisoft.botswanaemrambulance.AmbulanceIncidentStatus;
import com.intellisoft.botswanaemrambulance.DriverStatus;
import com.intellisoft.botswanaemrambulance.FormatterClass;
import com.intellisoft.botswanaemrambulance.Results;
import com.intellisoft.botswanaemrambulance.backoffice.entity.DriverDetails;
import com.intellisoft.botswanaemrambulance.backoffice.service_impl.impl.DriverServiceImpl;
import com.intellisoft.botswanaemrambulance.backoffice.service_impl.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/api/v1/backoffice")
@RestController
public class DriverController {

    @Autowired
    private DriverServiceImpl driverService;
    private FormatterClass formatterClass = new FormatterClass();

    //Search user from acl using email address
    @RequestMapping(value = "/search-user", method = RequestMethod.GET)
    public ResponseEntity<?> addDriver(
            @Param("emailAddress") String emailAddress
    ) {

        Results results = driverService.getUsers(emailAddress);
        return formatterClass.getResponse(results);

    }

    //Update Driver from user
    @RequestMapping(value = "/update-driver/{driverId}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUserToDriver(@PathVariable("driverId") String driverId) {

        Results results = driverService.updateUserToDriver(driverId);
        return formatterClass.getResponse(results);

    }

    //Get Driver List
    @RequestMapping(value = "/drivers/{pageNo}/{itemNo}", method = RequestMethod.GET)
    public ResponseEntity<?> getDriversList(
            @PathVariable("pageNo") int pageNo,
            @PathVariable("itemNo") int itemNo,
            @Param("driverStatus") String driverStatus
    ) {

        boolean hasAmbulance = false;
        if (driverStatus != null){

            if (driverStatus.equals("Assigned")){
                hasAmbulance = true;
            }

        }

        Results results = driverService.getDriversList(pageNo, itemNo, "hasAmbulance", hasAmbulance);
        return formatterClass.getResponse(results);

    }

    //Get Driver Details
    @RequestMapping(value = "/driver/{driverId}", method = RequestMethod.GET)
    public ResponseEntity<?> getDriverDetails(@PathVariable("driverId") String driverId) {

        Results results = driverService.getDriver(driverId);
        return formatterClass.getResponse(results);

    }



}
