package com.intellisoft.botswanaemrambulance.incident.controller;

import com.intellisoft.botswanaemrambulance.*;
import com.intellisoft.botswanaemrambulance.incident.service_impl.impl.IncidentRequestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/api/v1/incident-request")
@RestController
public class IncidentRequestController {

    @Autowired
    private IncidentRequestServiceImpl incidentRequestService;

    private FormatterClass formatterClass = new FormatterClass();

    @PostMapping(path = "/create")
    public ResponseEntity<?> addAmbulance(@RequestBody DbIncidentRequest dbIncidentRequest) {

        Results results = incidentRequestService.createIncidentRequest(dbIncidentRequest);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/{incidentId}")
    public ResponseEntity<?> getAmbulance(@PathVariable("incidentId") String incidentId){

        Results results = incidentRequestService.getIncidentDetails(incidentId);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/{pageNo}/{itemNo}")
    public ResponseEntity<?> getPagedAmbulances(
            @PathVariable("pageNo") int pageNo,
            @PathVariable("itemNo") int itemNo,
            @Param("incidentStatus") String incidentStatus
    ){

        Results results = incidentRequestService.getIncidents(pageNo,itemNo, incidentStatus);
        return formatterClass.getResponse(results);

    }

    //Assign Ambulance to Incident
    @PostMapping(value = "/approve-request")
    public ResponseEntity<?> assignAmbulance(
            @RequestBody DbAcceptRequest dbAcceptRequest
    ){

    	Results results = incidentRequestService.assignAmbulanceIncident(dbAcceptRequest);
        return formatterClass.getResponse(results);
    }

    //Reject Ambulance Request
    @PostMapping(value = "/reject-request")
    public ResponseEntity<?> rejectAmbulance(
            @RequestBody DbRejectRequest dbRejectRequest
    ){

    	Results results = incidentRequestService.rejectAmbulanceIncident(dbRejectRequest);
        return formatterClass.getResponse(results);
    }



}
