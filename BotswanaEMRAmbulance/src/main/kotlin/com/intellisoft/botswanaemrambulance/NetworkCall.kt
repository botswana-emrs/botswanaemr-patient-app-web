package com.intellisoft.botswanaemrambulance

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate


@Component
class NetworkCall() {

    @Autowired
    lateinit var appProperties: AppProperties

    var restTemplate = RestTemplate()


    fun getUserDetailsEmailAddress(emailAddress: String) : PatientDetails?{

        val authenticationUrl = appProperties.authenticationUrl
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val url = "/service/authentication/user/search/$emailAddress"
        val baseUrl = authenticationUrl + url

        return try {

            val results = restTemplate.getForEntity(baseUrl, PatientDetails::class.java, 1)
            if (results.statusCode == HttpStatus.OK){
                println("----- ${results.body}")
                results.body
            }else{
                null
            }

        }catch (e: Exception){
            println("----- ${e}")

            null
        }



    }

    fun getUserDetailsKeyCloak(keycloakId: String) : PatientDetails?{

        val notificationUrl = appProperties.authenticationUrl
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val url = "/service/authentication/user/$keycloakId"

        return try {

            val results = restTemplate.getForEntity("$notificationUrl$url", PatientDetails::class.java, 1)
            if (results.statusCode == HttpStatus.OK){
                println("----- ${results.body}")
                results.body
            }else{
                null
            }

        }catch (e: Exception){
            null
        }



    }

    fun updateRole(dbUserRole: DbUserRole):Any?{

        val authenticationUrl = appProperties.authenticationUrl
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val url = "/service/authentication/user/update-role"

        val baseUrl = authenticationUrl + url

        return try {

            val results = restTemplate.postForEntity(baseUrl, dbUserRole, Any::class.java)
            if (results.statusCode == HttpStatus.OK){
                println("----- ${results.body}")
                results.body
            }else{
                println("----- ${results.statusCode}")
                null
            }

        }catch (e: Exception){
            println("----- $e")
            null
        }

    }

}