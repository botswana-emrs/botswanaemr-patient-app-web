package com.intellisoft.botswanaemrappointments.controller;

import com.intellisoft.botswanaemrappointments.*;
import com.intellisoft.botswanaemrappointments.retrofit.NetworkRequestServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.Call;

import java.io.IOException;
import java.util.Optional;

@RequestMapping(value = "/api/v1")
@RestController
public class AppointmentsController {
    
    FormatterClass formatterClass = new FormatterClass();

    @Autowired
    private NetworkRequestServiceImpl networkRequestService;

    @PostMapping(path = "/create-patient")
    public ResponseEntity<?> createPatient(@RequestBody DbUserData dbUserData) {

        Results results = networkRequestService.createPatient(dbUserData);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/providers")
    public ResponseEntity<?> getAllProviders(
            @Param("q") String q
    ){

        Results results = networkRequestService.getFacilityProviders(q);
        return formatterClass.getResponse(results);
    }
    @GetMapping(value = "/locations")
    public ResponseEntity<?> getLocations(){

        Results results = networkRequestService.getFacilityLocations();
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/services")
    public ResponseEntity<?> getFacilityServices(){

        Results results = networkRequestService.getFacilityServices();
        return formatterClass.getResponse(results);

    }
    @GetMapping(value = "/timeslots")
    public ResponseEntity<?> getTimeSlots(
            @Param("limit") String limit
    ){

        Results results = networkRequestService.getTimeSlots(limit);
        return formatterClass.getResponse(results);

    }
    @GetMapping(value = "/timeslots/{timeslotId}")
    public ResponseEntity<?> getTimeSlotDetails(
            @PathVariable("timeslotId") String timeslotId
    ){

        Results results = networkRequestService.getTimeSlotDetails(timeslotId);
        return formatterClass.getResponse(results);

    }
    @GetMapping(value = "/types")
    public ResponseEntity<?> getAppointmentTypes(
            @Param("limit") String limit
    ){

        Results results = networkRequestService.getAppointmentTypes(limit);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/patient")
    public ResponseEntity<?> getPatientAppointments(
            @Param("fromDate") String fromDate,
            @Param("toDate") String toDate

    ){

        Optional<String> userId = formatterClass.getCurrentUserLogin();
        Results results;
        if (userId.isPresent()){
            results = networkRequestService.getPatientAppointments(
                    fromDate,
                    userId.get(),
                    toDate);
        }else {
            results = new Results(400, "We could not find the user");
        }


        return formatterClass.getResponse(results);

    }

    @PostMapping(path = "/schedule")
    public ResponseEntity<?> scheduleNewAppointment(@RequestBody DbScheduleNewAppointment scheduleNewAppointment) {

        Optional<String> userId = formatterClass.getCurrentUserLogin();
        Results results;
        if (userId.isPresent()){
            results = networkRequestService.scheduleNewAppointment(userId.get(),scheduleNewAppointment);

        }else {
            results = new Results(400, "We could not find the user");
        }
        return formatterClass.getResponse(results);

    }


}
