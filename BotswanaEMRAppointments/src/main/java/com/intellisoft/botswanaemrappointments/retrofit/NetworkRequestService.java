package com.intellisoft.botswanaemrappointments.retrofit;

import com.intellisoft.botswanaemrappointments.DbCreateAppointmentBlock;
import com.intellisoft.botswanaemrappointments.DbScheduleNewAppointment;
import com.intellisoft.botswanaemrappointments.DbUserData;
import com.intellisoft.botswanaemrappointments.Results;
import retrofit2.Call;
import retrofit2.http.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

public interface NetworkRequestService {

    Results createPatient(DbUserData dbUserData);
    Results getFacilityProviders(String provider);
    Results getFacilityLocations();
    Results getFacilityServices();
    Results getTimeSlots(String limit);
    Results getTimeSlotDetails(String timeslotId);
    Results getAppointmentTypes(String limit);
    Results getPatientAppointments(String fromDate, String keycloakId, String toDate);
    Results scheduleNewAppointment(String keycloakId, DbScheduleNewAppointment dbScheduleNewAppointment);


}
