package com.intellisoft.botswanaemrappointments.retrofit;


import com.intellisoft.botswanaemrappointments.*;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
public class NetworkRequestServiceImpl implements NetworkRequestService{

    @Autowired
    NetworkCall networkCall = new NetworkCall();

    private FormatterClass formatterClass = new FormatterClass();

    @Override
    public Results createPatient(DbUserData dbUserData) {
        //The provided id is the keycloak id from the headers, so get user details from auth service
        String keycloakId = dbUserData.getKeycloakId();
        UserDetails userDetails = getUserDetails(keycloakId);
        if (userDetails != null){

            DbPatientDetails dbPatientDetails = new DbPatientDetails(
                    "Regular",
                    "","",
                    "", "","",
                    userDetails.getGender(), "",
                    userDetails.getEmailAddress(),
                    userDetails.getPhoneNumber(),
                    dbUserData.getOccupation(),
                    dbUserData.getEmployerName(),
                    dbUserData.getHomeaddress(),
                    dbUserData.getPatientFacilityLocation(),
                    dbUserData.getNok(),
                    dbUserData.getRelationship());

            return networkCall.createPatient(dbPatientDetails);

        }else {
            return new Results(400, "We could not locate the authenticated user.");
        }

    }


    @Override
    public Results getFacilityProviders(String provider) {

        String providerDetails = "";

        //Check if provider is not null
        if (provider != null){

            if (!provider.equals("")){
                providerDetails = provider;
            }

        }

        return networkCall.getFacilityProviders(providerDetails);


    }

    @Override
    public Results getFacilityLocations() {
        return networkCall.getFacilityLocations();
    }

    @Override
    public Results getFacilityServices() {
        return networkCall.getFacilityServices();
    }

    @Override
    public Results getTimeSlots(String limit) {

        String limitValue = "";
        if (limit == null){
            limitValue = "10";
        }else {
            limitValue = limit;
        }

        return networkCall.getTimeSlots(limitValue);
    }

    @Override
    public Results getTimeSlotDetails(String timeslotId) {
        return networkCall.getTimeSlotDetails(timeslotId);
    }

    @Override
    public Results getAppointmentTypes(String limit) {

        String limitValue = "";
        limitValue = Objects.requireNonNullElse(limit, "10");
        return networkCall.getAppointmentTypes(limitValue);
    }

    @Override
    public Results getPatientAppointments(String fromDate, String keycloakId, String toDate) {

        String error = "";
        if (fromDate == null){
            error = error + "\n" + "Please provide a start date";
            return new Results(400, error);
        }

        UserDetails userDetails = getUserDetails(keycloakId);
        System.out.println("----- "+userDetails);
        if (userDetails != null){

            boolean isPatient = userDetails.getPatient();
            String openMrsId = userDetails.getOpenMrsId();
            if (isPatient && openMrsId !=null){
                return networkCall.getPatientAppointments(fromDate, openMrsId, toDate);
            }else {
                return new Results(400, "Make sure the user is a patient.");
            }

        }else {
            return new Results(400, "User could not be found.");
        }


    }

    @Override
    public Results scheduleNewAppointment(String keycloakId, DbScheduleNewAppointment dbScheduleNewAppointment) {

        UserDetails userDetails = getUserDetails(keycloakId);
        if (userDetails != null){
            String openMrsId = userDetails.getOpenMrsId();
            if (openMrsId != null){

                DbScheduleNewAppointment scheduleNewAppointment = new DbScheduleNewAppointment(
                        dbScheduleNewAppointment.getAppointmentType(),
                        dbScheduleNewAppointment.getReason(),
                        dbScheduleNewAppointment.getStatus(),
                        dbScheduleNewAppointment.getTimeSlot(),
                        openMrsId);

                return networkCall.scheduleNewAppointment(scheduleNewAppointment);


            }else {
                return new Results(400, "Make sure the user is a patient.");
            }


        }else {
            return new Results(400, "User details not found.");
        }

    }

    private UserDetails getUserDetails(String keycloakId){
        return networkCall.getUserDetails(keycloakId);
    }


}
