//package com.intellisoft.botswanaemrappointments.service;
//
//import com.intellisoft.botswanaemrappointments.DbCreateAppointment;
//import com.intellisoft.botswanaemrappointments.DbCreateAppointmentBlock;
//import com.intellisoft.botswanaemrappointments.DbRequestQueryParams;
//import com.intellisoft.botswanaemrappointments.Results;
//
//public interface AppointmentService {
//
//    Results getLocations();
//    Results getAppointmentTypes();
//    Results getProviders();
//    Results getAppointmentBlocks(DbRequestQueryParams dbRequestQueryParams);
//    Results createAppointmentBlock(DbCreateAppointmentBlock dbCreateAppointmentBlock);
//
//    Results createAppointment(DbCreateAppointment dbCreateAppointment);
//}
