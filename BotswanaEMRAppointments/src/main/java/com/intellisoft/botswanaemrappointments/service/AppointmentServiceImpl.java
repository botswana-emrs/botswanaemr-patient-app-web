//package com.intellisoft.botswanaemrappointments.service;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.intellisoft.botswanaemrappointments.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//
//@Service
//public class AppointmentServiceImpl implements AppointmentService{
//
//    @Autowired
//    NetworkCall networkCall = new NetworkCall();
//
//    String allLocations = new Appointment().getAllLocations();
//    String allAppointment = new Appointment().getAppointmentTypes();
//    String provider = new Appointment().getProvider();
//    String appointmentBlock = new Appointment().getAppointmentBlock();
//
//    private Gson gson = new Gson();
//
//    @Override
//    public Results getLocations() {
//
//        Results results = networkCall.callAppointment(allLocations);
//        int code = results.getCode();
//        String resultsString = String.valueOf(results.getDetails());
//
//        List<DbResults> dbResultsList = new ArrayList<>();
//
//        Gson gson = new Gson();
//        DbLocations resultsData = gson.fromJson(resultsString, DbLocations.class);
//        List<DbResults> resultsList = resultsData.getResults();
//        for (int i = 0; i < resultsList.size(); i++){
//
//            String uuid = resultsList.get(i).getUuid();
//            String locationName = resultsList.get(i).getDisplay();
//
//            DbResults dbResults = new DbResults(uuid, locationName);
//            dbResultsList.add(dbResults);
//
//        }
//
//        if (code == 200){
//
//            return new Results(200, dbResultsList);
//
//        }else {
//            return results;
//        }
//
//    }
//
//    @Override
//    public Results getAppointmentTypes() {
//
//        DbQueryParams dbQueryParams = new DbQueryParams("v", "default");
//        List<DbQueryParams> dbQueryParamsList = new ArrayList<>();
//        dbQueryParamsList.add(dbQueryParams);
//
//        Results results = networkCall.callAppointmentWithQuery(allAppointment, dbQueryParamsList);
//        int code = results.getCode();
//        String resultsString = String.valueOf(results.getDetails());
//
//        List<DbAppointmentTypesResults> dbAppointmentTypesResultsList = new ArrayList<>();
//
//        Gson gson = new Gson();
//        DbAppointmentTypes dbAppointmentTypes = gson.fromJson(resultsString, DbAppointmentTypes.class);
//        List<DbAppointmentTypesResults> dbAppointmentTypesList =  dbAppointmentTypes.getResults();
//        for (int i = 0; i < dbAppointmentTypesList.size(); i++){
//
//            String uuid = dbAppointmentTypesList.get(i).getUuid();
//            String name = dbAppointmentTypesList.get(i).getName();
//            int duration = dbAppointmentTypesList.get(i).getDuration();
//
//            DbAppointmentTypesResults dbAppointmentTypesResults = new DbAppointmentTypesResults(uuid, name, duration);
//            dbAppointmentTypesResultsList.add(dbAppointmentTypesResults);
//
//        }
//
//        if (code == 200){
//
//            return new Results(200, dbAppointmentTypesResultsList);
//
//        }else {
//            return results;
//        }
//    }
//
//    @Override
//    public Results getProviders() {
//
//        DbQueryParams dbQueryParams = new DbQueryParams("v", "default");
//        List<DbQueryParams> dbQueryParamsList = new ArrayList<>();
//        dbQueryParamsList.add(dbQueryParams);
//
//        Results results = networkCall.callAppointmentWithQuery(provider, dbQueryParamsList);
//        int code = results.getCode();
//        String resultsString = String.valueOf(results.getDetails());
//
//        List<DbPerson> dbProviderResultsArrayList = new ArrayList<>();
//
//        Gson gson = new Gson();
//        DbProvider dbAppointmentTypes = gson.fromJson(resultsString, DbProvider.class);
//        List<DbProviderResults> dbProviderResultsList =  dbAppointmentTypes.getResults();
//        for (int i = 0; i < dbProviderResultsList.size(); i++){
//
//            DbPerson person = dbProviderResultsList.get(i).getPerson();
//            if (person != null){
//                String personId = person.getUuid();
//                String personName = person.getDisplay();
//
//                DbPerson dbProviderResults = new DbPerson(personId, personName);
//                dbProviderResultsArrayList.add(dbProviderResults);
//            }
//
//
//        }
//
//        if (code == 200){
//
//            return new Results(200, dbProviderResultsArrayList);
//
//        }else {
//            return results;
//        }
//    }
//
//    @Override
//    public Results getAppointmentBlocks(DbRequestQueryParams dbRequestQueryParams) {
//
//        FormatterClass formatterClass = new FormatterClass();
//
//        List<DbQueryParams> dbQueryParamsList = new ArrayList<>();
//
//        String fromDate = dbRequestQueryParams.getFromDate();
//        String toDate = dbRequestQueryParams.getToDate();
//
//        DbQueryParams dbQueryParams = new DbQueryParams("v", "default");
//        DbQueryParams dbQueryParams1 = new DbQueryParams("fromDate", formatterClass.getOpenMrsDate(fromDate));
//        DbQueryParams dbQueryParams2 = new DbQueryParams("toDate", formatterClass.getOpenMrsDate(toDate));
//
//        dbQueryParamsList.addAll(Arrays.asList(
//                dbQueryParams,dbQueryParams1,dbQueryParams2
//        ));
//
//        Results results = networkCall.callAppointmentWithQuery(appointmentBlock, dbQueryParamsList);
//        int code = results.getCode();
//        String resultsString = String.valueOf(results.getDetails());
//
//        List<DbAppointmentBlockResults> dbProviderResultsArrayList = new ArrayList<>();
//
//        Gson gson = new Gson();
//        DbAppointment dbAppointmentTypes = gson.fromJson(resultsString, DbAppointment.class);
//        List<DbAppointmentBlockResults> dbAppointmentBlockResultsList =  dbAppointmentTypes.getResults();
//
//        for (int i = 0; i < dbAppointmentBlockResultsList.size(); i++){
//
//            String uuid = dbAppointmentBlockResultsList.get(i).getUuid();
//            String startDate = dbAppointmentBlockResultsList.get(i).getStartDate();
//            String endDate = dbAppointmentBlockResultsList.get(i).getEndDate();
//
//            DbProviderData dbProvider = dbAppointmentBlockResultsList.get(i).getProvider();
//            DbResults dbLocation = dbAppointmentBlockResultsList.get(i).getLocation();
//            List<Dbtypes> types = dbAppointmentBlockResultsList.get(i).getTypes();
//
//            DbAppointmentBlockResults dbAppointmentBlockResults = new DbAppointmentBlockResults(
//                    uuid, startDate, endDate, dbProvider, dbLocation, types);
//            dbProviderResultsArrayList.add(dbAppointmentBlockResults);
//
//        }
//
//        if (code == 200){
//            return new Results(200, dbProviderResultsArrayList);
//        }else {
//            return results;
//        }
//
//    }
//
//    @Override
//    public Results createAppointmentBlock(DbCreateAppointmentBlock dbCreateAppointmentBlock) {
//
//        List<String> typeList = dbCreateAppointmentBlock.getTypes();
//        String location = dbCreateAppointmentBlock.getLocation();
//        String startDate = dbCreateAppointmentBlock.getStartDate();
//        String endDate = dbCreateAppointmentBlock.getEndDate();
//        String provider = dbCreateAppointmentBlock.getProvider();
//
//        if (typeList.size() > 0 && location != null && startDate != null && endDate != null && provider != null){
//
//            Results results = networkCall.callCreateAppointmentBlock(dbCreateAppointmentBlock, appointmentBlock);
//            int code = results.getCode();
//            String resultsString = String.valueOf(results.getDetails());
//
//            System.out.println("----- " +results.getDetails());
//
//            return new Results(code, results.getDetails());
//
//
//        }else {
//
//            return new Results(400, "Please make sure all the fields have been provided.");
//
//        }
//
//    }
//
//
//    @Override
//    public Results createAppointment(DbCreateAppointment dbCreateAppointment) {
//
//        List<String> typeList = dbCreateAppointment.getTypes();
//        String location = dbCreateAppointment.getLocation();
//        String startDate = dbCreateAppointment.getStartDate();
//        String endDate = dbCreateAppointment.getEndDate();
//        String provider = dbCreateAppointment.getProvider();
//
//        if (typeList.size() > 0 && location != null && startDate != null && endDate != null && provider != null){
//
//            Results results = networkCall.callCreateAppointment(dbCreateAppointment, appointmentBlock);
//            int code = results.getCode();
//            String resultsString = String.valueOf(results.getDetails());
//
//            System.out.println("----- " +resultsString);
//
//            return new Results(200, resultsString);
//
//
//        }else {
//
//            return new Results(400, "Please make sure all the fields have been provided.");
//
//        }
//
//
//    }
//
//
//}
