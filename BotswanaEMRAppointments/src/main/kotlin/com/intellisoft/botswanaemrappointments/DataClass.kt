package com.intellisoft.botswanaemrappointments

import com.fasterxml.jackson.annotation.JsonProperty


data class Results(val code: Int, val details: Any?)
data class Error(var timestamp: String, var status: String, var error: String)
data class DbResultsData(val message: String)

data class DbServiceTypes(
    val results:List<DbAppointmentTypesResults>
)
data class DbAppointmentTypesResults(
    val uuid: String,
    val name: String,
    val duration: Int
)

data class DbLocations(
    val results:List<DbResults>
)
data class DbResults(
    val uuid: String,
    val display: String
)

data class DbProvider(
    val results: List<DbProviderResults>
)
data class DbProviderResults(
    val uuid:String,
    val person: DbPerson?
)
data class DbPerson(
    val uuid: String,
    val display: String
)

data class DbSearchAvailableAppointmentBlock(
    val results: List<DbAvailableAppointmentBlock>
)
data class DbAvailableAppointmentBlock(
    val uuid: String,
    val startDate: String,
    val endDate: String,
    val countOfAppointments: Int,
    val unallocatedMinutes: Int,
    val display: String,
    val appointmentBlock: DbAppointmentBlock

    )
data class DbAppointmentBlock(
    val display: String,
    val startDate: String,
    val endDate: String,
    val provider: DbProviderResults,
    val locations: DbResults,
    val types :List<DbResults>)

data class DbAvailableTimeBlock(
    val results: List<DbAppointmentBlock>
)


data class DbPatient(
    val results: List<DbPatientData>
)
data class DbPatientData(
    val uuid: String,
    val person: DbPersonData?

)
data class DbPersonData(
    val gender: String,
    val age: Int,
    val birthdate: String,
    val display: String,
    val dead: Boolean,
    val deathDate: String?,
    val personName: DbPersonName
)
data class DbPersonName(
    val display: String,
    val uuid:String,
)


data class DbSuccessAppointmentBlock(
    val uuid: String?,
    val startDate: String?,
    val endDate: String?,
    val provider: DbProviderDataInfo,
    val location: DbResults,
    val types: List<DbResults>
)
data class DbProviderDataInfo(
    val person: DbPerson
)


data class DbAppointmentBlockResults(
    val uuid: String,
    val startDate: String,
    val endDate: String,
    val provider: DbProviderData,
    val location: DbResults,
    val types: List<Dbtypes>
)
data class Dbtypes(
    val uuid: String,
    val display: String
)

data class DbProviderData(
    val person: DbPerson?
)

data class DbCreateAppointment(
    val types: List<String>,
    val location: String?,
    val startDate: String?,
    val endDate: String?,
    val provider: String?
)
data class DbCreateAppointmentBlock(
    val types: List<String>,
    val location: String?,
    val startDate: String?,
    val endDate: String?,
    val provider: String?
)

data class DbScheduleNewAppointment(
    val appointmentType: String?,
    val reason: String,
    val status: String?,
    val timeSlot: String?,
    val patient: String?)


data class DbPatientAppointmentData(
    val results: List<DbPatientAppointment>
)

data class DbPatientAppointment(
    val uuid: String,
    val display: String,
    val timeSlot: DbTimeSlot,
    val patient: DbPatientData,
    val status: String,
    val reason:String?,
    val cancelReason:String?,

)
data class DbTimeSlotResult(
    val results : List<DbTimeSlot>
)
data class DbTimeSlot(
    val uuid: String,
    val startDate: String,
    val endDate: String,
    val appointmentBlock: DbAppointmentBlock,
    val countOfAppointments: Int,
    val unallocatedMinutes: Int

)

data class DbSuccessAppointment(
    val uuid: String,
    val display: String,
    val timeSlot: DbTimeSlot,
    val patient: DbPatientData,
    val status: String,
    val reason:String?,
    val cancelReason:String?,
)
data class DbUserData(
    val keycloakId: String?,
    val occupation : String,
    val employerName: String,
    val homeaddress: String,
    val patientFacilityLocation: String,
    val nok: List<DbKinNext>,
    val relationship: List<DbRelationship>
)

data class DbPatientDetails(
    val patientType: String,
    val idType: String,
    val idNumber: String,
    val givenName: String,
    val middleName: String,
    val familyName: String,
    val gender: String,
    val dob: String,
    val email: String,
    val contactNumber: String,

    val occupation : String,
    val employerName: String,
    val homeaddress: String,
    val patientFacilityLocation: String,
    val nok: List<DbKinNext>,
    val relationship: List<DbRelationship>
)
data class DbKinNext(
    val idType : String,
    val nokIdNumber : String,
    val nokFullName : String,
    val nokRelationship : String,
    val nokContact : String,
    val nokEmail : String,
)
data class DbRelationship(
    val patientId: String,
    val relationship: String
)
enum class NotificationDetails(){
    SYSTEM,
    AUTHENTICATION,
    PATIENT_ACTIVATION
}
data class DbNotification(
    val title: String,
    val message: String,
    val userId: String,
    val notificationType: String,
    val process: String
)
data class DbCreatePatientSuccess(
    val status: String,
    val response: Any
)
data class DbPersonDetails(
    val uuid: String,
    val display: String
)
enum class EndpointTypes(){
    CREATE_PATIENT
}
data class UserDetails(

    @JsonProperty("id")
    val id: String,
    @JsonProperty("keycloakId")
    val keycloakId: String,
    @JsonProperty("phoneNumber")
    val phoneNumber: String,
    @JsonProperty("gender")
    val gender: String,
    @JsonProperty("emailAddress")
    val emailAddress: String,
    @JsonProperty("username")
    val username: String,
    @JsonProperty("openMrsId")
    val openMrsId: String?,
    @JsonProperty("patient")
    val patient: Boolean,
    @JsonProperty("patientIdentificationNo")
    val patientIdentificationNo: String?
)
