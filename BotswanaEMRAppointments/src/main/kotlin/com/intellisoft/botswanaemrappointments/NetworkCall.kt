package com.intellisoft.botswanaemrappointments

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.*
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import retrofit2.Response
import retrofit2.Retrofit
import javax.annotation.PostConstruct
import kotlin.NumberFormatException


@Component
class NetworkCall() {

    @Autowired
    lateinit var appProperties: AppProperties

    var restTemplate = RestTemplate()

    @Autowired
    private val retrofit: Retrofit? = null

    private var networkRequestInterface: NetworkRequestInterface? = null

    @PostConstruct
    fun setup() {
        networkRequestInterface = retrofit?.create(NetworkRequestInterface::class.java)
    }

    
    
    fun createPatient(dbPatientDetails: DbPatientDetails)= runBlocking { bacCreatePatient(dbPatientDetails) }
    private suspend fun bacCreatePatient(dbPatientDetails: DbPatientDetails): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.createPatient(dbPatientDetails)

            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }

        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }

        return Results(code, details)

    }
    

    fun getFacilityProviders(provider:String) = runBlocking { getBacFacilityProviders(provider) }
    private suspend fun getBacFacilityProviders(provider:String): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.getProviders(provider, "default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }

        return Results(code, details)


    }
    
    fun getFacilityLocations() = runBlocking { getBacFacilityLocations() }
    private suspend fun getBacFacilityLocations(): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.getFacilityLocations()
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }
            return Results(code, details)

        }catch (e: Exception){
            println("------- $e")
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."
        }



        return Results(code, details)

    }
    
    fun getFacilityServices() = runBlocking { getBacFacilityServices() }
    private suspend fun getBacFacilityServices(): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.getFacilityServices("default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }

        return Results(code, details)


    }
    
    fun getTimeSlots(limit: String) = runBlocking { getBacTimeSlots(limit) }
    private suspend fun getBacTimeSlots(limit:String): Results{

        var details: Any?
        var code: Int

        try {

            val limitValue = limit.toInt()

            val retrofitCall = networkRequestInterface?.getTimeSlots(limitValue, "default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }catch (e: NumberFormatException){
            code = 400
            details = "Provide a valid limit."
        }

        return Results(code, details)


    }
    
    fun getTimeSlotDetails(timeslotId:String) = runBlocking { getBacTimeSlotDetails(timeslotId) }
    private suspend fun getBacTimeSlotDetails(timeslotId:String): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.getTimeSlotDetails(timeslotId)
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."
            println("------ $e")
        }

        return Results(code, details)


    }
    
    fun getAppointmentTypes(limit:String) = runBlocking { getBacAppointmentTypes(limit) }
    private suspend fun getBacAppointmentTypes(limit:String): Results{

        var details: Any?
        var code: Int

        try {

            val limitValue = limit.toInt()

            val retrofitCall = networkRequestInterface?.getAppointmentTypes(limitValue, "default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }catch (e: NumberFormatException){
            code = 400
            details = "Provide a valid limit"
        }

        return Results(code, details)


    }
    
    fun getPatientAppointments(fromDate:String, patient:String, toDate:String) = runBlocking { getBacPatientAppointments(fromDate, patient, toDate) }
    private suspend fun getBacPatientAppointments(fromDate:String, patient:String, toDate:String): Results{

        var details: Any?
        var code: Int

        try {

            val retrofitCall = networkRequestInterface?.getPatientAppointments(fromDate, patient,toDate, "default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }


        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }

        return Results(code, details)


    }
    
    fun scheduleNewAppointment(dbScheduleNewAppointment: DbScheduleNewAppointment) = runBlocking { bacScheduleNewAppointment(dbScheduleNewAppointment) }
    private suspend fun bacScheduleNewAppointment(dbScheduleNewAppointment: DbScheduleNewAppointment): Results{

        var details: Any?
        var code: Int


        try {

            val retrofitCall = networkRequestInterface?.scheduleNewAppointment(dbScheduleNewAppointment)
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 400
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

                println("------ $retrofitCall")


            }else{
                code = 400
                details = "The requested resource could not be found."
            }

        }catch (e:Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }
        return Results(code, details)


    }

    fun getUserDetails(keycloakId: String) = runBlocking { getBacUserDetails(keycloakId) }
    private suspend fun getBacUserDetails(keycloakId: String) : UserDetails?{

        val notificationUrl = appProperties.authenticationUrl
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val url = "/service/authentication/user/$keycloakId"

        return try {

            val results = restTemplate.getForEntity("$notificationUrl$url", UserDetails::class.java, 1)
            if (results.statusCode == HttpStatus.OK){
                println("----- ${results.body}")
                results.body
            }else{
                null
            }

        }catch (e: Exception){
            null
        }



    }





}