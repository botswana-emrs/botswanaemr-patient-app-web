package com.intellisoft.botswanaemrauthentication.authentication.controller;

import com.intellisoft.botswanaemrauthentication.*;

import com.intellisoft.botswanaemrauthentication.authentication.service_class.PatientDetailsServiceImpl;
import org.keycloak.KeycloakPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@RequestMapping(value = "/api/v1")
@RestController
public class AuthenticationController {

    @Value("${notification}")
    private String notificationService;

    @Autowired
    private PatientDetailsServiceImpl patientDetailsService;


    FormatterClass formatterClass = new FormatterClass();

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(
            @RequestParam("password") String password,
            @RequestParam("confirmPassword") String confirmPassword,
            @RequestParam("emailAddress") String emailAddress,
            @RequestParam("firstName") String firstName,
            @RequestParam("lastName") String lastName,
            @RequestParam("phoneNumber") String phoneNumber,
            @RequestParam("dateOfBirth") String dateOfBirth,
            @RequestParam("gender") String gender,
            @RequestParam("patientIdentificationNo") String patientIdentificationNo,
            @RequestParam("nationalPassportNo") String nationalPassportNo,
            @RequestParam("username") String username,
            @RequestParam("identificationType") String identificationType,
            @RequestParam("file") String fileUrl) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

        try {


            RegisterRequest registerRequest = new RegisterRequest(
                    password, confirmPassword, emailAddress, firstName, lastName, phoneNumber, dateOfBirth, gender,
                    patientIdentificationNo, nationalPassportNo, username, identificationType, fileUrl);

            Results results = patientDetailsService.addPatient(registerRequest);
            return formatterClass.getResponse(results);

        }catch (Exception e){
            Results results = new Results(400, "Please check if you have provided a profile picture.");
            return ResponseEntity.badRequest().body(results);
        }



    }

    @PostMapping(path = "/login")
    public ResponseEntity<?> signin(@RequestBody LoginRequest loginRequest)
            throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        Results results = patientDetailsService.loginUser(loginRequest);
        return formatterClass.getResponse(results);

    }

    @PostMapping(path = "/resend-link")
    public ResponseEntity<?> resendLink(@RequestBody ResendLink resendLink){

        Results results = patientDetailsService.resendLink(resendLink);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/padd")
    public String getPatientDetails(){
        System.out.println("link: " + "link");
        System.out.println("req_id: " + "req_id");
        return "xxxx";
    }

    @GetMapping(value = "/verify-link/")
    public String getVerificationDetails(
            @Param("link") String link,
            @Param("req_id") String req_id
    ){

        System.out.println("link: " + link);
        System.out.println("req_id: " + req_id);

        Results results = patientDetailsService.verifyLink(link, req_id);
        return (String) results.getMessage();

    }

    @PostMapping(path = "/request-password-reset")
    public ResponseEntity<?> requestResetPassword(@RequestBody ResendLink resendLink){

        Results results = patientDetailsService.requestResetPassword(resendLink);
        return formatterClass.getResponse(results);

    }

    @PostMapping(path = "/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordRequest resetPasswordRequest) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        Results results = patientDetailsService.resetPassword(resetPasswordRequest);
        return formatterClass.getResponse(results);

    }



}
