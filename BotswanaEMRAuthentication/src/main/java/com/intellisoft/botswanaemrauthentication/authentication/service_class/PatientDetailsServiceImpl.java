package com.intellisoft.botswanaemrauthentication.authentication.service_class;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.intellisoft.botswanaemrauthentication.*;
import com.intellisoft.botswanaemrauthentication.authentication.entity.PatientDetails;
import com.intellisoft.botswanaemrauthentication.authentication.repository.PatientDetailsRepository;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.HttpClients;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jetbrains.annotations.NotNull;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.Configuration;
import org.keycloak.authorization.client.util.HttpResponseException;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.ws.rs.core.Response;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static org.keycloak.OAuth2Constants.PASSWORD;
import org.apache.commons.codec.binary.Base64;

@Service
public class PatientDetailsServiceImpl implements PatientDetailsService{

    @Value("${app.keycloak.authServerUrl}")
    private String authServerUrl;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${app.keycloak.client-id}")
    private String clientId;

    @Value("${app.keycloak.client-secret}")
    private String  clientSecret;

    @Value("${app.keycloak.grant-type}")
    private String grant_type;

    @Value("${app.keycloak.realm.username}")
    private String username;
    @Value("${app.keycloak.realm.password}")
    private String password;

    @Autowired
    private NetworkCall networkCall = new NetworkCall();

    @Autowired
    private PatientDetailsRepository patientDetailsRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    private FormatterClass formatterClass = new FormatterClass();


    @Transactional
    @Override
    public Results addPatient(RegisterRequest registerRequest) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

//        Check if email address exists
        String emailAddress = registerRequest.getEmailAddress();
        String password = registerRequest.getPassword();
        String confirmPassword = registerRequest.getConfirmPassword();

        boolean isPassword = formatterClass.isPasswordMatch(password, confirmPassword);
        boolean isEmailValid = formatterClass.isEmailValid(emailAddress);

        if (isPassword && isEmailValid){
            //Password matches

            boolean isEmailAddress = checkEmailAddress(emailAddress);

            if (!isEmailAddress){

//                Email address does not exist, add user using keycloak
                String userId = createKeycloakUser(registerRequest).getUserId();

                if (!userId.equals("")){

                    //User has been saved in keycloak, save them to our db
                    PatientDetails patientDetails = new PatientDetails(
                            registerRequest.getFirstName(),
                            registerRequest.getLastName(),
                            registerRequest.getPhoneNumber(),
                            registerRequest.getDateOfBirth(),
                            registerRequest.getGender(),
                            registerRequest.getPatientIdentificationNo(),
                            registerRequest.getNationalPassportNo(),
                            registerRequest.getIdentificationType(),
                            registerRequest.getEmailAddress(),
                            userId,
                            registerRequest.getUsername(), false, registerRequest.getImageUrl());


                    DbVerificationLink dbVerificationLink = generateVerificationLink(patientDetails);

                    PatientDetails addedPatientDetails = patientDetailsRepository.save(dbVerificationLink.getPatientDetails());

                    formatterClass.sendMail(javaMailSender,
                            addedPatientDetails,
                            dbVerificationLink.getBaseUrl(),
                            dbVerificationLink.getVerificationLink());

                    //Create notification that user has not created a patient

                    DbNotification dbNotification = new DbNotification(
                            "Patient Activation.",
                            "Your account has been created, but you have not activated the patient status.",
                            addedPatientDetails.getId(),
                            NotificationDetails.SYSTEM.name(),
                            NotificationDetails.PATIENT_ACTIVATION.name());

                    createNotification(dbNotification);

                    return new Results(201, "Patient has been saved successfully. Check email for verification link.");

                }else {

                    return new Results(400, "There was an issue saving the user please try again after sometime.");

                }

            }else {
                //Email address exists, inform user
                return new Results(400, "The provided email address is already in the system.");
            }
        }else {
            //Password don't match or email is not valid

            String error = "";

            if (!isPassword) error = error + "Your password don't match.";
            if (!isEmailValid) error = error + "Please submit a valid email address.";

            return new Results(400, error);

        }


    }

    @Override
    public Results verifyLink(String verifyLink, String userId) {

        //Get patient with the following user id
        Optional<PatientDetails> optionalPatientDetails = patientDetailsRepository.findById(userId);
        if (optionalPatientDetails.isPresent()){
            //User exists, check if the provided details are valid
            PatientDetails patientDetails = optionalPatientDetails.get();

            boolean isValid = patientDetails.isVerified();
            if (!isValid){
                //User is not verified
                boolean isLinkValid = patientDetails.isOTPValid();
                if (isLinkValid){
                    //Link is valid, proceed with verification

                    String dbVerifyLink = patientDetails.getOneTimeLink();
                    if(dbVerifyLink.equals(verifyLink)){

                        patientDetails.setVerified(true);
                        PatientDetails updatePatientDetails = updatePatientDetails(patientDetails);

                        if (updatePatientDetails != null){

                            //Create Person and Patient in background.

                            String uuid = updatePatientDetails.getId();

                            String givenName = updatePatientDetails.getFirstName();
                            String familyName = updatePatientDetails.getLastName();

                            DbName dbName = new DbName(givenName, familyName);

                            List<DbName> dbNameList = new ArrayList<>();
                            dbNameList.add(dbName);

                            String gender = updatePatientDetails.getGender();
                            String birthDate = updatePatientDetails.getDateOfBirth();

                            DbAddress dbAddress = new DbAddress("Kenya", "Kenya", "Kenya", "00100");
                            List<DbAddress> dbAddressList = new ArrayList<>();

                            dbAddressList.add(dbAddress);

                            DbPerson dbPerson = new DbPerson(dbNameList, gender, birthDate, dbAddressList);

                            formatterClass.createPerson(this, dbPerson,uuid);

                            return new Results(200, "Patient has been verified successfully.");
                        }else {
                            return new Results(400, "Patient could not be verified. Please try again");
                        }

                    }else {
                        return new Results(400, "The provided link is invalid.");
                    }

                }else {
                    return new Results(400, "The link has expired, request a new verification link.");
                }

            }else {
                return new Results(400, "The link has already been used.");
            }

        }else {
            return new Results(400, "The link is not valid please check again before proceeding.");

        }


    }

    @Transactional
    @Override
    public Results loginUser(LoginRequest loginRequest) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        try{

            //Check if user is verified
            String emailAddress = loginRequest.getEmailAddress();
            PatientDetails patientDetails = findByEmailAddress(emailAddress);

            if (patientDetails != null){

                boolean isVerified = patientDetails.isVerified();
                if (isVerified){

                    Map<String, Object> clientCredentials = new HashMap<>();
                    clientCredentials.put("secret", clientSecret);
                    clientCredentials.put("grant_type", grant_type);

                    HttpClient httpClient = HttpClients
                            .custom()
                            .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                            .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                            .build();

                    Configuration configuration =
                            new Configuration(authServerUrl, realm, clientId, clientCredentials, httpClient);
                    AuthzClient authzClient = AuthzClient.create(configuration);

                    try{

                        AccessTokenResponse response = authzClient
                                .obtainAccessToken(loginRequest.getEmailAddress(), loginRequest.getPassword());

                        String accessToken = response.getToken();
                        long expiresIn = response.getExpiresIn();

                        String refreshToken = response.getRefreshToken();
                        long refreshExpiresIn = response.getRefreshExpiresIn();

                        String type = response.getTokenType();

                        List<String> roleList = getRoles(accessToken);


                        LoginResponse loginResponse = new LoginResponse(accessToken, expiresIn,
                                refreshToken, refreshExpiresIn, type, roleList, patientDetails);


                        return new Results(200, loginResponse);

                    }catch (HttpResponseException ex){
                        return new Results(400, "The provided credentials are invalid.");
                    }


                }else {
                    return new Results(400, "The email address is not verified.");

                }


            }else {
                return new Results(400, "The email address could not be found.");
            }

        }catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e){

            /**TODO: Capture the correct message from keycloak
             *
             */

            System.out.println("---- " + e.getMessage());
            return new Results(400, "Patient could not be logged in.");
        }

    }

    private List<String> getRoles(String jwtToken){

//        System.out.println("------------ Decode JWT ------------");
        String[] split_string = jwtToken.split("\\.");
        String base64EncodedHeader = split_string[0];
        String base64EncodedBody = split_string[1];
        String base64EncodedSignature = split_string[2];

//        System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
        Base64 base64Url = new Base64(true);
        String header = new String(base64Url.decode(base64EncodedHeader));


        System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
        String body = new String(base64Url.decode(base64EncodedBody));
        Gson g = new Gson();
        JsonObject jsonObject = g.fromJson(body, JsonObject.class);
        JsonArray roles = jsonObject
                .get("realm_access").getAsJsonObject()
                .get("roles").getAsJsonArray();

        List<String> rolesList = new ArrayList<>();
        for (int i = 0; i < roles.size(); i++){

            String role = roles.get(i).getAsString();
            if (role.equals("offline_access") || role.equals("uma_authorization")) {
            }else {
                rolesList.add(role);
            }

        }
        return rolesList;

    }

    @Transactional
    @Override
    public Results resendLink(ResendLink resendLink) {

        String emailAddress = resendLink.getEmailAddress();

        boolean isEmailAddress = checkEmailAddress(emailAddress);
        if (isEmailAddress){

            PatientDetails patientDetails = findByEmailAddress(emailAddress);
            DbVerificationLink dbVerificationLink = generateVerificationLink(patientDetails);

            PatientDetails updatePatientDetails = updatePatientDetails(dbVerificationLink.getPatientDetails());

            formatterClass.sendMail(javaMailSender,
                    updatePatientDetails,
                    dbVerificationLink.getBaseUrl(),
                    dbVerificationLink.getVerificationLink());


            return new Results(200, "Verification link has been sent to the email address.");


        }else{
            return new Results(400, "The email address could not be found.");
        }

    }

    @Override
    public Results requestResetPassword(ResendLink resendLink) {

        //Request reset password code. Provide code that will be submitted with new password. Send to email address
        String emailAddress = resendLink.getEmailAddress();
        boolean isEmailAddress = checkEmailAddress(emailAddress);

        if (isEmailAddress){
            //Email address exists

            PatientDetails patientDetails = findByEmailAddress(emailAddress);

            DbVerificationLink dbResetOtp = getResetPassword(patientDetails);

            PatientDetails updatePatientDetails = updatePatientDetails(dbResetOtp.getPatientDetails());

            formatterClass.sendResetPasswordMail(javaMailSender,
                    updatePatientDetails,
                    dbResetOtp.getVerificationLink());


            DbNotification dbNotification = new DbNotification(
                    "Password change",
                    "Someone has requested to change your password.",
                    patientDetails.getId(),
                    NotificationDetails.SYSTEM.name(),
                    NotificationDetails.AUTHENTICATION.name());

            createNotification(dbNotification);

            return new Results(200, "A reset password code has been sent to your email address.");

        }else {
            //Email does not exist
            return new Results(400, "The email address could not be found.");
        }

    }

    @Override
    public Results resetPassword(ResetPasswordRequest resetPasswordRequest) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        String emailAddress = resetPasswordRequest.getEmailAddress();
        boolean isEmailAddress = checkEmailAddress(emailAddress);

        String newPassword = resetPasswordRequest.getPassword();
        String confirmPassword = resetPasswordRequest.getConfirmPassword();

        if (newPassword.equals(confirmPassword)){

            if (isEmailAddress){
                //Email address exists
                PatientDetails patientDetails = findByEmailAddress(emailAddress);

                String requestOtpCode = resetPasswordRequest.getOtpCode();
                String dbOtpCode = patientDetails.getOneTimeLink();

                boolean isLinkValid = patientDetails.isOTPValid();

                if (isLinkValid){
                    //Link is valid, proceed with verification

                    if(dbOtpCode.equals(requestOtpCode)){

                        Keycloak keycloak = KeycloakBuilder
                                .builder()
                                .serverUrl(authServerUrl)
                                .grantType(PASSWORD)
                                .realm("master")
                                .clientId("admin-cli")
                                .username(username).password(password)
                                .resteasyClient(
                                        new ResteasyClientBuilder()
                                                .sslContext(new SSLContextBuilder().loadTrustMaterial(null,
                                                        TrustAllStrategy.INSTANCE).build())
                                                .hostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                                .connectionPoolSize(10).build())
                                .build();

                        keycloak.tokenManager().getAccessToken();

                        //Change the password
                        String keycloakId = patientDetails.getUserKeycloakId();
                        // create password credential
                        CredentialRepresentation passwordCred = new CredentialRepresentation();
                        passwordCred.setTemporary(false);
                        passwordCred.setType(CredentialRepresentation.PASSWORD);
                        passwordCred.setValue(resetPasswordRequest.getPassword());


                        RealmResource realmResource = keycloak.realm(realm);
                        UsersResource usersRessource = realmResource.users();
                        UserResource userResource = usersRessource.get(keycloakId);
                        userResource.resetPassword(passwordCred);

                        return new Results(200, "Password has been changed successfully.");


                    }else {
                        return new Results(400, "The provided otp is invalid.");

                    }

                }else {
                    return new Results(400, "The otp has expired, request a new verification link.");

                }


            }else {
                //Email does not exist
                return new Results(400, "The email address could not be found.");
            }

        }else {
            return new Results(400, "The provided passwords do not match.");

        }




    }

    @Override
    public Results findKeycloakUserId(String keycloakId) {

        PatientDetails patientDetails = getPatientDetails(keycloakId);

        return getPatientResultsData(patientDetails);

    }



    public PatientDetails getPatientDetails(String keycloakId){

        PatientDetails patientDetails = patientDetailsRepository.findByUserKeycloakId(keycloakId);
        if (patientDetails != null){
            return patientDetails;
        }else {
            return null;
        }

    }

    @Override
    public Results updatePatientInfo(UpdateUserDetails userDetails, String keycloakId) {

        PatientDetails patientDetails = getPatientDetails(keycloakId);
        if (patientDetails != null){

            String firstName = userDetails.getFirstName();
            String lastName = userDetails.getLastName();
            String phoneNumber = userDetails.getPhoneNumber();
            String pin = userDetails.getPatientIdentificationNo();
            String username = userDetails.getUsername();

            if (firstName != null && !firstName.equals("")) patientDetails.setFirstName(firstName);
            if (lastName != null && !lastName.equals("")) patientDetails.setLastName(lastName);
            if (phoneNumber != null && !phoneNumber.equals("")) patientDetails.setPhoneNumber(phoneNumber);
            if (pin != null && !pin.equals("")){
                patientDetails.setPatientIdentificationNo(pin);
                patientDetails.setPatient(true);
                patientDetails.setOpenMrsId(pin);
            }
            if (username != null && !username.equals("")) patientDetails.setUsername(username);

            PatientDetails updatePatientDetails = updateUserDetails(patientDetails);
            if (updatePatientDetails != null){

                return new Results(200,new DbResults("User details has been updated successfully."));
            }else {
                return new Results(400, "User could not be updated.");
            }

        }else {
            return new Results(400, "User not found");
        }


    }

    @Override
    public Results getUserDetails(String patientInfo, String keycloakId) {

        PatientDetails patientDetails = getPatientDetails(keycloakId);
        Results results;

        if (patientDetails != null){

            DbPatientDetails dbPatientDetails = new DbPatientDetails(
                    patientDetails.getId(),
                    patientDetails.getEmailAddress(),
                    patientDetails.getFirstName(),
                    patientDetails.getLastName(),
                    patientDetails.getPhoneNumber(),
                    patientDetails.getDateOfBirth(),
                    patientDetails.getPatientIdentificationNo(),
                    patientDetails.getOpenMrsId(),
                    patientDetails.getUserKeycloakId(),
                    patientDetails.getGender(),
                    patientDetails.getUsername(),
                    patientDetails.isPatient());

            if(patientInfo != null && !patientInfo.equals("")){

                String openMrsId = dbPatientDetails.getPatientIdentificationNo();
                if (openMrsId != null){

                    Results resultsPatient = networkCall.getPatientDetails(openMrsId);
                    int code = resultsPatient.getCode();
                    if (code == 200){

                        DbOpenMrsLocal dbOpenMrsLocal = new DbOpenMrsLocal(dbPatientDetails, resultsPatient.getMessage());
                        results = new Results(200, dbOpenMrsLocal);

                    }else {

                        results = new Results(400, "We could not find the required details.");

                    }

                }else {

                    results = new Results(400, "The user does not have PIN from any hospital.");
                }


            }else {

                DbOpenMrsLocal dbOpenMrsLocal = new DbOpenMrsLocal(dbPatientDetails, null);
                results = new Results(200, dbOpenMrsLocal);
            }

        }else {

            results = new Results(400, "User not found.");
        }

        return results;
    }

    @Override
    public Results searchUser(String emailAddress) {

        PatientDetails patientDetails = searchUserByEmail(emailAddress);
        return getPatientResultsData(patientDetails);
    }

    @Override
    public Results updateUserRole(String keyCloakId, String role) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        addRealmRoleToUser(keyCloakId, role);

        return new Results(200, new DbResults("User role has been updated successfully."));
    }

    public void addRealmRoleToUser(String keyCloakId, String role_name) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        Keycloak keycloak = KeycloakBuilder
                .builder()
                .serverUrl(authServerUrl)
                .grantType(PASSWORD)
                .realm("master")
                .clientId("admin-cli")
                .username(username).password(password)
                .resteasyClient(
                        new ResteasyClientBuilder()
                                .sslContext(new SSLContextBuilder()
                                        .loadTrustMaterial(null,
                                                TrustAllStrategy.INSTANCE).build())
                                .hostnameVerifier(NoopHostnameVerifier.INSTANCE)
                                .connectionPoolSize(10).build())
                .build();


        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersRessource = realmResource.users();
        UserResource userResource = usersRessource.get(keyCloakId);

        RoleRepresentation realmRoleUser = realmResource.roles().get(role_name).toRepresentation();

        // Assign realm role user to the user
        userResource.roles().realmLevel().add(Arrays.asList(realmRoleUser));

    }

    @NotNull
    private Results getPatientResultsData(PatientDetails patientDetails) {
        if (patientDetails != null){
            DbPatientDetails dbPatientDetails = new DbPatientDetails(
                    patientDetails.getId(),
                    patientDetails.getEmailAddress(),
                    patientDetails.getFirstName(),
                    patientDetails.getLastName(),
                    patientDetails.getPhoneNumber(),
                    patientDetails.getDateOfBirth(),
                    patientDetails.getPatientIdentificationNo(),
                    patientDetails.getOpenMrsId(),
                    patientDetails.getUserKeycloakId(),
                    patientDetails.getGender(),
                    patientDetails.getUsername(),
                    patientDetails.isPatient());
            return new Results(200, dbPatientDetails);
        }else {
            return new Results(400, "User not found.");
        }
    }

    //Search for a user by email address
    private PatientDetails searchUserByEmail(String emailAddress){

        boolean isEmailExist = patientDetailsRepository.existsByEmailAddress(emailAddress);
        if (isEmailExist) {
            return patientDetailsRepository.findByEmailAddress(emailAddress);
        }else {
            return null;
        }

    }

    public PatientDetails updateUserDetails(PatientDetails patientDetails){

        return patientDetailsRepository.findById(patientDetails.getId())
                .map(patientDetailsOld ->{
                    patientDetailsOld.setFirstName(patientDetails.getFirstName());
                    patientDetailsOld.setLastName(patientDetails.getLastName());
                    patientDetailsOld.setPhoneNumber(patientDetails.getPhoneNumber());
                    patientDetailsOld.setUsername(patientDetails.getUsername());
                    patientDetailsOld.setProfileUrl(patientDetails.getProfileUrl());

                    patientDetailsOld.setPatientIdentificationNo(patientDetails.getPatientIdentificationNo());
                    patientDetailsOld.setPatient(patientDetails.isPatient());
                    patientDetailsOld.setOpenMrsId(patientDetails.getOpenMrsId());
                    return patientDetailsRepository.save(patientDetailsOld);
                }).orElse(null);


    }


    private void createNotification(DbNotification dbNotification){

        networkCall.createNotification("CREATE_NOTIFICATION", dbNotification);

    }

    //get reset otp
    private DbVerificationLink getResetPassword(PatientDetails patientDetails){

        String otpCode = formatterClass.getResetPasswordOtp(6);
        patientDetails.setOneTimeLink(otpCode);
        patientDetails.setOtpRequestedTime(new Date());

        String baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
        return new DbVerificationLink(baseUrl, otpCode, patientDetails);

    }


    //get verification link
    private DbVerificationLink generateVerificationLink(PatientDetails patientDetails){

        String verificationLink = formatterClass.getVerificationLink(30);

        patientDetails.setOneTimeLink(verificationLink);
        patientDetails.setOtpRequestedTime(new Date());

        String baseUrl = "https://botswanaemrdev.intellisoftkenya.com:9191";

        return new DbVerificationLink(baseUrl, verificationLink, patientDetails);

    }

    private PatientDetails findByEmailAddress(String emailAdress){
        return patientDetailsRepository.findByEmailAddress(emailAdress);
    }

    private PatientDetails updatePatientDetails(PatientDetails patientDetails){

        String id = patientDetails.getId();
        return patientDetailsRepository.findById(id)
                .map(patientDetailsOld -> {

                    patientDetailsOld.setVerified(patientDetails.isVerified());

                    patientDetailsOld.setOneTimeLink(patientDetails.getOneTimeLink());
                    patientDetailsOld.setOtpRequestedTime(patientDetails.getOtpRequestedTime());

                    return patientDetailsRepository.save(patientDetailsOld);

                }).orElse(null);
    }

    private KeycloakUserId createKeycloakUser(RegisterRequest registerRequest) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {

        String role = "app-user";

        // Install the all-trusting trust manager
        Keycloak keycloak = KeycloakBuilder
                .builder()
                .serverUrl(authServerUrl)
                .grantType(PASSWORD)
                .realm("master")
                .clientId("admin-cli")
                .username(username).password(password)
                .resteasyClient(
                        new ResteasyClientBuilder()
                                .sslContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
                                .hostnameVerifier(NoopHostnameVerifier.INSTANCE).connectionPoolSize(10).build())
                .build();

        keycloak.tokenManager().getAccessToken();

        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(registerRequest.getEmailAddress());
        user.setFirstName(registerRequest.getFirstName());
        user.setLastName(registerRequest.getLastName());
        user.setEmail(registerRequest.getEmailAddress());

        // Get realm
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersRessource = realmResource.users();

        Response response = usersRessource.create(user);

        int statusCode = response.getStatus();

        System.out.println("----------");
        System.out.println(statusCode);
        System.out.println(response);

        if (statusCode == 201){
            //User has been registered successfully, get saved details
            String userId = CreatedResponseUtil.getCreatedId(response);

            // create password credential
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(registerRequest.getPassword());


            UserResource userResource = usersRessource.get(userId);
//            // Set password credential
            userResource.resetPassword(passwordCred);

            // Get realm role user
            RoleRepresentation realmRoleUser = realmResource.roles().get(role).toRepresentation();

            // Assign realm role user to the user
            userResource.roles().realmLevel().add(Arrays.asList(realmRoleUser));

            return new KeycloakUserId(userId);


        }else {

            return new KeycloakUserId("");
        }


    }

    private Boolean checkEmailAddress(String emailAddress) {

        return patientDetailsRepository.existsByEmailAddress(emailAddress);
    }



}
