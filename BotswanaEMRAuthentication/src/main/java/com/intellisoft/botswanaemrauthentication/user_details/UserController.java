package com.intellisoft.botswanaemrauthentication.user_details;

import com.intellisoft.botswanaemrauthentication.DbConsent;
import com.intellisoft.botswanaemrauthentication.FormatterClass;
import com.intellisoft.botswanaemrauthentication.Results;
import com.intellisoft.botswanaemrauthentication.UpdateUserDetails;
import com.intellisoft.botswanaemrauthentication.authentication.service_class.PatientDetailsServiceImpl;
import com.intellisoft.botswanaemrauthentication.user_details.consent.service.ConsentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RequestMapping(value = "/api/v1/user/")
@RestController
public class UserController {

    FormatterClass formatterClass = new FormatterClass();

    @Autowired
    private PatientDetailsServiceImpl patientDetailsService;

    @Autowired
    private ConsentServiceImpl consentService;

    @GetMapping(value = "details")
    public ResponseEntity<?> getUserDetails(
            @Param("patientInfo") String patientInfo,
            HttpServletRequest request
    ){

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()){
                results = patientDetailsService.getUserDetails(patientInfo, userId.get());
            }else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        }catch (Exception e){
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }



    }

    @PutMapping(value = "details")
    public ResponseEntity<?> updateUserDetails(
            @RequestBody UpdateUserDetails userDetails){

        try{

            Optional<String> keyCloak = formatterClass.getCurrentUserLogin();
            Results results;
            if (keyCloak.isPresent()){
                results = patientDetailsService.updatePatientInfo(userDetails, keyCloak.get());
            }else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        }catch (Exception e){
            Results results = new Results(400, "Please check if you have provided a profile picture.");
            return ResponseEntity.badRequest().body(results);
        }


    }

    @GetMapping(value = "{consentId}")
    public ResponseEntity<?> getConsent(@PathVariable("consentId") String consentId) {

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()) {
                results = consentService.getConsent(consentId);
            } else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        } catch (Exception e) {
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }
    }

    @PutMapping(value = "{consentId}")
    public ResponseEntity<?> updateConsent(@PathVariable("consentId") String consentId, @RequestBody DbConsent consent) {

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()) {
                results = consentService.updateConsent(consentId, consent);
            } else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        } catch (Exception e) {
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }
    }

    @PostMapping(value = "{consentId}")
    public ResponseEntity<?> createConsent(@RequestBody DbConsent consent) {

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()) {
                results = consentService.saveConsent(consent, userId.get());
            } else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        } catch (Exception e) {
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }
    }

    @DeleteMapping(value = "{consentId}")
    public ResponseEntity<?> deleteConsent(@PathVariable("consentId") String consentId) {

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()) {
                results = consentService.deleteConsent(consentId);
            } else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        } catch (Exception e) {
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }
    }

    @GetMapping(value = "consents")
    public ResponseEntity<?> getConsents() {

        Optional<String> userId = formatterClass.getCurrentUserLogin();

        try {

            Results results;
            if (userId.isPresent()) {
                results = consentService.getConsentByUserId(userId.get());
            } else {
                results = new Results(400, "We could not find the user");
            }
            return formatterClass.getResponse(results);

        } catch (Exception e) {
            Results results = new Results(400, "We could not find the user. Please try again after sometime.");
            return formatterClass.getResponse(results);

        }
    }

}
