package com.intellisoft.botswanaemrauthentication.user_details.consent.repository;

import com.intellisoft.botswanaemrauthentication.user_details.consent.entity.Consent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConsentRepository extends JpaRepository<Consent, String> {

    List<Consent> findByUserId(String userId);
}