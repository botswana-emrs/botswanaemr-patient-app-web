package com.intellisoft.botswanaemrauthentication.user_details.consent.service;

import com.intellisoft.botswanaemrauthentication.DbConsent;
import com.intellisoft.botswanaemrauthentication.Results;
import com.intellisoft.botswanaemrauthentication.user_details.consent.entity.Consent;
import com.intellisoft.botswanaemrauthentication.user_details.consent.repository.ConsentRepository;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsentServiceImpl {

    @Autowired
    private ConsentRepository consentRepository;

    public Results saveConsent(DbConsent dbConsent, String userId){

        String language = dbConsent.getLanguage();
        String plannedOperation = dbConsent.getPlannedOperation();
        String patientName = dbConsent.getPatientName();
        String nurseName = dbConsent.getNurseName();
        String patientRelationship = dbConsent.getPatientRelationship();
        String signature = dbConsent.getSignature();
        String witnessName = dbConsent.getWitnessName();

        Consent consent = new Consent(language, plannedOperation, patientName, nurseName, patientRelationship, signature, witnessName, userId);
        Consent savedConsent = consentRepository.save(consent);
        if (savedConsent != null) {
            return new Results(201, "Consent saved successfully");
        } else {
            return new Results(400, "Consent not saved");
        }
    }

    public Results getConsent(String id) {

        Consent consent = consentRepository.findById(id).orElse(null);
        Results results;
        if (consent != null){
            results = new Results(200, consent);
        }else {
            results = new Results(400, "We could not find the consent");
        }
        return results;
    }

    public Results deleteConsent(String id) {
        Consent consent = consentRepository.findById(id).orElse(null);
        if (consent != null){
            consentRepository.deleteById(id);
            return new Results(200, "Consent deleted successfully");
        }else {
            return new Results(400, "Consent not deleted");
        }

    }
    
    //Update consent
    public Results updateConsent(String consentId, DbConsent dbConsent){
        
        Consent consent = consentRepository.findById(consentId).orElse(null);
        Results results;
        if (consent != null) {

            Consent updateConsent =  consentRepository.findById(consent.getId())
                    .map(consentOld ->{

                        consentOld.setLanguage(dbConsent.getLanguage());
                        consentOld.setPlannedOperation(dbConsent.getPlannedOperation());
                        consentOld.setPatientName(dbConsent.getPatientName());
                        consentOld.setNurseName(dbConsent.getNurseName());
                        consentOld.setPatientRelationship(dbConsent.getPatientRelationship());
                        consentOld.setSignature(dbConsent.getSignature());
                        consentOld.setWitnessName(dbConsent.getWitnessName());

                        return consentRepository.save(consentOld);

                    }).orElse(null);

            if (updateConsent != null) {
                results = new Results(200, "Consent details were updated successfully");
            }else {
                results = new Results(400, "We could not update the consent");
            }

        }else {
            results = new Results(400, "We could not find the consent");
        }
        
        return results;
    }
    
    //get all consent by user id
    public Results getConsentByUserId(String userId) {

        List<Consent> consentList = consentRepository.findByUserId(userId);
        return new Results(200, consentList);

    }


}
