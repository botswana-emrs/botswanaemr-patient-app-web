package com.intellisoft.botswanaemrauthentication

import com.intellisoft.botswanaemrauthentication.authentication.entity.PatientDetails

data class KeycloakUserId(
    val userId: String
)
data class ResetPasswordRequest(
    val emailAddress: String ,
    val password: String,
    val confirmPassword: String,
    val otpCode: String)

data class ResendLink(
    val emailAddress: String = ""
)

data class DbVerificationLink(
    val baseUrl : String,
    val verificationLink: String,
    val patientDetails: PatientDetails
)

data class Results(
    val code: Int,
    val message: Any)

data class LoginResponse(
    val access_token: String,
    val expires_in: Long,
    val refresh_token: String,
    val refresh_expires_in: Long,
    val token_type: String,
    val roles: List<String>,
    val user_details: Any?
)
data class LoginRequest(
    val emailAddress: String ,
    val password: String 
)
data class RegisterRequest(

    val password: String,
    val confirmPassword: String,
    val emailAddress: String ,
    val firstName:String ,
    val lastName: String ,
    val phoneNumber: String ,
    val dateOfBirth: String ,
    val gender: String ,
    val patientIdentificationNo: String? ,
    val nationalPassportNo: String ,
    val username: String ,
    val identificationType: String,
    val imageUrl: String,
    )
data class UpdateUserDetails(
    val firstName:String? ,
    val lastName: String? ,
    val phoneNumber: String? ,
    val patientIdentificationNo: String? ,
    val username: String?)

data class DbPerson(
    val names:List<DbName>,
    val gender: String?,
    val birthdate:String?,
    val addresses:List<DbAddress>
)
data class DbName(
    val givenName: String,
    val familyName:String
)
data class DbAddress(
    val address1:String,
    val cityVillage:String,
    val country: String,
    val postalCode: String
)
data class DbPersonSuccess(
    val uuid:String
)

data class DbPatient(
    val person: String?,
    val identifiers: List<DbIdentifiers>
)
data class DbIdentifiers(
    val identifier: String,
    val identifierType: String,
    val location: String,
    val preferred: Boolean
)
data class DbNotification(
    val title: String,
    val message: String,
    val userId: String,
    val notificationType: String,
    val process: String
)
data class DbPatientData(
    val patientType: String,
    val idType: String,
    val idNumber: String,
    val givenName: String,
    val middleName: String,
    val familyName: String,
    val gender: String,
    val dob: String,
    val email: String,
    val contactNumber: String,
    val occupation : String,
    val employerName: String,
    val homeaddress: String,
    val patientFacilityLocation: String,
    val nok: List<DbKinNext>,
    val relationship: List<DbRelationship>
)
data class DbKinNext(
    val idType : String,
    val nokIdNumber : String,
    val nokFullName : String,
    val nokRelationship : String,
    val nokContact : String,
    val nokEmail : String,
)
data class DbRelationship(
    val patientId: String,
    val relationship: String
)
enum class NotificationDetails(){
    SYSTEM,
    AUTHENTICATION,
    PATIENT_ACTIVATION
}
data class DbResults(
    val message: String
)
data class DbPatientDetails(

    val id: String,
    val emailAddress: String,
    val firstName:String,
    val lastName:String,
    val phoneNumber: String,
    val dateOfBirth: String,
    val patientIdentificationNo: String?,
    val openMrsId: String?,
    val keycloakId: String,
    val gender: String,
    val username: String,
    val patient: Boolean

)
data class DbOpenMrsResult(
    val results: List<DbOpenMrsInfo>
)
data class DbOpenMrsInfo(
    val person: DbOpenMrsData
)
data class DbOpenMrsData(
    val display: String,
    val gender: String,
    val age: String,
    val birthdate: String,
    val birthdateEstimated: String,
    val dead: String,
    val deathDate: String,
    val causeOfDeath: String,
    val birthtime: String,
    val deathdateEstimated: String,
)
data class DbOpenMrsLocal(
    val personDetails : Any,
    val patientDetails: Any?
)
data class DbUserRole(
    val keyCloakId: String,
    val roleName: String
)
data class DbConsent(
    val language: String,
    val plannedOperation: String,
    val patientName: String,
    val nurseName: String,
    val patientRelationship: String,
    val signature: String,
    val witnessName: String,
)
