package com.intellisoft.botswanaemrauthentication

import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import retrofit2.Retrofit
import javax.annotation.PostConstruct


@Component
class NetworkCall() {

    @Autowired
    lateinit var appProperties: AppProperties

    var restTemplate = RestTemplate()

    @Autowired
    private val retrofit: Retrofit? = null

    private var networkRequestInterface: NetworkRequestInterface? = null

    @PostConstruct
    fun setup() {
        networkRequestInterface = retrofit?.create(NetworkRequestInterface::class.java)
    }


    fun createNotification(type: String, dbNotification: DbNotification) {

        CoroutineScope(Dispatchers.IO).launch { sendNotification(type, dbNotification) }

    }
    private suspend fun sendNotification(type: String, dbNotification: DbNotification){

        coroutineScope {
            launch(Dispatchers.IO){
                val notificationUrl = appProperties.notificationUrl
                val headers = HttpHeaders()
                headers.contentType = MediaType.APPLICATION_JSON
                val url = when (type) {
                    "CREATE_NOTIFICATION" -> {
                        "/service/notification/create"
                    }
                    else -> {
                        ""
                    }
                }


                val results = restTemplate.postForObject(
                    "$notificationUrl$url", dbNotification, Any::class.java
                )
                println("------ $results")
            }
        }

    }

    fun getPatientDetails(openMrsId: String) = runBlocking { getBacPatientDetails(openMrsId) }
    private suspend fun getBacPatientDetails(openMrsId: String): Results{

        var details: Any
        var code: Int

        try {
            val retrofitCall = networkRequestInterface?.getUserDetails(openMrsId, "default")
            if (retrofitCall != null){

                if (retrofitCall.isSuccessful){

                    code = 200
                    details = retrofitCall.body()!!

                }else{

                    val errorCode = retrofitCall.code()
                    if (errorCode == 500){
                        code = 500
                        details = "There was an issue connecting. Please try again after sometime."
                    }else{
                        code = 400
                        details = "There is an issue processing the request."
                    }

                }

            }else{
                code = 400
                details = "The requested resource could not be found."
            }
        }catch (e: Exception){
            code = 400
            details = "We could not process your request at the moment. Please try again after sometime."

        }

        return Results(code, details)

    }



}