package com.intellisoft.botswanaemrauthentication

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface NetworkRequestInterface {

    @POST("openmrs/ws/rest/v1/person")
    fun createPerson(@Body dbPerson: DbPerson): Call<DbPersonSuccess>

    @POST("openmrs/ws/rest/v1/patient")
    fun createPatientFromPerson(@Body dbPatient: DbPatient): Call<DbPersonSuccess>

    @POST("api/v1/create")
    fun createNotification(@Body dbNotification:DbNotification):Call<Results>

    @GET("patient")
    suspend fun getUserDetails(@Query("q") q: String, @Query("v") v: String):Response<DbOpenMrsResult>

}