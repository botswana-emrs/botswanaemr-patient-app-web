package com.intellisoft.botswanaemrfilestorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotswanaEmrFileStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(BotswanaEmrFileStorageApplication.class, args);
    }

}
