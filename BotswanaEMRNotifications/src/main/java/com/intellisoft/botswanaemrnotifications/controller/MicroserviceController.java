package com.intellisoft.botswanaemrnotifications.controller;

import com.intellisoft.botswanaemrnotifications.DbNotification;
import com.intellisoft.botswanaemrnotifications.FormatterClass;
import com.intellisoft.botswanaemrnotifications.Results;
import com.intellisoft.botswanaemrnotifications.service.NotificationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import retrofit2.http.POST;

import java.util.Optional;

@RequestMapping(value = "/service/notification/")
@RestController
public class MicroserviceController {

    FormatterClass formatterClass = new FormatterClass();

    @Autowired
    private NotificationServiceImpl notificationService;

    @PostMapping(value = "create")
    public ResponseEntity<?> createService(@RequestBody DbNotification dbNotification){

        Results results = notificationService.createNotification(dbNotification);
        return formatterClass.getResponse(results);
    }

    @GetMapping(value = "user-notification")
    public ResponseEntity<?> getUserNotifications(@Param("userId") String userId){

        Results results = notificationService.getMyNotification(userId);
        return formatterClass.getResponse(results);
    }

}
