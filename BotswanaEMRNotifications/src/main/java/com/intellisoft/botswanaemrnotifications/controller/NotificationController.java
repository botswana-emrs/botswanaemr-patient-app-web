package com.intellisoft.botswanaemrnotifications.controller;

import com.intellisoft.botswanaemrnotifications.DbNotification;
import com.intellisoft.botswanaemrnotifications.DbResults;
import com.intellisoft.botswanaemrnotifications.FormatterClass;
import com.intellisoft.botswanaemrnotifications.Results;
import com.intellisoft.botswanaemrnotifications.service.NotificationServiceImpl;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping(value = "/api/v1")
@RestController
public class NotificationController {

    FormatterClass formatterClass = new FormatterClass();


    @Autowired
    private NotificationServiceImpl notificationService;


    @PostMapping(path = "/create")
    public ResponseEntity<?> createNotification(@RequestBody DbNotification dbNotification){
        Results results = notificationService.createNotification(dbNotification);
        return formatterClass.getResponse(results);

    }

    @GetMapping(value = "/user-notification")
    public ResponseEntity<?> getUserNotifications(){

        Optional<String> userId = formatterClass.getCurrentUserLogin();
        Results results;
        if (userId.isPresent()){
            results = notificationService.getMyNotification(userId.get());
        }else {
            results = new Results(400, "We could not find the user");
        }
        return formatterClass.getResponse(results);


    }

//    @GetMapping(value = "/user-notification")
//    public ResponseEntity<?> getUserFilteredNotifications(
//            @Param("userId") String userId,
//            @Param("filter") String filter
//            ){
//
//        Results results = notificationService.getFilteredNotification(userId, filter);
//        return formatterClass.getResponse(results);
//    }

    @GetMapping(value = "/notification-details")
    public ResponseEntity<?> getUserNotificationDetails(
            @Param("notificationId") String notificationId){

        Results results = notificationService.getNotificationDetails(notificationId);
        return formatterClass.getResponse(results);
    }
    @PutMapping(value = "/notification-status")
    public ResponseEntity<?> updateNotification(
            @Param("notificationId") String notificationId,
            @Param("status") String status
            ){

        Results results = notificationService.updateNotification(notificationId, status);
        return formatterClass.getResponse(results);
    }


    



}
