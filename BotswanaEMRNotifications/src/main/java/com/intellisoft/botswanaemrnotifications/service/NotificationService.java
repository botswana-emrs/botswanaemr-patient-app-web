package com.intellisoft.botswanaemrnotifications.service;

import com.intellisoft.botswanaemrnotifications.DbNotification;
import com.intellisoft.botswanaemrnotifications.Results;
import com.intellisoft.botswanaemrnotifications.entity.Notification;

public interface NotificationService {

    Results createNotification(DbNotification notification);
    Results getMyNotification(String userId);
    Results getNotificationDetails(String notificationId);
    Results updateNotification(String notificationId,  String status);
    Results getFilteredNotification(String userId, String filter);

}
