package com.intellisoft.botswanaemrnotifications.service;

import com.intellisoft.botswanaemrnotifications.*;
import com.intellisoft.botswanaemrnotifications.entity.Notification;
import com.intellisoft.botswanaemrnotifications.repository.NotificationRepository;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl implements NotificationService{

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NetworkCall networkCall = new NetworkCall();

    @Override
    public Results createNotification(DbNotification notification) {

        Notification notificationSaved = saveNotification(notification);
        return new Results(201, notificationSaved);
    }

    @Override
    public Results getMyNotification(String keycloakId) {

        PatientDetails patientDetails = networkCall.getUserDetails(keycloakId);

        if (patientDetails != null){

            String userId = patientDetails.getId();
            List<Notification> notificationList = getAllUserNotifications(userId);
            return new Results(200, notificationList);
        }else {
            return new Results(400, "User not found.");
        }


    }

    @Override
    public Results getNotificationDetails(String notificationId) {

        Notification notification = getNotificationInfo(notificationId);
        if (notification != null){
            return new Results(200, notification);
        }else {
            return new Results(204, "The notification cannot be found");
        }
    }

    @Override
    public Results updateNotification(String notificationId, String status) {

        Notification notification = getNotificationInfo(notificationId);

        if (notification != null){

            Notification notificationUpdate = updateNotificationStatus(notification, status);

            if (notificationUpdate != null){
                return new Results(200, new DbResults("The notifications has been updated successfully."));
            }else {
                return new Results(204, new DbResults("The notification cannot be found"));
            }

        }else {
            return new Results(204, new DbResults("The notification cannot be found"));
        }



    }

    @Override
    public Results getFilteredNotification(String userId, String filter) {

        List<Notification> notificationList = getNotificationFilter(userId, filter);
        return new Results(200, notificationList);
    }

    private List<Notification> getNotificationFilter(String userId, String filter){

        return getAllUserNotifications(userId);

    }

    private Notification saveNotification(DbNotification dbNotification){

        Notification notification = new Notification(
                dbNotification.getTitle(),
                dbNotification.getMessage(),
                dbNotification.getUserId(),
                dbNotification.getNotificationType(),
                false,
                dbNotification.getProcess()
        );


        return notificationRepository.save(notification);
    }

    private Notification updateNotificationStatus(Notification notification, String status){

        String id = notification.getId();

        if (status.equalsIgnoreCase("read")){
            notification.setRead(true);
        }
        if (status.equalsIgnoreCase("unread")){
            notification.setRead(false);
        }

        return notificationRepository.findById(id)
                .map(notificationOld ->{
                    notificationOld.setRead(notification.isRead());
                    notificationOld.setMessage(notification.getMessage());
                    notificationOld.setNotificationType(notification.getNotificationType());
                    notificationOld.setProcess(notification.getProcess());
                    return notificationRepository.save(notificationOld);
                }).orElse(null);

    }

    private Notification getNotificationInfo(String notificationId){
        Optional<Notification> optionalGroups = notificationRepository.findById(notificationId);
        return optionalGroups.orElse(null);
    }

    private List<Notification> getAllUserNotifications(String userId){

        return notificationRepository.findAllByUserId(userId);

    }

    private List<Notification> getPagedUserNotification(int pageNo, int pageSize, String sortField, String sortDirection) {
        String sortPageField = "";
        String sortPageDirection = "";

        if (sortField.equals("")){sortPageField = "createdAt"; }else {sortPageField = sortField;}
        if (sortDirection.equals("")){sortPageDirection = "DESC"; }else {sortPageDirection = sortField;}

        Sort sort = sortPageDirection.equalsIgnoreCase(Sort.Direction.ASC.name())
                ? Sort.by(sortPageField).ascending() : Sort.by(sortPageField).descending();
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<Notification> page = notificationRepository.findAll(pageable);



        return page.getContent();
    }
}
