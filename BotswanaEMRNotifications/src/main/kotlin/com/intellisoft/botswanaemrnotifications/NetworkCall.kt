package com.intellisoft.botswanaemrnotifications

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class NetworkCall() {

    @Autowired
    lateinit var appProperties: AppProperties

    var restTemplate = RestTemplate()


    fun getUserDetails(keycloakId: String) : PatientDetails?{

        val notificationUrl = appProperties.authenticationUrl
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val url = "/service/authentication/user/$keycloakId"

        return try {

            val results = restTemplate.getForEntity("$notificationUrl$url", PatientDetails::class.java, 1)
            if (results.statusCode == HttpStatus.OK){
                println("----- ${results.body}")
                results.body
            }else{
                null
            }

        }catch (e: Exception){
            null
        }



    }



}