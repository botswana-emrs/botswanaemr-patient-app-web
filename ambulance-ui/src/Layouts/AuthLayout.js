import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import routes from 'authroutes';
import AuthContainer from 'components/AuthContainer';
import styled from 'styled-components';

const LoginImg = styled.div`
  background-image: url('https://images.unsplash.com/photo-1576091160399-112ba8d25d1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2340&q=80');
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: 100vh;
  width: 100%;
  @media (max-width: 768px) {
    display: none;
  }
`;

export default function AuthLayout() {
  const { userInfo } = useSelector(state => state.userSignin);

  useEffect(() => {
    if (userInfo) {
      // window.location.href = '/app/dashboard';
    }
  }, [userInfo]);

  return (
    <>
      <div className='min-h-full h-screen grid gap-4 md:grid-cols-2 lg:grid-cols-2 sm:grid-cols-1'>
        <div className='flex flex-col justify-center items-center'>
          <Routes>
            {routes.map((prop, key) => {
              return (
                <Route
                  path={prop.path}
                  element={
                    <AuthContainer
                      title={prop.title}
                      width={prop.width || 'auth-login-form'}
                    >
                      <prop.component />
                    </AuthContainer>
                  }
                  key={key}
                />
              );
            })}
          </Routes>
        </div>
        <LoginImg />
      </div>
    </>
  );
}
