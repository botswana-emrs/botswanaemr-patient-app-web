import { Fragment, useEffect, useState } from 'react';
import {
  Routes,
  Route,
  useNavigate,
  Link,
  useLocation,
} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Dialog, Menu, Transition, Popover } from '@headlessui/react';
import { BellIcon, MenuAlt1Icon, XIcon } from '@heroicons/react/outline';
import { logout } from 'redux/actions/userActions';
import { getNotifications } from 'redux/actions/notificationActions';
import {
  UserCircleIcon,
  SearchIcon,
  HomeIcon,
  ClipboardListIcon,
  UsersIcon,
  OfficeBuildingIcon,
  CogIcon,
} from '@heroicons/react/solid';
import logo from 'assets/img/botswana.png';
import routes from 'appRoutes';
import { Button } from 'components';

const navigation = [
  {
    name: 'Dashboard',
    href: '/app/dashboard',
    icon: 'fa-laptop-medical',
    current: true,
  },
  {
    name: 'Ambulance Requests',
    href: '/app/ambulance-requests',
    icon: 'fa-truck-medical',
    current: false,
  },
  {
    name: 'Ambulance Management',
    href: '#',
    icon: 'fa-briefcase-medical',
    current: false,
  },
  {
    name: 'User Management',
    href: '/app/facilities',
    icon: 'fa-users',
    current: false,
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function DashboardLayout() {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const { userInfo } = useSelector(state => state.userSignin);
  const { notifications } = useSelector(state => state.getNotifications);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  const hasUnreadNotification = notifications => {
    return (
      notifications.filter(notification => notification.read === false).length >
      0
    );
  };

  useEffect(() => {
    if (!userInfo) {
      // navigate('/auth/login');
    }
    if (userInfo) {
      console.log(userInfo?.user_details?.patientIdentificationNo);
      dispatch(
        getNotifications(userInfo?.user_details?.patientIdentificationNo)
      );
    }
  }, [userInfo]);

  return (
    <>
      <Popover className='min-h-full relative '>
        <Transition.Root show={sidebarOpen} as={Fragment}>
          <Dialog
            as='div'
            className='relative z-[9999] lg:hidden'
            onClose={setSidebarOpen}
          >
            <Transition.Child
              as={Fragment}
              enter='transition-opacity ease-linear duration-300'
              enterFrom='opacity-0'
              enterTo='opacity-100'
              leave='transition-opacity ease-linear duration-300'
              leaveFrom='opacity-100'
              leaveTo='opacity-0'
            >
              <div className='fixed inset-0 bg-gray-600 bg-opacity-75' />
            </Transition.Child>

            <div className='fixed inset-0 flex z-[9999]'>
              <Transition.Child
                as={Fragment}
                enter='transition ease-in-out duration-300 transform'
                enterFrom='-translate-x-full'
                enterTo='translate-x-0'
                leave='transition ease-in-out duration-300 transform'
                leaveFrom='translate-x-0'
                leaveTo='-translate-x-full'
              >
                <Dialog.Panel className='relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-[white]'>
                  <Transition.Child
                    as={Fragment}
                    enter='ease-in-out duration-300'
                    enterFrom='opacity-0'
                    enterTo='opacity-100'
                    leave='ease-in-out duration-300'
                    leaveFrom='opacity-100'
                    leaveTo='opacity-0'
                  >
                    <div className='absolute top-0 right-0 -mr-12 pt-2'>
                      <button
                        type='button'
                        className='ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-light'
                        onClick={() => setSidebarOpen(false)}
                      >
                        <span className='sr-only'>Close sidebar</span>
                        <XIcon
                          className='h-6 w-6 text-light'
                          aria-hidden='true'
                        />
                      </button>
                    </div>
                  </Transition.Child>
                  <div className='flex-shrink-0 flex flex-col justify-center items-center px-4'>
                    <img className='w-1/3' src={logo} alt='Easywire logo' />
                  </div>
                  <nav
                    className='mt-5 flex-shrink-0 h-full divide-y divide-cyan-800 overflow-y-auto'
                    aria-label='Sidebar'
                  >
                    <div className='px-2 space-y-1'>
                      {navigation.map(item => (
                        <Link
                          key={item.name}
                          to={item.href}
                          className={classNames(
                            pathname.includes(item.href)
                              ? 'bg-blue-dark text-light'
                              : 'text-cyan-100 hover:text-light hover:bg-blue-dark',
                            'group flex items-center px-2 py-2 text-base font-medium rounded-md'
                          )}
                          aria-current={
                            pathname.includes(item.href) ? 'page' : undefined
                          }
                        >
                          <i
                            className={`fa-solid ${item.icon} mr-4 flex-shrink-0 h-6 w-6 text-cyan-200`}
                          ></i>
                          {item.name}
                        </Link>
                      ))}
                    </div>
                  </nav>
                </Dialog.Panel>
              </Transition.Child>
              <div className='flex-shrink-0 w-14' aria-hidden='true'>
                {/* Dummy element to force sidebar to shrink to fit close icon */}
              </div>
            </div>
          </Dialog>
        </Transition.Root>

        {/* Static sidebar for desktop */}
        <div className='hidden lg:flex lg:w-64 lg:flex-col lg:fixed lg:inset-y-0'>
          {/* Sidebar component, swap this element with another sidebar if you like */}
          <div className='flex flex-col flex-grow  pt-5 pb-4 overflow-y-auto'>
            <div className='flex flex-col items-center justify-center flex-shrink-0 px-4'>
              <img className='w-1/2' src={logo} alt='Easywire logo' />
              <h5>BotswanaEMR</h5>
            </div>
            <nav
              className='mt-5 flex-1 flex flex-col divide-y  overflow-y-auto'
              aria-label='Sidebar'
            >
              <div className='px-2 space-y-1'>
                {navigation.map(item => (
                  <Link
                    key={item.name}
                    to={item.href}
                    className={classNames(
                      pathname.includes(item.href)
                        ? 'text-blue-dark'
                        : 'text-gray-600 hover:text-blue-dark',
                      'group flex items-center px-2 py-2 text-sm leading-6 font-medium rounded-md'
                    )}
                    aria-current={
                      pathname.includes(item.href) ? 'page' : undefined
                    }
                  >
                    <i
                      className={`fa-solid ${item.icon} mr-4 flex-shrink-0 h-6 w-6 text-cyan-200`}
                    ></i>
                    {/* <item.icon
                      className='mr-4 flex-shrink-0 h-6 w-6 text-cyan-200'
                      aria-hidden='true'
                    /> */}
                    {item.name}
                  </Link>
                ))}
              </div>
            </nav>
          </div>
        </div>

        <div className='lg:pl-64 flex flex-col flex-1'>
          <div className='relative z-10 flex-shrink-0 flex h-16 bg-blue-dark border-b border-light-300 lg:border-none'>
            <button
              type='button'
              className='px-4 border-r border-light-300 text-light-300 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500 lg:hidden'
              onClick={() => setSidebarOpen(true)}
            >
              <span className='sr-only'>Open sidebar</span>
              <MenuAlt1Icon className='h-6 w-6' aria-hidden='true' />
            </button>
            {/* Search bar */}
            <div className='flex-1 px-4 flex justify-between sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
              <div className='flex-1 flex hos-name'>
                <h1>Princess Marina Hospital</h1>
              </div>
              <div className='ml-4 flex items-center md:ml-6'>
                <Menu as='div' className='ml-3 relative'>
                  <div className='user-icon'>
                    <Menu.Button className='max-w-xs  rounded-full flex items-center text-sm focus:outline-none focus:ring-0  focus:ring-cyan-500 lg:p-2 lg:rounded-md '>
                      {/* <img
                        className='h-8 w-8 rounded-full'
                        src={UserCircleIcon}
                        alt=''
                      /> */}
                      <UserCircleIcon
                        className='h-10 w-10 ml-2 mr-2 text-light'
                        aria-hidden='true'
                      />
                    </Menu.Button>
                    <div className='username'>
                      <h4>
                        Samuel M. <small>Good Morning</small>
                      </h4>
                    </div>
                  </div>
                </Menu>
                <Popover className='relative'>
                  {({ open }) => (
                    <>
                      <Popover.Button className='p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-0'>
                        <span className='sr-only'>View notifications</span>
                        <BellIcon
                          className='h-6 w-6 text-light'
                          aria-hidden='true'
                        />
                        {notifications && hasUnreadNotification(notifications) && (
                          <svg
                            className='-ml-0.5 mr-1 h-3 w-3 text-red absolute right-0 top-0'
                            fill='currentColor'
                            viewBox='0 0 8 8'
                          >
                            <circle cx={4} cy={4} r={3} />
                          </svg>
                        )}
                      </Popover.Button>

                      <Transition
                        as={Fragment}
                        enter='transition ease-out duration-200'
                        enterFrom='opacity-0 translate-y-1'
                        enterTo='opacity-100 translate-y-0'
                        leave='transition ease-in duration-150'
                        leaveFrom='opacity-100 translate-y-0'
                        leaveTo='opacity-0 translate-y-1'
                      >
                        <Popover.Panel className='absolute z-[9999] -ml-4 mt-3 transform px-0 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2'>
                          <div className='rounded-lg shadow-lg overflow-hidden'>
                            <div className='relative grid  bg-light px-0 py-6  sm:py-8'>
                              {notifications?.slice(0, 5).map((item, i) => (
                                <Link
                                  key={i}
                                  to={'#'}
                                  className='px-3 py-4 flex items-start bg-lblue-100 border-b border-gray-200'
                                >
                                  <div className='h-14 w-14 rounded-md flex justify-center items-center  relative'>
                                    <i className='fas fa-solid fa-calendar-plus text-blue-dark text-3xl'></i>
                                    {!item.read && (
                                      <svg
                                        className='-ml-0.5 mr-1 h-3 w-3 text-red absolute right-0 top-1'
                                        fill='currentColor'
                                        viewBox='0 0 8 8'
                                      >
                                        <circle cx={4} cy={4} r={3} />
                                      </svg>
                                    )}
                                  </div>
                                  <div className='ml-4'>
                                    <p className='text-xs font-medium text-blue-dark'>
                                      {item?.title}
                                    </p>
                                    <p className='mt-1 text-xs'>
                                      {item?.message}
                                    </p>
                                  </div>
                                </Link>
                              ))}
                            </div>
                            <div className='px-5 pb-5 bg-gray-50 justify-end space-y-6 flex sm:space-y-0 sm:space-x-10 sm:px-8'>
                              <Button
                                onClick={() => navigate('/app/notifications')}
                                size='small'
                              >
                                View All({notifications?.length})
                              </Button>
                            </div>
                          </div>
                        </Popover.Panel>
                      </Transition>
                    </>
                  )}
                </Popover>

                {/*  */}

                {/* Profile dropdown */}
                <Menu as='div' className='ml-3 relative'>
                  <div>
                    <Menu.Button className='max-w-xs  rounded-full flex items-center text-sm focus:outline-none focus:ring-0  focus:ring-cyan-500 lg:p-2 lg:rounded-md '>
                      {/* <img
                        className='h-8 w-8 rounded-full'
                        src={UserCircleIcon}
                        alt=''
                      /> */}
                      <UserCircleIcon
                        className='h-8 w-8 ml-2 text-light'
                        aria-hidden='true'
                      />
                    </Menu.Button>
                  </div>
                  <Transition
                    as={Fragment}
                    enter='transition ease-out duration-100'
                    enterFrom='transform opacity-0 scale-95'
                    enterTo='transform opacity-100 scale-100'
                    leave='transition ease-in duration-75'
                    leaveFrom='transform opacity-100 scale-100'
                    leaveTo='transform opacity-0 scale-95'
                  >
                    <Menu.Items className='origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-[white] ring-0 ring-none ring-opacity-5 focus:outline-none'>
                      <Menu.Item>
                        {({ active }) => (
                          <a
                            href='#'
                            className={classNames(
                              active ? 'bg-gray-100' : '',
                              'block px-4 py-2 text-sm text-gray-700'
                            )}
                          >
                            Your Profile
                          </a>
                        )}
                      </Menu.Item>
                      <Menu.Item>
                        {({ active }) => (
                          <a
                            href='#'
                            className={classNames(
                              active ? 'bg-gray-100' : '',
                              'block px-4 py-2 text-sm text-gray-700'
                            )}
                          >
                            Settings
                          </a>
                        )}
                      </Menu.Item>
                      <Menu.Item>
                        {({ active }) => (
                          <Link
                            to='/#'
                            className={classNames(
                              active ? 'bg-gray-100' : '',
                              'block px-4 py-2 text-sm text-gray-700'
                            )}
                            onClick={() => dispatch(logout())}
                          >
                            Logout
                          </Link>
                        )}
                      </Menu.Item>
                    </Menu.Items>
                  </Transition>
                </Menu>
              </div>
            </div>
          </div>
          <main className='flex-1 pb-8 px-4 sm:px-6  lg:px-84 bg-light h-full min-h-[calc(100vh-64px)]'>
            <Routes>
              {routes.map((route, index) => (
                <Route key={index} {...route} />
              ))}
            </Routes>
          </main>
        </div>
      </Popover>
    </>
  );
}
