import React, { useState } from 'react';
import { Tab, Breadcrumb, Card, Button } from 'components';
import AppointmentsList from './AppointmentsList';
import GoogleMap from 'components/GoogleMap';
import Stack from 'components/Stack';
import { Modal } from 'components';
import { format } from 'date-fns/esm';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { Input } from 'components';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Appointments', href: '/app/appointments', current: true },
];

const approveAmbulanceSchema = Yup.object().shape({
  ambulanceRegistration: Yup.string().required('Ambulance registration is required'),
  pickupLocation: Yup.string().required('Pickup location is required'),
  dropOffLocation: Yup.string().required('Drop off location is required'),
});

const rejectAmbulanceSchema = Yup.object().shape({
  reason: Yup.string().required('Reason is required'),
})

export default function Appointments() {
  const [approveOpen, setApproveOpen] = useState(false);
  const [rejectOpen, setRejectOpen] = useState(false);

  const formik = useFormik({
    initialValues: {
      ambulanceRegistration: '',
      pickupLocation: '',
      dropOffLocation: '',
    },
    validationSchema: approveAmbulanceSchema,
    onSubmit: (values) => {
      console.log(values);
    }
  });

  const rejectFormik = useFormik({
    initialValues: {
      reason: '',
    },
    validationSchema: rejectAmbulanceSchema,
    onSubmit: (values) => {
      console.log(values);
    }
  });
  const requestDetails = {
    Date: format(new Date(), 'MMM dd, yyyy hh:mm a'),
    Location: 'Mabopane',
    Status: 'Approved',
    'Incident Details': 'Patient has been involved in a car accident',
  };

  const ambulanceDetails = {
    Registration: 'ND 1234',
    'Estimated Time': '10 minutes',
    'Drop Off Location': 'Mabopane',
  };
  return (
    <div>
      <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
        <div className='py-6 md:flex md:items-center md:justify-between'>
          <div className='flex-1 min-w-0'>
            {/* Profile */}
            <div className='flex items-center'>
              <div>
                <div className='flex items-center'>
                  <i className='fas fa-bell text-blue-dark'></i>
                  <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                    Ambulance Requests
                  </h1>
                </div>
              </div>
            </div>
          </div>
          <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
            <Breadcrumb pages={pages} />
          </div>
        </div>
      </div>
      <Card
        header={
          <div className='flex items-center justify-between'>
            <h3 className='text-lg leading-6 font-normal w-fit'>
              Ambulance Requests: 11211
            </h3>
            <div className='flex space-x-2'>
              <Button kind='danger' size='small' onClick={() => setRejectOpen(true)} >Reject Request</Button>
              <Button kind='success' size='small' onClick={() => setApproveOpen(true)}>
                Approve Request
              </Button>
            </div>
          </div>
        }
      >
        <div className='px-2 mb-6 grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-8'>
          <Card
            header={
              <h3 className='text-lg leading-6 w-fit font-light text-blue-dark'>
                Request Details
              </h3>
            }
            light={true}
            noShadow={true}
          >
            <Stack data={requestDetails} merge='Incident Details' />
          </Card>
          <Card
            header={
              <h3 className='text-lg leading-6 w-fit font-light text-blue-dark'>
                Ambulance Details
              </h3>
            }
            light={true}
            noShadow={true}
          >
            <Stack data={ambulanceDetails} />
          </Card>
        </div>
        <Card
          header={
            <h3 className='text-lg leading-6 ml-2 w-fit font-light text-blue-dark'>
              Map Details
            </h3>
          }
          light={true}
          noShadow={true}
        >
          <GoogleMap />
        </Card>
      </Card>

      <Modal title='Request Ambulance' open={approveOpen} setOpen={setApproveOpen}>
        <form className="space-y-8" onSubmit={formik.handleSubmit}>
          <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
            <div className="sm:col-span-6">
              <Input label="Ambulance Registration" name="ambulanceRegistration"
                onChange={formik.handleChange}
                error={formik.errors.ambulanceRegistration}
              />
            </div>
            <div className="sm:col-span-6">
              <Input label="Pickup Location" name="pickupLocation"
                onChange={formik.handleChange}
                error={formik.errors.pickupLocation}
              />
            </div>
            <div className="sm:col-span-6">
              <Input label="Drop Off Location" name="dropOffLocation"
                onChange={formik.handleChange}
                error={formik.errors.dropOffLocation}
              />
            </div>
          </div>
          <div className="mt-5 sm:mt-6 flex flex-row-reverse">
            <Button className='ml-4' kind='primary' type='submit'
            >
              Request
            </Button>
            <Button kind='light' type='button' onClick={() => setApproveOpen(false)}>Cancel</Button>
          </div>
        </form>

      </Modal>


      <Modal title='Reject Request' open={rejectOpen} setOpen={setRejectOpen}>
        <form className="space-y-8" onSubmit={rejectFormik.handleSubmit}>
          <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
            <div className="sm:col-span-6">
              <label htmlFor="reason" className="block text-sm font-light">
                Enter reasons for rejecting the request
              </label>
              <div className="mt-1">
                <textarea
                  id="reason"
                  name="reason"
                  rows={3}
                  autocomplete='off'
                  className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  onChange={rejectFormik.handleChange}
                />
              </div>
              {(rejectFormik.errors.reason && rejectFormik.touched.reason) &&
                <p className="mt-2 text-xs font-sm text-red">{
                  rejectFormik.errors.reason
                }</p>}
            </div>
          </div>
          <div className="mt-5 sm:mt-6 flex flex-row-reverse">
            <Button className='ml-4' kind='danger' type='submit' full={true}
            >
              Reject Request
            </Button>
          </div>
        </form>
      </Modal>
    </div>
  );
}
