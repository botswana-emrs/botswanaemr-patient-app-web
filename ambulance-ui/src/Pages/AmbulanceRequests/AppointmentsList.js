import React from 'react';
import { Table, Button, Empty } from 'components';
import { SearchIcon } from '@heroicons/react/outline';
import { Link } from 'react-router-dom';

export default function AppointmentsList({ appointments=[] }) {
  const columns = [
    {
      title: 'Date of Appointment',
      key: 'date',
    },
    {
      title: 'Facility Name',
      key: 'facilityName',
    },
    {
      title: 'Facility Type',
      key: 'facilityType',
    },
    {
      title: 'Doctor to Visit',
      key: 'doctorToVisit',
    },
    {
      title: 'Action',
      key: 'action',
      render: (record, _, i) => (
        <Link
          key={i}
          to={`/app/appointment/${record.id}`}
          className=' flex justify-center'
        >
          <i className='fa-solid text-lg fa-circle-arrow-right text-blue-dark'></i>
        </Link>
      ),
    },
  ];
  return (
    <div>
      <div className='flex justify-between px-6 mt-8'>
        <Button kind='primary-outline'>Create Appointment</Button>

        <div className='mt-1 relative flex items-center'>
          <input
            type='text'
            name='search'
            id='search'
            placeholder='Search Appointments'
            className='shadow-sm search-table placeholder:italic focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-12 sm:text-sm border-gray-300 rounded-md'
          />
          <div className='absolute inset-y-0 left-0 flex py-1.5 pl-1.5'>
            <kbd className='inline-flex items-center rounded px-2 text-sm font-sans font-medium text-gray-400'>
              <SearchIcon className='h-5 w-5' />
            </kbd>
          </div>
        </div>
      </div>
      <div>
        <Table columns={columns} data={appointments} kind='primary' />
        {
          appointments?.length === 0 && (
            <Empty text='No appointments' />
          )
        }
      </div>
    </div>
  );
}
