import React, { useEffect, useState } from 'react';
import {
  Button,
  Card,
  Form,
  Select,
  Breadcrumb,
  ButtonLink,
} from 'components';
import { getFacility } from 'redux/actions/facilityActions';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import Loader from 'components/Loader';
import Notification from 'components/Notification';
import Divider from 'components/Divider';
import StackedList from 'components/StackedList';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Facilities', href: '/app/facilities', current: true },
];

export default function AppointmentDetails() {
  const { facility, loading, error } = useSelector(state => state.getFacility);
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFacility(id));
  }, [dispatch, id]);

  const generateAddress = physicalAddress => {
    const {
      cityTown,
      plotNumber,
      streetName,
      buildingName,
      unitOffice,
      mailName,
      ward,
    } = physicalAddress;

    // create address string in Botswana format
    let address = '';
    if (plotNumber) {
      address += `${plotNumber}, `;
    }
    if (streetName) {
      address += `${streetName}, `;
    }
    if (buildingName) {
      address += `${buildingName}, `;
    }
    if (unitOffice) {
      address += `${unitOffice}, `;
    }
    if (mailName) {
      address += `${mailName}, `;
    }
    if (ward) {
      address += `${ward}, `;
    }
    if (cityTown) {
      address += `${cityTown} `;
    }
    return address;
  };

  return (
    <>
      {loading || !facility ? (
        <Loader />
      ) : (
        <>
          {error && <Notification kind='error' message={error} />}
          <div className='bg-gradient-to-b from-lblue-light to-light'>
            <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
              <div className='py-6 md:flex md:items-center md:justify-between'>
                <div className='flex-1 min-w-0'>
                  {/* Profile */}
                  <div className='flex items-center'>
                    <div>
                      <div className='flex items-center'>
                        <i className='fas fa-hospital text-blue-dark'></i>
                        <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                          Facilities
                        </h1>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
                  <Breadcrumb pages={pages} />
                </div>
              </div>
            </div>
          </div>

          <div className='mt-8'>
            <Card
              header={
                <div className='flex justify-between'>
                  <h3 className='text-lg leading-6 font-light text-light'>
                    {facility?.facilityName}
                  </h3>
                  <Link
                    to='/app/facilities'
                    className='flex items-center bg-light rounded px-4 text-blue-dark'
                  >
                    Back
                  </Link>
                </div>
              }
            >
           
              <div className='mt-8'>
                <h1 className='pl-2 text-md font-normal text-blue-dark'>
                  OPERATING TIMES
                </h1>
                <Divider />
                <div>
                  {facility?.operationalTimes.map((time, index) => (
                    <span
                      key={index}
                      className='inline-flex items-center px-3 py-1 mb-4 rounded-full text-xs font-sm mr-3 bg-light'
                    >
                      {`${time.day}: ${time.timeFrom} AM - ${time.breakTo} PM`}
                    </span>
                  ))}
                </div>
              </div>
              <div className='mt-8'>
                <h1 className='pl-2 text-md font-normal text-blue-dark'>
                  FACILITY INFORMATION
                </h1>
                <Divider />
                <div className='grid grid-cols-2 text-sm font-light gap-8 mt-6'>
                  <table>
                    <tbody>
                      <StackedList
                        item={{
                          key: 'District:',
                          value: facility?.district?.name || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Address:',
                          value: generateAddress(facility.physicalAddress)
                            ? generateAddress(facility.physicalAddress)
                            : 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Service Delivery Type:',
                          value: 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'EstimatedFacility Catchment Area:',
                          value: facility.catchmentArea || 'N/A',
                        }}
                      />
                    </tbody>
                  </table>
                  <table>
                    <tbody>
                      <StackedList
                        item={{
                          key: 'DHMT:',
                          value: facility?.dhmt?.name || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Facility Type:',
                          value: facility.facilityType?.name || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Facility Owner:',
                          value: facility.facilityOwner || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Facility Status:',
                          value: facility.status || 'N/A',
                        }}
                      />
                    </tbody>
                  </table>
                </div>
              </div>
            </Card>
          </div>
        </>
      )}
    </>
  );
}
