import React, { useRef, useEffect, useState } from 'react';
import Breadcrumb from 'components/Breadcrumb';
import { Link } from 'react-router-dom';
import { SearchIcon } from '@heroicons/react/outline';
import { Button, Card, Form, Select, Table } from 'components';
import {
  getFacilities,
  getDhmt,
  getFacilityType,
  getFacilityOwner,
  getDistrict,
  getFacilityConstituency,
} from 'redux/actions/facilityActions';
import { useDispatch, useSelector } from 'react-redux';
import useInputs from 'hooks/useInput';
// import { filterFacilities, searchFacilities } from 'lib/facilityFilter';
import Loader from 'components/Loader';
import Notification from 'components/Notification';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Facilities', href: '/app/facilities', current: true },
];
export default function Facilities() {
  const [facilitiesList, setFacilitiesList] = useState([]);
  const formRef = useRef(null);
  const [inputs, changeHandler, reset] = useInputs({});
  const dispatch = useDispatch();

  const { facilities, loading, error } = useSelector(
    state => state.getFacilities
  );
  const { dhmt } = useSelector(state => state.getDhmt);
  const { facilityType } = useSelector(state => state.getFacilityType);
  const { facilityOwner } = useSelector(state => state.getFacilityOwner);
  const { district } = useSelector(state => state.getDistrict);
  const { facilityConstituency } = useSelector(
    state => state.getFacilityConstituency
  );

  const handleReset = () => {
    formRef.current.reset();
    reset();
    setFacilitiesList(facilities);
  };

  useEffect(() => {
    if (!facilities) {
      dispatch(getFacilities());
      dispatch(getDhmt());
      dispatch(getFacilityType());
      dispatch(getFacilityOwner());
      dispatch(getDistrict());
      dispatch(getFacilityConstituency());
    }
  }, [dispatch]);

  useEffect(() => {
    if (facilities) {
      setFacilitiesList(facilities);
    }
  }, [facilities]);

  const handleSubmit = () => {
    // const filteredFacilities = filterFacilities(facilities, inputs);
    // setFacilitiesList(filteredFacilities);
  };

  const footer = (
    <div className='flex justify-end '>
      <button
        onClick={handleReset}
        className='inline-flex items-center px-4 py-1 mr-3 border border-none shadow text-sm font-light rounded-md text-blue-dark bg-[white] focus:outline-none focus:ring-0  focus:ring-none'
      >
        Reset Filters
      </button>
      <Button
        onClick={() => handleSubmit()}
        className='inline-flex items-center px-4 py-1 border border-none shadow text-sm font-light rounded-md text-light bg-blue-dark  focus:outline-none focus:ring-0  focus:ring-none'
      >
        Search
      </Button>
    </div>
  );

  const columns = [
    {
      title: 'Facility Name',
      key: 'facilityName',
    },
    {
      title: 'District Name',
      key: 'district.name',
      render: (record, _) => record?.district?.name ?? '-',
    },
    {
      title: 'DHMT',
      key: 'dhmt.name',
      render: (record, _) => record?.dhmt?.name ?? '-',
    },
    {
      title: 'Facility Type',
      key: 'facilityType.name',
      render: (record, _) => record?.facilityType?.name ?? '-',
    },
    {
      title: 'Facility Owner',
      key: 'facilityOwner',
    },
    {
      title: 'Action',
      key: 'action',
      render: (item, _, i) => (
        <Link
          key={i}
          to={`/app/facilities/${item.newFacilityCode}`}
          className=' flex justify-center'
        >
          <i className='fa-solid text-lg fa-circle-arrow-right text-blue-dark'></i>
        </Link>
      ),
    },
  ];

  return (
    <>
      {' '}
      {loading ? (
        <Loader />
      ) : (
        <>
          {error && <Notification kind='error' message={error} />}
          <div className='bg-gradient-to-b from-lblue-light to-light'>
            <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
              <div className='py-6 md:flex md:items-center md:justify-between'>
                <div className='flex-1 min-w-0'>
                  {/* Profile */}
                  <div className='flex items-center'>
                    <div>
                      <div className='flex items-center'>
                        <i className='fas fa-hospital text-blue-dark'></i>
                        <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                          Facilities
                        </h1>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
                  <Breadcrumb pages={pages} />
                </div>
              </div>
            </div>
          </div>

          <div className='mt-8'>
            <Card
              header={
                <h3 className='text-lg leading-6 font-light text-light'>
                  Search Facilities
                </h3>
              }
              footer={footer}
            >
              <div className='facility-map'>
                {/* <Map
                  facilities={facilitiesList?.filter(
                    item => item.lat && item.lng
                  )}
                /> */}
              </div>
              {/* create a grid with 3 columns */}
              <Form
                className='my-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6'
                ref={formRef}
                id='facility-search'
                // onSubmit={()=>handleSubmit}
              >
                <div className='sm:col-span-2'>
                  <Select
                    id='district'
                    name='district'
                    placeholder='Filter by Facility District'
                    onChange={changeHandler}
                  >
                    {district?.map(item => (
                      <option key={item.id} value={item.name}>
                        {item.name}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className='sm:col-span-2'>
                  <Select
                    id='facilityOwner'
                    name='facilityOwner'
                    placeholder='Filter by Facility Ownership'
                    onChange={changeHandler}
                  >
                    {facilityOwner?.map(item => (
                      <option key={item.id} value={item.name}>
                        {item.name}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className='sm:col-span-2'>
                  <Select
                    id='facilityType'
                    name='facilityType'
                    placeholder='Filter by Facility Type'
                    onChange={changeHandler}
                  >
                    {facilityType?.map(item => (
                      <option key={item.id} value={item.name}>
                        {item.name}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className='sm:col-span-2'>
                  <Select
                    id='dhmt'
                    name='dhmt'
                    placeholder='Filter by facility DHMT'
                    onChange={changeHandler}
                  >
                    {dhmt?.map(item => (
                      <option key={item.id} value={item.name}>
                        {item.name}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className='sm:col-span-2'>
                  <Select
                    id='constituency'
                    name='constituency'
                    placeholder='Filter by Facility Constituency'
                    onChange={changeHandler}
                  >
                    {facilityConstituency?.map(item => (
                      <option key={item.id} value={item.name}>
                        {item.name}
                      </option>
                    ))}
                  </Select>
                </div>
              </Form>
            </Card>
            <div className='mt-5'>
              <Card
                header={
                  <h3 className='text-lg leading-6 font-light text-light'>
                    Facility List
                  </h3>
                }
              >
                <div className='px-4 sm:px-6 lg:px-8 relative flex justify-end w-full text-gray-400 focus-within:text-gray-600'>
                  <div
                    className='absolute inset-y-0 left-0 flex items-center pointer-events-none'
                    aria-hidden='true'
                  >
                    <SearchIcon
                      className='h-5 w-5 ml-2'
                      aria-hidden='true'
                      color='white'
                    />
                  </div>
                  <input
                    type='text'
                    className='focus:ring-blue-dark focus:border-none block w-1/4 sm:text-sm border-none shadow rounded-sm'
                    placeholder='Search Facilities'
                    // onChange={e =>
                    //   setFacilitiesList(
                    //     searchFacilities(facilities, e.target.value)
                    //   )
                    // }
                  />
                </div>
                <Table columns={columns} data={facilitiesList} />
              </Card>
            </div>
          </div>
        </>
      )}
    </>
  );
}
