export const mockFacilities = [
  {
    id: 1,
    name: 'Good Hope Primary Hospital',
    district: 'Mabustane',
    dhmt: 'N/A',
    owner: 'Private',
    type: 'Poly Clinic',
    coordinates: [-24.6282, 25.8552508],
    contact: '+27 (0)3 684 888 888',
  },
];
