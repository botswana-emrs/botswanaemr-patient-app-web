import React, { useState } from 'react';

const tabs = [
  { name: 'Facility Services', key: 'facilityServices' },
  { name: 'Infrastructure', key: 'knownYesNoInfrastructures' },
  { name: 'Staff', key: '' },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Services({ facility }) {
  const [activeTab, setActiveTab] = useState(0);
  return (
    <div className='my-6'>
      <div className='sm:hidden'>
        <label htmlFor='tabs' className='sr-only'>
          Select a tab
        </label>
        <select
          id='tabs'
          name='tabs'
          className='block w-full focus:ring-blue-dark focus:border-blue-dark border-blue-dark rounded-md'
          defaultValue={tabs[activeTab].name}
        >
          {tabs.map(tab => (
            <option key={tab.name}>{tab.name}</option>
          ))}
        </select>
      </div>
      <div className='hidden sm:block'>
        <div className='border-b border-blue-dark'>
          <nav className='-mb-px flex' aria-label='Tabs'>
            {tabs.map((tab, index) => (
              <p
                className={classNames(
                  index === activeTab
                    ? 'border-b-2 text-blue-dark'
                    : 'border-b text-blue-dark',
                  'w-1/3 border-blue-dark py-4 px-1 text-center font-medium cursor-pointer text-sm'
                )}
                aria-current={index === activeTab ? 'page' : undefined}
                onClick={() => setActiveTab(index)}
              >
                {tab.name}
              </p>
            ))}
          </nav>
        </div>
      </div>
      {activeTab === 0 && (
        // create div boxes styles like a table with 4 cols in each row
        <div className='grid grid-cols-4 mt-4'>
          {facility?.facilityServices?.map((service, index) => (
            <div key={index} className='border border-collapse py-2 px-3'>
              <div className='text-sm font-light'>{service.name}</div>
            </div>
          ))}
        </div>
      )}

      {activeTab === 1 && (
        <div className='grid grid-cols-4 mt-4'>
          {facility?.knownYesNoInfrastructures?.map((infrastructure, index) => (
            <div key={index} className='border border-collapse py-2 px-3'>
              <div className='text-sm font-light'>{infrastructure.name}</div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}
