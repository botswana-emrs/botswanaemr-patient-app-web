import React, { useState } from 'react';
import { SearchIcon, ArrowLeftIcon } from '@heroicons/react/outline';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Button, Modal, Table } from 'components';
import { formatDistanceToNow } from 'date-fns';
import * as yup from 'yup';
import { useFormik } from 'formik';
import { Input } from 'components';

const people = [
  {
    name: 'Lindsay Walton',
    title: 'Front-end Developer',
    email: 'lindsay.walton@example.com',
    role: 'Member',
  },
];

const ambulanceSchema = yup.object().shape({
  patientName: yup.string().required('Patient name is required'),
  patientId: yup.string().required('Patient ID is required'),
  pickUpLocation: yup.string().required('Pick up location is required'),
  dropOffLocation: yup.string().required('Drop off location is required'),
  incidentDetails: yup.string().required('Incident details is required'),
});

export default function Home() {
  const { userInfo } = useSelector(state => state.userSignin);
  const [open, setOpen] = useState(false);

  const formik = useFormik({
    initialValues: {
      patientName: '',
      patientId: '',
      pickUpLocation: '',
      dropOffLocation: '',
      incidentDetails: '',
    },
    validationSchema: ambulanceSchema,
    onSubmit: values => {
      console.log(values);
    },
  });

  const cards = [
    {
      name: 'Appointments',
      href: '/app/appointments',
      icon: 'fa-solid fa-kit-medical',
    },
    {
      name: 'Facilities',
      href: '/app/facilities',
      icon: 'fas fa-hospital',
    },
    {
      name: 'Ambulance',
      href: '#',
      icon: 'fa-solid fa-ambulance',
    },
    {
      name: 'Your Records',
      href: '#',
      icon: 'fas fa-clipboard-list',
    },
  ];

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Filler,
    Legend
  );
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
    },
  };

  const data = {
    labels: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],
    datasets: [
      {
        data: [33, 53, 85, 41, 44, 65, 40, 43, 22, 43, 22, 43],
        fill: true,
        backgroundColor: 'rgba(246, 253, 254, 0.7)',
        borderWidth: 1,
        borderColor: '#50F2F2',
        pointRadius: 0,
      },
    ],
  };

  const columns = [
    {
      title: 'No.',
      key: 'no',
    },
    {
      title: 'Name',
      key: 'name',
      render: (record, _) => record?.district?.name ?? '-',
    },
    {
      title: 'Location',
      key: 'location',
      render: (record, _) => record?.dhmt?.name ?? '-',
    },
    {
      title: 'Ambulance Status',
      key: 'status',
      render: (record, _) => record?.facilityType?.name ?? '-',
    },
    {
      title: 'Request Time',
      key: 'requestTime',
    },
    {
      title: 'Action',
      key: 'action',
      render: (item, _, i) => (
        <Link
          key={i}
          to={`/app/ambulance/${item.newFacilityCode}`}
          className='flex justify-center text-blue-dark underline'
        >
          view
        </Link>
      ),
    },
  ];

  const announcements = [
    {
      id: 1,
      user: 'You',
      purpose: 'Screening',
      patient: {
        name: 'John Doe',
        id: '1234567890',
        gender: 'Male'
      },
      createdAt: '2021-05-01T12:00:00.000Z',
    },
    {
      id: 2,
      user: 'You',
      purpose: 'Screening',
      patient: {
        name: 'John Doe',
        id: '1234567890',
        gender: 'Male'
      },
      createdAt: '2021-05-01T12:00:00.000Z',
    },
    {
      id: 3,
      user: 'You',
      purpose: 'Screening',
      patient: {
        name: 'John Doe',
        id: '1234567890',
        gender: 'Male',
      },
      createdAt: '2021-05-01T12:00:00.000Z',
    },


  ];

  return (
    <>
      <div className='text-left'>
        <h2 className='pt-4'>
          Total Requests: <span>7</span>
        </h2>
        <div className=' bg-white rounded'>
          <div className='py-6 sm:px-6 md:flex md:items-center md:justify-between h-full home-chart'>
            <Line options={options} data={data} className='h-full' />
          </div>
          <div className='border-t border-gray-200 sm:px-6'>
            <p className='font-light text-sm text-gray p-3'>
              Daily average requests: <span>10</span>
            </p>
          </div>
        </div>
      </div>

      <div className='mt-4 grid md:grid-cols-auto-60 sm:grid-cols-1 gap-4'>
        <div className='w-full shadow bg-[white] rounded'>
          <div className='text-lg border-b border-gray-200 p-3 flex justify-between'>
            <h2>Ambulance Requests</h2>
            <Button kind='primary-outline' onClick={() => setOpen(true)}>New Request</Button>
          </div>
          <div className='mt-2 py-3'>
            <div className='px-4 sm:px-6 lg:px-8 relative flex justify-end w-full text-gray-400 focus-within:text-gray-600'>
              <div
                className='absolute inset-y-0 left-0 flex items-center pointer-events-none'
                aria-hidden='true'
              ></div>
              <input
                type='text'
                className='focus:ring-blue-dark focus:border-none block sm:text-sm border-none shadow rounded-sm mr-2 w-full'
                placeholder='Search by name, ambulance registration'
              />
              <Button kind='primary' className='mr-2'>
                <SearchIcon
                  className='h-5 w-5 mr-2'
                  aria-hidden='true'
                  color='white'
                />
                Search
              </Button>
              <Button kind='light' className='w-fit flex items-center'>
                <i class='fa-solid fa-arrow-rotate-left'></i>
              </Button>
            </div>
            <Table columns={columns} data={[]} />
          </div>
        </div>

        <div className='w-full shadow bg-[white] rounded'>
          <div className='text-lg border-b border-gray-200 p-3 flex justify-between'>
            <h2 className='py-1'>Activity</h2>
          </div>
          <div className='mt-2 py-3'>
            <div className='mt-6 flow-root'>
              <ul role='list' className='-my-5 px-3 divide-y divide-gray-200'>
                {announcements?.slice(0, 5).map(announcement => (
                  <li key={announcement.id} className='py-5 text-left'>
                    <div className='relative focus-within:ring-2 focus-within:ring-indigo-500'>

                      <p className='mt-1 text-sm text-gray-600 line-clamp-2'>
                        <span
                          className='text-blue-dark'
                          aria-hidden='true'
                        >
                          {`${announcement.user} `}
                        </span>
                        completed the screening of <b>{announcement.patient.name}</b> {announcement.patient.gender} <span className='text-red'>{`${formatDistanceToNow(new Date(announcement.createdAt))} ago`}</span>
                      </p>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
      <Modal className='z-[999]' open={open} setOpen={setOpen} title='Request Ambulance'>
        <form className="space-y-8" onSubmit={formik.handleSubmit}>
          <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
            <div className="sm:col-span-6">
              <Input label='Full Patient Names' name='patientName' autocomplete='off' formik={formik} error={formik.errors.patientName
              }
                onChange={formik.handleChange}
              />
            </div>
            <div className="sm:col-span-6">
              <Input label='Patient ID' name='patientId' formik={formik} autocomplete='off' error={formik.errors.patientId}
                onChange={formik.handleChange}
              />
            </div>

            <div className="sm:col-span-6">
              <Input label='Pickup Location' name='pickUpLocation' autocomplete='off' formik={formik} error={formik.errors.pickUpLocation}
                onChange={formik.handleChange}
              />
            </div>

            <div className="sm:col-span-6">
              <Input label='Drop Off Location' name='dropOffLocation' autocomplete='off' formik={formik} error={formik.errors.dropOffLocation}
                onChange={formik.handleChange}
              />
            </div>

            <div className="sm:col-span-6">
              <label htmlFor="incidentDetails" className="block text-sm font-light">
                Incident Details
              </label>
              <div className="mt-1">
                <textarea
                  id="incidentDetails"
                  name="incidentDetails"
                  rows={3}
                  autocomplete='off'
                  className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                  onChange={formik.handleChange}
                />
              </div>
              {(formik.errors.incidentDetails && formik.touched.incidentDetails) &&
                <p className="mt-2 text-xs font-sm text-red">{
                  formik.errors.incidentDetails
                }</p>}
            </div>
          </div>

          <div className="mt-5 sm:mt-6 flex flex-row-reverse">
            <Button className='ml-4' kind='primary' type='submit'>Request</Button>
            <Button kind='light' type='button'>Cancel</Button>
          </div>
        </form>
      </Modal>
    </>
  );
}
