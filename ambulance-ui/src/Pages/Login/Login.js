import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { login } from 'redux/actions/userActions';
import useInputs from 'hooks/useInput';
import { Alert, Input, Button, CheckBox, Form, Spinner } from 'components';
import { UserIcon, LockOpenIcon } from '@heroicons/react/solid';

export default function Login() {
  const { error, loading } = useSelector(state => state.userSignin);
  const dispatch = useDispatch();
  const [inputs, changeHandler] = useInputs({});

  const submitHandler = e => {
    e.preventDefault();
    dispatch(login(inputs));
  };
  
  return (
    <Form className='space-y-6 py-6 px-6 md:px-10' onSubmit={submitHandler}>
      {error && <Alert kind='error' title='Error' message={error} />}
      <Input
        name='emailAddress'
        type='username'
        autoComplete='off'
        placeholder='Username'
        label='Username'
        onChange={changeHandler}
        required
        icon={<UserIcon className='h-5 w-5 text-gray-dark' />}
      />

      <Input
        name='password'
        type='password'
        autoComplete='off'
        onChange={changeHandler}
        required
        placeholder='password'
        label='Password'
        icon={<LockOpenIcon className='h-5 w-5 text-gray-dark' />}
      />
      <div className='flex items-center justify-between'>
        <CheckBox id='remember-me' name='remember-me' label='Remember me' />

        <div className='text-sm'>
          <Link to='#' className='font-medium text-[gray] text-xs'>
            Forgot your password?
          </Link>
        </div>
      </div>

      <div>
        <Button type='submit' full disabled={loading}>
          {loading && <Spinner />} Sign in
        </Button>
      </div>
      <p className='text-xs text-[gray] text-center'>
        Don't have an account?{' '}
        <a href='/auth/register' className='font-semibold'>
          Sign up
        </a>
      </p>
    </Form>
  );
}
