import React, { useEffect, useState } from 'react';
import Pagination from 'components/Pagination';
import NotificationList from './NotificationList';

const tabs = [
  { name: 'All Notifications', href: '#', current: false },
  { name: 'Unread Notifications', href: '#', current: true },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function NotificationTabs({ notifications }) {
  const [currentTab, setCurrentTab] = useState(0);
  const [pageSize] = useState(10);
  const [tabData, setTabData] = useState([]);

  const unread = notifications?.filter(notification => !notification.read);

  const changeTab = tab => {
    setCurrentTab(tab);
  };

  useEffect(() => {
    if (currentTab === 0 && notifications) {
      setTabData(notifications);
    } else if (currentTab === 1 && notifications) {
      setTabData(unread);
    }
  }, [currentTab, notifications]);

  return (
    <div>
      <div className='sm:hidden'>
        <label htmlFor='tabs' className='sr-only'>
          Select a tab
        </label>
        {/* Use an "onChange" listener to redirect the user to the selected tab URL. */}
        <select
          id='tabs'
          name='tabs'
          className='block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md'
          defaultValue={tabs.find((tab, index) => index === currentTab).name}
          onChange={e => {
            changeTab(Number(e.target.selectedIndex));
          }}
        >
          {tabs.map((tab, index) => (
            <option key={tab.name} value={index}>
              {tab.name}
            </option>
          ))}
        </select>
      </div>
      <div className='hidden sm:block'>
        <div className='border-b border-blue-dark'>
          <nav className='-mb-px flex' aria-label='Tabs'>
            {tabs.map((tab, index) => (
              <p
                key={tab.name}
                className={classNames(
                  index === currentTab
                    ? 'border-blue-dark border-b-4 font-bold'
                    : 'border-blue-dark border-b text-gray-500 hover:font-bold',
                  'w-1/2 py-4 px-1 cursor-pointer text-center  font-medium text-sm text-blue-dark'
                )}
                onClick={() => changeTab(index)}
                aria-current={index === currentTab ? 'page' : undefined}
              >
                {tab.name}
              </p>
            ))}
          </nav>
        </div>
      </div>
      <Pagination
        data={tabData}
        RenderComponent={NotificationList}
        pageLimit={Math.ceil(tabData?.length / pageSize)}
        dataLimit={pageSize}
      />
    </div>
  );
}
