import AmbualnceRequests from 'Pages/AmbulanceRequests/AmbulanceRequests';
import Facilities from 'Pages/Facilities/Facilities';
import Facility from 'Pages/Facility/Facility';
import Home from 'Pages/Home/Home';
import Notifications from 'Pages/Notifications/Notifications';

const routes = [
  {
    path: '/dashboard',
    element: <Home />,
    exact: true,
  },
 
  {
    path: '/notifications',
    element: <Notifications />,
  },
  {
    path: '/ambulance-requests',
    element: <AmbualnceRequests />,
  },
];

export default routes;
