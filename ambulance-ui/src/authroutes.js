import Login from 'Pages/Login/Login';

const routes = [
  {
    path: '/login',
    component: Login,
    exact: true,
    title: 'Login',
  },
];

export default routes;
