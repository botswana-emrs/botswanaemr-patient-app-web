import React from 'react';
import { XCircleIcon } from '@heroicons/react/solid';

export default function Alert({ color = 'red', message, title }) {
  return (
    <div className={`bg-red-light border-l-4 border-red p-4 mb-3`}>
      <div className='flex'>
        <div className='flex-shrink-0'>
          <XCircleIcon className={`h-5 w-5 text-${color}`} aria-hidden='true' />
        </div>
        <div className='ml-3'>
          <p className={`text-sm text-${color}`}>{message}</p>
        </div>
      </div>
    </div>
  );
}
