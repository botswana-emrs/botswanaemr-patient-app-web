import React from 'react';
import logo from 'assets/img/botswana.png';

export default function AuthContainer({
  title,
  children,
  width = 'auth-login-form',
}) {
  return (
    <>
      <div className={`sm:mx-auto sm:w-full  ${width}`}>
        <img className='mx-auto w-36' src={logo} alt='Workflow' />
        <h5 className='text-center text-md text-gray-dark py-3'>
          Botswana EMR Hospital Services
        </h5>
      </div>

      <div className={`sm:mx-auto sm:w-full bg-[white] ${width}`}>
        <div className='bg-white'>{children}</div>
      </div>
    </>
  );
}
