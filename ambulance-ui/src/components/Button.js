import React from 'react';

export default function Button(props) {
  const { type = 'button', kind = 'primary', size = 'medium', ...rest } = props;

  const buttonKind = () => {
    switch (kind) {
      case 'primary':
        return 'bg-blue-dark hover:bg-dblue text-white';
      case 'secondary':
        return 'bg-gray-200 hover:bg-gray-300 text-gray-700';
      case 'danger':
        return 'bg-red-dark hover:bg-red-dark text-white';
      case 'success':
        return 'bg-green-900 hover:bg-green-dark text-white';
      case 'warning':
        return 'bg-yellow-dark hover:bg-yellow-dark text-white';
      case 'light':
        return 'bg-light hover:bg-light text-[black]';
      case 'primary-outline':
        return 'bg-white hover:bg-blue-dark hover:text-white text-blue-dark border-blue-dark';
      default:
        return 'bg-blue-dark hover:bg-dblue text-white';
    }
  };
  const buttonSize = () => {
    switch (size) {
      case 'small':
        return 'px-4 py-1 text-sm font-light';
      case 'medium':
        return 'px-4 py-2 text-base font-light';
      case 'large':
        return 'py-2 px-4 font-light';
      default:
        return 'py-2 px-4 font-light';
    }
  };
  return (
    <button
      type={type}
      {...rest}
      className={`flex justify-center border border-transparent rounded-md shadow-sm text-sm font-medium text-light  focus:outline-none ${buttonKind()} ${buttonSize()} ${
        props.full ? 'w-full' : null
      } ${props.className}`}
    >
      {props.children}
    </button>
  );
}
