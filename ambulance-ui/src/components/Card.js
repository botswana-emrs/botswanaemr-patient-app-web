export default function Card({ children, header, footer, light, noShadow }) {
  return (
    <div
      className={`max-w-6xl ${light ? 'ml-2' : 'mx-auto'} ${
        noShadow ? '' : 'shadow'
      } bg-[white]`}
    >
      <div
        className={`px-4 py-2 border-b border-gray-200 sm:px-6 ${
          light ? 'border-b-2 border-blue-dark' : 'bg-white'
        }`}
      >
        {header}
      </div>
      <div className='px-4 py-2'>{children}</div>
      {footer && (
        <div className='px-4 py-2 border-b border-gray-200 sm:px-6 bg-light-100'>
          {footer}
        </div>
      )}
    </div>
  );
}
