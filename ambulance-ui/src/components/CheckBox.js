import React from 'react';

export default function CheckBox(props) {
  const {
    className = 'h-4 w-4 text-[gray] text-xs border-gray-300 rounded',
    label,
    ...rest
  } = props;
  return (
    <div className='flex items-center'>
      <input
        type='checkbox'
        className={className}
        {...rest}
      />
      <label htmlFor='remember-me' className='ml-2 block text-xs text-[gray]'>
        {label}
      </label>
    </div>
  );
}
