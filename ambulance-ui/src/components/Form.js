import React from 'react';

const Form = React.forwardRef((props, ref) => {
  const { children, className, ...rest } = props;
  return (
    <form ref={ref} className={`${className} form`} {...rest}>
      {children}
    </form>
  );
});

export default Form;
