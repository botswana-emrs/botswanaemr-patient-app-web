import React, { useState } from 'react';
import {
  GoogleMap,
  useJsApiLoader,
  DirectionsService,
  DirectionsRenderer,
} from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '400px',
};

const center = { lat:-24.6146592 , lng: 25.8865324 }

function GoogleMaps() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAPS_KEY,
  });

  const [map, setMap] = useState(null);
  const [directionsResponse, setDirectionsResponse] = useState(null);
  const origin = { lat: -24.4778762, lng: 25.6562887 };
  const destination = { lat:-24.6146592 , lng: 25.8865324 };
  const travelMode = 'DRIVING';

  const onLoad = React.useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds);
    setMap(map);
  }, []);

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null);
  }, []);



  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={10}
      onLoad={onLoad}
      onUnmount={onUnmount}
      options={{
        // fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        zoomControl: false,
        time: true,
        mapTypeId: 'roadmap',
      }}
    >
      <DirectionsService
        options={{
          destination: destination,
          origin: origin,
          travelMode: travelMode,
        }}
        callback={res => {
          if (res !== null) {
            setDirectionsResponse(res);
          }
        }}
      />

      {directionsResponse !== null && (
        <DirectionsRenderer
          options={{
            directions: directionsResponse,
          }}
        />
      )}
    </GoogleMap>
  ) : (
    <></>
  );
}

export default React.memo(GoogleMaps);
