import React from 'react';

export default function Input(props) {
  const { type = 'text', error, className, ...rest } = props;
  return (
    <div className='mt-1 flex flex-col-reverse relative items-start'>
      {error && <p className='mt-2 text-xs font-sm text-red'>{error}</p>}
      {props.icon && (
        <div className='absolute left-0 bottom-0 px-2 flex items-center pointer-events-none bg-gray-300 h-[38px] rounded-l-md'>
          {props.icon}
        </div>
      )}
      <input
        {...rest}
        type={type}
        className={`appearance-none block w-full px-3 py-2 border-light rounded-md shadow placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${className} ${
          props.icon && 'pl-10 focus:ring-0 focus:border-0'
        }`}
      />
      <label htmlFor={props.name} className='block text-sm font-light'>
        {props.label}
      </label>
    </div>
  );
}
