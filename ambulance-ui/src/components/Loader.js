import React from 'react';
import logo from 'assets/img/botswana.png';

export default function Loader() {
  return (
    <div className='w-full relative loader'>
      <div className='snippet' data-title='.dot-flashing'>
        <div className='w-32 pb-5'>
          <img src={logo} alt='botswana' className='w-full' />
        </div>
        <div className='stage'>
          <div className='dot-flashing'></div>
        </div>
      </div>
    </div>
  );
}
