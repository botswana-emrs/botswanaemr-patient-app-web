import { MapContainer, TileLayer, useMap, Marker, Popup } from 'react-leaflet';
import { Link } from 'react-router-dom';
import marker from 'assets/img/hospital.png';

import L from 'leaflet';

const customMarker = new L.icon({
  iconUrl: marker,
  iconSize: [27, 34],
  iconAnchor: [10, 41],
  popupAnchor: [2, -40],
});

export default function Map({ facilities = [] }) {
  const position = [-22.3285, 20.1915668];

  // console.log(facilities);
  const correctCoordinates = coord => {
    // count the number of dots
    if (coord) {
      const arr = (coord + '').split('.');
      const dots = arr.length;
      if (dots > 2) {
        return arr[0] + '.' + arr[1];
      }
      return coord;
    }
  };

  return (
    <MapContainer
      className='facility-map'
      center={position}
      zoom={6}
      scrollWheelZoom={false}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      />
      {facilities?.map(facility => (
        <Marker
          key={facility.newFacilityCode}
          position={[
            correctCoordinates(facility.lat),
            correctCoordinates(facility.lng),
          ]}
          icon={customMarker}
        >
          <Popup>
            <div className='flex flex-col space-y-2'>
              <Link
                to={`/app/facilities/${facility.newFacilityCode}`}
                className='text-xs font-semibold  text-gray-900'
              >
                {facility.facilityName}
              </Link>
              {facility.telephone && (
                <p className='text-xs font-light  text-gray-900'>
                  <span>
                    <i className='fa-solid fa-phone mr-2'></i>
                  </span>
                  {facility.telephone}
                </p>
              )}
            </div>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
}
