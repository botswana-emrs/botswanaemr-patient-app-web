import React, { useState, useEffect } from 'react';
import { XCircleIcon } from '@heroicons/react/solid';

export default function Notification({ kind, message }) {
  const [show, setShow] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShow(false);
    }, 4000);
  }, [setShow]);

  return (
    <>
      {kind === 'error' && (
        <div
          className={`fixed z-[999] rounded-md bg-red-50 p-4 top-2 left-1/2 -translate-x-2/4 translate-y-2/4 ${
            show ? 'block' : 'hidden'
          }`}
        >
          <div className='flex'>
            <div className='flex-shrink-0'>
              <XCircleIcon
                className='h-5 w-5 text-red-400'
                aria-hidden='true'
              />
            </div>
            <div className='ml-3'>
              <h3 className='text-sm font-medium text-red-800'>
                Error: {message}
              </h3>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
