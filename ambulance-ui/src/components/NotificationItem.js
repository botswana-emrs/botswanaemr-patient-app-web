import React from 'react';
import { Link } from 'react-router-dom';
import { updateNotification } from 'redux/actions/notificationActions';
import { useDispatch } from 'react-redux';

export default function NotificationItem({ notification }) {
  const { message, notificationType, id, read } = notification;
  const dispatch = useDispatch();
  return (
    <li className='my-3 shadow-lg'>
      <Link
        to={`/app/notifications/`}
        onClick={() => dispatch(updateNotification(id))}
        className='block hover:bg-gray-50'
      >
        <div className='flex items-center px-4 py-4 sm:px-6'>
          <div className='min-w-0 flex-1 flex items-center'>
            <div className='flex-shrink-0'>
              <div className='h-14 w-14 rounded-md flex justify-center items-center bg-lblue-100 relative'>
                <i className='fas fa-solid fa-calendar-plus text-blue-dark text-2xl'></i>
                {!read && (
                  <svg
                    className='-ml-0.5 mr-1.5 h-3 w-3 text-red absolute right-0 top-1'
                    fill='currentColor'
                    viewBox='0 0 8 8'
                  >
                    <circle cx={4} cy={4} r={3} />
                  </svg>
                )}
              </div>
            </div>
            <div className='min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4'>
              <div>
                <p className='text-sm font-medium text-indigo-600 truncate'>
                  {notificationType}
                </p>
                <p className='mt-2 flex items-center text-sm text-gray-500'>
                  <span className='truncate'>{message}</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </li>
  );
}
