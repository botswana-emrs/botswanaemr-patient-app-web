import * as React from 'react';
import { useState } from 'react';

import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid';

function Pagination({ data, RenderComponent, pageLimit, dataLimit, columns, kind }) {
  const [currentPage, setCurrentPage] = useState(1);

  function goToNextPage() {
    setCurrentPage(page => page + 1);
  }

  function goToPreviousPage() {
    setCurrentPage(page => page - 1);
  }

  function changePage(event) {
    const pageNumber = Number(event.target.textContent);
    setCurrentPage(pageNumber);
  }

  const getPaginatedData = () => {
    const startIndex = currentPage * dataLimit - dataLimit;
    const endIndex = startIndex + dataLimit;
    console.log(startIndex, endIndex, data);
    return data.slice(startIndex, endIndex);
  };
  const getPaginationGroup = () => {
    const min =
      currentPage <= 1 && currentPage < pageLimit
        ? 1
        : Number(currentPage) === pageLimit && pageLimit > 2
        ? currentPage - 2
        : currentPage - 1;

    const next =
      pageLimit >= 2 && currentPage < pageLimit && currentPage > 1
        ? currentPage + 1
        : Number(currentPage) === 1 && pageLimit > 2
        ? currentPage + 2
        : currentPage;
    let arr = [];
    for (let i = min; i <= next; i++) {
      arr.push(i);
    }
    return arr;
  };

  return (
    <div>
      <div>
        <RenderComponent columns={columns} data={getPaginatedData()} kind={kind} />
      </div>

      {data?.length > 10 && (
        <div className='bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6'>
          <div className='flex-1 flex justify-between sm:hidden'>
            <button className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'>
              Previous
            </button>
            <button
              href='#'
              className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
            >
              Next
            </button>
          </div>
          <div className='hidden sm:flex-1 sm:flex sm:items-center sm:justify-between'>
            <div>
              <p className='text-sm text-gray-700'>
                Showing{' '}
                <span className='font-medium'>
                  {currentPage * dataLimit - dataLimit + 1}
                </span>{' '}
                to{' '}
                <span className='font-medium'>
                  {currentPage * dataLimit - dataLimit + dataLimit}
                </span>{' '}
                of <span className='font-medium'>{data?.length}</span> results
              </p>
            </div>
            <div>
              <nav
                className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px'
                aria-label='Pagination'
              >
                <button
                  onClick={goToPreviousPage}
                  disabled={currentPage === 1}
                  className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-blue-dark disabled:text-gray-600 hover:bg-gray-50'
                >
                  Previous
                </button>
                {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}
                {getPaginationGroup().map((item, index) => (
                  <button
                    key={index}
                    onClick={changePage}
                    aria-current='page'
                    className={`z-10  relative inline-flex items-center px-4 py-2 border text-sm font-medium ${
                      currentPage === item
                        ? 'bg-blue-dark text-light'
                        : 'text-blue-dark'
                    }`}
                  >
                    {item}
                  </button>
                ))}
                <button
                  onClick={goToNextPage}
                  disabled={currentPage === pageLimit}
                  className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-blue-dark disabled:text-gray-600 hover:bg-gray-50'
                >
                  Next
                </button>
              </nav>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Pagination;
