export default function Stack({ data = {}, merge = '' }) {
  return (
    <div className='overflow-hidden h-fit bg-white border border-gray-200 w-full'>
      <ul className='divide-y divide-gray-200'>
        {Object.keys(data).map((item, index) => (
          <li key={index}>
            <div className='flex items-center px-4  sm:px-6'>
              <div className='min-w-0 flex-1 sm:flex sm:items-center sm:justify-between w-full'>
                {merge.includes(item) ? (
                  <div className='truncate w-full'>
                    <div className='flex text-sm w-full flex-col'>
                      <div className=' w-full'>
                        <p className='truncate font-semibold pt-4 w-fit'>
                          {item}:
                        </p>
                      </div>
                      <div className='w-full'>
                        <p className='truncate font-light pb-4 w-fit'>
                          {data[item]}
                        </p>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className='truncate w-full'>
                    <div className='flex text-sm w-full'>
                      <div className='border-r border-gray-200 w-1/2'>
                        <p className='truncate font-light py-4 w-fit'>
                          {item}:
                        </p>
                      </div>
                      <div className='w-1/2'>
                        <p className='pl-2 truncate font-light py-4 w-fit'>
                          {data[item]}
                        </p>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}
