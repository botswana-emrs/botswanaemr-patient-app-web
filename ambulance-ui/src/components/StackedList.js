import React from 'react';

export default function StackedList({ item }) {
  return (
    <tr>
      <td className='border border-collapse py-2 px-3'>{item.key}</td>
      <td className='border border-collapse py-2 px-3'>{item.value}</td>
    </tr>
  );
}
