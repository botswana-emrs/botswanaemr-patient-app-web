export default function TableBody({ columns = [], data = [], kind }) {
  const theme =
    kind === 'primary'
      ? { bg: 'lblue', text: 'dark', font: 'semibold', divide: 'dark' }
      : { bg: 'gray-100', text: 'dark', font: 'extralight', divide:'gray-300'  };
  return (
    <div className='px-4 pb-3 sm:px-6 lg:px-8'>
      <div className='mt-8 flex flex-col'>
        <div className='-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8'>
          <div className='inline-block min-w-full py-2 align-middle md:px-6 lg:px-8'>
            <div className='overflow-hidden shadow ring-1 ring-gray-300 '>
              <table className='min-w-full divide-y divide-gray-300'>
                <thead className={`bg-${theme.bg}`}>
                  <tr className={`divide-x divide-${theme.divide}`}>
                    {columns.map((item, idx) => (
                      <th
                        scope='col'
                        className={`py-3.5 pl-4 pr-4 text-left text-sm text-gray-900 sm:pl-6 font-${theme.font}`}
                        key={idx}
                      >
                        {item.title}
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody className='divide-y divide-gray-300 bg-white'>
                  {data?.map((prop, i) => (
                    <tr key={i} className='divide-x divide-gray-300'>
                      {columns.map((item, idx) => (
                        <td
                          className='whitespace-nowrap px-4 py-2 text-sm font-extralight text-[black]'
                          key={idx}
                        >
                          {item.render
                            ? item.render(prop, prop[item.key], i)
                            : prop[item.key]}
                        </td>
                      ))}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
