// inputs hooks are used to manage the state of the inputs
import { useState } from 'react';

export default function useInputs(initialState = {}) {
  const [inputs, setInputs] = useState(initialState);

  const changeHandler = e => {
    if (!e.target) {
      return setInputs({ ...inputs, ...e });
    }
    if (e.target.type === 'file') {
      return setInputs({ ...inputs, [e.target.name]: e.target.files[0] });
    }
    const { name, value } = e.target;
    setInputs({ ...inputs, [name]: value });
  };

  const reset = () => {
    setInputs(initialState);
  };
  return [inputs, changeHandler, reset];
}
