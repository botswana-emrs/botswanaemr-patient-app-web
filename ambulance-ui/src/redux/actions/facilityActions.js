import Axios from 'axios';
import Cookie from 'js-cookie';
import {
  GET_FACILITIES_REQUEST,
  GET_FACILITIES_SUCCESS,
  GET_FACILITIES_FAILURE,
  GET_FACILITY_REQUEST,
  GET_FACILITY_SUCCESS,
  GET_FACILITY_FAILURE,
  GET_DHMT_REQUEST,
  GET_DHMT_SUCCESS,
  GET_DHMT_FAILURE,
  GET_FACILITY_TYPE_REQUEST,
  GET_FACILITY_TYPE_SUCCESS,
  GET_FACILITY_TYPE_FAILURE,
  GET_FACILITY_OWNER_REQUEST,
  GET_FACILITY_OWNER_SUCCESS,
  GET_FACILITY_OWNER_FAILURE,
  GET_DISTRICT_REQUEST,
  GET_DISTRICT_SUCCESS,
  GET_DISTRICT_FAILURE,
  GET_FACILITY_CONSTITUENCY_REQUEST,
  GET_FACILITY_CONSTITUENCY_SUCCESS,
  GET_FACILITY_CONSTITUENCY_FAILURE,
} from '../constants/facilityContants';

const accessToken = Cookie.get('userInfo');
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Axios.defaults.headers.put['Accept'] = 'application/json';
if (accessToken) {
  const parsed = JSON.parse(accessToken);
  Axios.defaults.headers.common.Authorization = `Bearer ${parsed?.access_token}`;
}

// axios interceptor for handling errors
Axios.interceptors.response.use(
  response => response,
  error => {
    return Promise.reject(error);
  }
);

export const getFacilities = () => async dispatch => {
  dispatch({ type: GET_FACILITIES_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/all`);
    dispatch({ type: GET_FACILITIES_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_FACILITIES_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getFacility = id => async dispatch => {
  dispatch({ type: GET_FACILITY_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/facility/${id}/new`);
    dispatch({ type: GET_FACILITY_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_FACILITY_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getDhmt = () => async dispatch => {
  dispatch({ type: GET_DHMT_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/dhmt`);
    dispatch({ type: GET_DHMT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_DHMT_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getFacilityType = () => async dispatch => {
  dispatch({ type: GET_FACILITY_TYPE_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/type`);
    dispatch({ type: GET_FACILITY_TYPE_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_FACILITY_TYPE_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getFacilityOwner = () => async dispatch => {
  dispatch({ type: GET_FACILITY_OWNER_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/ownership`);
    dispatch({ type: GET_FACILITY_OWNER_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_FACILITY_OWNER_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getDistrict = () => async dispatch => {
  dispatch({ type: GET_DISTRICT_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/district`);
    dispatch({ type: GET_DISTRICT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_DISTRICT_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getFacilityConstituency = () => async dispatch => {
  dispatch({ type: GET_FACILITY_CONSTITUENCY_REQUEST });
  try {
    const { data } = await Axios.get(`/facility/api/v1/constituency`);
    dispatch({ type: GET_FACILITY_CONSTITUENCY_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_FACILITY_CONSTITUENCY_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};
