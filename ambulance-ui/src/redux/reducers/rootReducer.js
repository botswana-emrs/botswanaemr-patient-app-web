import { combineReducers } from 'redux';
import {
  userLoginReducer,
  userRegisterReducer,
  userUpdateReducer,
  getUserReducer,
  verifyEmailReducer,
  resendVerificationLinkReducer,
  forgotPasswordReducer,
  resetPasswordReducer,
} from './userReducer';
import {
  getFacilitiesReducer,
  getFacilityReducer,
  getDhmtReducer,
  getDistrictReducer,
  getFacilityConstituencyReducer,
  getFacilityOwnerReducer,
  getFacilityTypeReducer,
} from './facilityReducer';
import {
  createNotificationReducer,
  getNotificationsReducer,
  getNotificationReducer,
  updateNotificationReducer,
} from './notificationReducer';

const reducer = combineReducers({
  userSignin: userLoginReducer,
  userSignup: userRegisterReducer,
  userUpdate: userUpdateReducer,
  getUser: getUserReducer,
  verifyEmail: verifyEmailReducer,
  resendVerificationLink: resendVerificationLinkReducer,
  forgotPassword: forgotPasswordReducer,
  resetPassword: resetPasswordReducer,
  getFacilities: getFacilitiesReducer,
  getFacility: getFacilityReducer,
  getDhmt: getDhmtReducer,
  getDistrict: getDistrictReducer,
  getFacilityConstituency: getFacilityConstituencyReducer,
  getFacilityOwner: getFacilityOwnerReducer,
  getFacilityType: getFacilityTypeReducer,
  createNotification: createNotificationReducer,
  getNotifications: getNotificationsReducer,
  getNotification: getNotificationReducer,
  updateNotification: updateNotificationReducer,
});

export default reducer;
