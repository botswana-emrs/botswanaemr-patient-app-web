import './App.css';
import DashboardLayout from 'Layouts/DashboardLayout';
import { Routes, Route, Navigate } from 'react-router-dom';
import AuthLayout from 'Layouts/AuthLayout';

function App() {


  return (
    <div className='App'>
      <Routes>
        <Route path='/app/*' element={<DashboardLayout />} />
        <Route path='/auth/*' element={<AuthLayout />} />
        <Route path='/' element={<Navigate to='/app/dashboard' />} />
      </Routes>
    </div>
  );
}

export default App;
