import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import routes from 'authroutes';
import AuthContainer from 'components/AuthContainer';

export default function AuthLayout() {
  const { userInfo } = useSelector(state => state.userSignin);

  useEffect(() => {
    if (userInfo) {
      window.location.href = '/app/dashboard';
    }
  }, [userInfo]);

  return (
    <>
      <div className='min-h-full h-screen flex flex-col justify-center bg-light-100  sm:px-6 lg:px-8'>
        <Routes>
          {routes.map((prop, key) => {
            return (
              <Route
                path={prop.path}
                element={
                  <AuthContainer
                    title={prop.title}
                    width={prop.width || 'auth-login-form'}
                  >
                    <prop.component />
                  </AuthContainer>
                }
                key={key}
              />
            );
          })}
        </Routes>
      </div>
    </>
  );
}
