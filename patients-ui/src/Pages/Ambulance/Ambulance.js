import React, { useEffect, useState, useRef } from 'react';
import { Tab, Breadcrumb, Card, Button, Input, Modal } from 'components';
import AmbulanceList from './AmbulanceList';
import { createIncident, listIncidents } from 'redux/actions/ambulanceActions';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
  location: Yup.string().required('Location is required'),
  incident: Yup.string().required('Incident is required'),
  additionalInformation: Yup.string().required(
    'Additional Information is required'
  ),
});

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Ambulance', href: '/app/ambulance', current: true },
];
export default function Ambulance() {
  const dispatch = useDispatch();
  const { incidents, loading, error } = useSelector(
    state => state.listAmbulance
  );

  const { success: successCreate } = useSelector(
    state => state.createAmbulance
  );

  const formRef = useRef(null);

  const { user } = useSelector(state => state.getUser);

  const [open, setOpen] = useState(false);

  useEffect(() => {
    dispatch(
      listIncidents({ incidentStatus: 'AWAITING', pageNo: 1, itemNo: 100 })
    );
  }, [dispatch, successCreate]);

  const formik = useFormik({
    initialValues: {
      location: '',
      incident: '',
      additionalInformation: '',
    },
    validationSchema,
    onSubmit: values => {
      console.log(user);
      const data = {
        ...values,
        userName: `${user.firstName} ${user.lastName}`,
        phoneNumber: user.phoneNumber,
      };
      console.log(data);
      dispatch(createIncident(data));
      setOpen(false);
      formRef.current.reset();
    },
  });

  const tabs = [
    {
      title: 'YOUR REQUESTS',
      content: <AmbulanceList loading={loading} requests={incidents || []} />,
    },
  ];

  return (
    <div>
      <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
        <div className='py-6 md:flex md:items-center md:justify-between'>
          <div className='flex-1 min-w-0'>
            {/* Profile */}
            <div className='flex items-center'>
              <div>
                <div className='flex items-center'>
                  <i className='fas fa-bell text-blue-dark'></i>
                  <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                    Ambulance
                  </h1>
                </div>
              </div>
            </div>
          </div>
          <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
            <Breadcrumb pages={pages} />
          </div>
        </div>
      </div>
      <Card
        header={
          <h3 className='text-lg leading-6 font-light text-light'>
            Ambulance Request
          </h3>
        }
      >
        <Button
          kind='primary-outline'
          className='border-blue'
          onClick={() => setOpen(true)}
        >
          Request Ambulance
        </Button>
        <Tab tabs={tabs} />
      </Card>
      <Modal
        title='Request Ambulance'
        open={open}
        setOpen={setOpen}
        onOk={formik.handleSubmit}
        onCancel={() => setOpen(false)}
        noCancel={true}
        okText='Request Ambulance'
      >
        <form
          className='grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6'
          ref={formRef}
        >
          <div className='sm:col-span-6'>
            <Input
              placeholder='Location'
              name='location'
              onChange={formik.handleChange}
              error={
                formik.errors.location && formik.touched.location
                  ? formik.errors.location
                  : null
              }
            />
            <Input
              placeholder='Incident'
              name='incident'
              onChange={formik.handleChange}
              className='mt-4'
              error={
                formik.errors.incident && formik.touched.incident
                  ? formik.errors.incident
                  : null
              }
            />
            <Input
              type='textarea'
              name='additionalInformation'
              className='mt-4'
              placeholder='Incident Details'
              onChange={formik.handleChange}
              error={
                formik.errors.additionalInformation &&
                formik.touched.additionalInformation
                  ? formik.errors.additionalInformation
                  : null
              }
              rows='5'
            ></Input>
          </div>
        </form>
      </Modal>
    </div>
  );
}
