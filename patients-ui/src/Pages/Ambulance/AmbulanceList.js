import React, { useEffect, useState } from 'react';
import { Table, Button, Empty } from 'components';
import { SearchIcon } from '@heroicons/react/outline';
import { Link } from 'react-router-dom';
import { listAppointments } from 'redux/actions/appointmentActions';
import { useDispatch, useSelector } from 'react-redux';
import { format } from 'date-fns';
import { Spinner } from 'components';
import { searchFilter } from 'lib/searchFilter';

export default function AmbulanceList({ requests = [], loading }) {
  const [ambulanceRequests, setAmbulanceRequests] = useState(requests);

  useEffect(() => {
    if (requests) {
      setAmbulanceRequests(requests);
    }
  }, [requests]);

  const columns = [
    {
      title: 'Date of Request',
      key: 'date',
      render: (record, _, i) => (
        <div key={i}>
          {record.createdAt
            ? format(new Date(record.createdAt), 'EEEE dd MMM yyyy hh:mm a')
            : null}
        </div>
      ),
    },
    {
      title: 'Location',
      key: 'pickUpLocation',
    },
    {
      title: 'Drop off Location',
      key: 'dropOffLocation',
    },
    {
      title: 'Action',
      key: 'action',
      render: (record, _, i) => (
        <Link
          key={i}
          to={`/app/ambulance/${record.id}`}
          onClick={e =>
            localStorage.setItem('ambulance', JSON.stringify(record))
          }
          className=' flex justify-center'
        >
          <i className='fa-solid text-lg fa-circle-arrow-right text-blue-dark'></i>
        </Link>
      ),
    },
  ];

  return (
    <div>
      <div className='flex  px-6 mt-8'>
        <div className='mt-1  ml-auto relative flex items-center'>
          <input
            type='text'
            name='search'
            autoComplete='off'
            id='search'
            onChange={e =>
              setAmbulanceRequests(searchFilter(e.target.value, requests))
            }
            placeholder='Search Appointments'
            className='shadow-sm search-table placeholder:italic focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-12 sm:text-sm border-gray-300 rounded-md'
          />
          <div className='absolute inset-y-0 left-0 flex py-1.5 pl-1.5'>
            <kbd className='inline-flex items-center rounded px-2 text-sm font-sans font-medium text-gray-400'>
              <SearchIcon className='h-5 w-5' />
            </kbd>
          </div>
        </div>
      </div>
      <div>
        <Table columns={columns} data={ambulanceRequests} kind='primary' />
        {loading && (
          <div className='w-full flex justify-center'>
            <Spinner />
          </div>
        )}
        {!loading && requests?.length === 0 && (
          <Empty text='No ambulance requests' />
        )}
      </div>
    </div>
  );
}
