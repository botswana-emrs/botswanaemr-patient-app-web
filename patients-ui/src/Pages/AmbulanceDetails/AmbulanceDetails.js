import React, { useEffect } from 'react';
import { Card, Breadcrumb } from 'components';
import { getIncident } from 'redux/actions/ambulanceActions';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import Loader from 'components/Loader';
import Divider from 'components/Divider';
import StackedList from 'components/StackedList';
import { format } from 'date-fns';
import Directions from 'components/Directions';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Ambulance', href: '/app/ambulance', current: true },
];

export default function AmbulanceDetails() {
  const { loading, incident } = useSelector(state => state.getAmbulance);

  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getIncident(id));
  }, []);

  return (
    <>
      {/* {error && <Notification kind='error' message={error} />} */}
      <div className='bg-gradient-to-b from-lblue-light to-light'>
        <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
          <div className='py-6 md:flex md:items-center md:justify-between'>
            <div className='flex-1 min-w-0'>
              {/* Profile */}
              <div className='flex items-center'>
                <div>
                  <div className='flex items-center'>
                    <i className='fas fa-hospital text-blue-dark'></i>
                    <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                      Facilities
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
              <Breadcrumb pages={pages} />
            </div>
          </div>
        </div>
      </div>

      <div className='mt-8'>
        <Card
          header={
            <div className='flex justify-between'>
              <h3 className='text-lg leading-6 font-light text-light'>
                {/* fomat date as Monday 12 June 2022 */}
                Ambulance Request
              </h3>
              <Link
                to='/app/ambulance'
                className='flex items-center bg-light rounded px-4 text-blue-dark'
              >
                Back
              </Link>
            </div>
          }
        >
          <div className='facility-map'>
            <Directions destination={incident?.dropOffLocation} />
          </div>
          {loading && <Loader />}
          {incident && (
            <div className='grid grid-cols-2 text-sm font-light gap-8 mt-6'>
              <div className='mt-6'>
                <h1 className='pl-2 text-md font-normal text-blue-dark'>
                  REQUEST DETAILS
                </h1>
                <Divider />
                <div className='text-sm font-light gap-8 mt-6'>
                  <table>
                    <tbody>
                      <StackedList
                        item={{
                          key: 'Date:',
                          // format as Sept 22 2021 12:00 PM
                          value: format(
                            new Date(incident?.createdAt),
                            'MMM dd yyyy hh:mm a'
                          ),
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Location:',
                          value: incident?.pickUpLocation || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Status:',
                          value: incident?.incidentStatus || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Incident details:',
                          value: incident?.incident || 'N/A',
                        }}
                      />
                    </tbody>
                  </table>
                </div>
              </div>

              <div>
                <h1 className='pl-2 text-md font-normal text-blue-dark mt-6'>
                  Ambulance Details
                </h1>
                <Divider />
                <div className='text-sm font-light gap-8 mt-6'>
                  <table className='h-fit'>
                    <tbody>
                      <StackedList
                        item={{
                          key: 'Registration:',
                          value: incident?.ambulance?.registration || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Estimated Time:',
                          value: incident?.ambulance?.estimatedTime || 'N/A',
                        }}
                      />
                      <StackedList
                        item={{
                          key: 'Drop Off location:',
                          value: incident?.dropOffLocation || 'N/A',
                        }}
                      />
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          )}
        </Card>
      </div>
    </>
  );
}
