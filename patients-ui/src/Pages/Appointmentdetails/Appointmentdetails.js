import React, { useEffect, useState } from 'react';
import { Button, Card, Form, Select, Breadcrumb, ButtonLink } from 'components';
import { getFacility } from 'redux/actions/facilityActions';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import Loader from 'components/Loader';
import Notification from 'components/Notification';
import Divider from 'components/Divider';
import StackedList from 'components/StackedList';
import { format } from 'date-fns';
import { Input } from 'components';
import GoogleMapsDirections from 'components/Directions';
import Modal from 'components/Modal';
import { QuestionMarkCircleIcon } from '@heroicons/react/outline';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Appointments', href: '/app/appointments', current: true },
];

export default function AppointmentDetails() {
  const [appointment, setAppointment] = useState(null);
  const [open, setOpen] = useState(false);

  const { id } = useParams();

  useEffect(() => {
    const appointment = JSON.parse(localStorage.getItem('appointment'));
    setAppointment(appointment);
  }, []);

  const startDate = appointment?.requestedAppointmentDetails
    ? appointment?.scheduledAppointmentDetails?.requestedOn
    : appointment?.scheduledAppointmentDetails?.startDate;

  const endDate = appointment?.requestedAppointmentDetails
    ? null
    : appointment?.scheduledAppointmentDetails?.endDate;

  return (
    <>
      {/* {error && <Notification kind='error' message={error} />} */}
      <div className='bg-gradient-to-b from-lblue-light to-light'>
        <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
          <div className='py-6 md:flex md:items-center md:justify-between'>
            <div className='flex-1 min-w-0'>
              {/* Profile */}
              <div className='flex items-center'>
                <div>
                  <div className='flex items-center'>
                    <i className='fas fa-hospital text-blue-dark'></i>
                    <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                      Facilities
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
              <Breadcrumb pages={pages} />
            </div>
          </div>
        </div>
      </div>

      <div className='mt-8'>
        <Card
          header={
            <div className='flex justify-between'>
              <h3 className='text-lg leading-6 font-light text-light'>
                {/* fomat date as Monday 12 June 2022 */}
                Appointment:{' '}
                {appointment &&
                  startDate &&
                  format(new Date(startDate), 'EEEE do MMMM yyyy')}
              </h3>
              <Link
                to='/app/appointments'
                className='flex items-center bg-light rounded px-4 text-blue-dark'
              >
                Back
              </Link>
            </div>
          }
        >
          <div className='mt-8'>
            <h1 className='pl-2 text-md font-normal text-blue-dark'>
              FACILITY INFORMATION
            </h1>
            <Divider />
            <div className='grid grid-cols-2 text-sm font-light gap-8 mt-6'>
              <table>
                <tbody>
                  <StackedList
                    item={{
                      key: 'Facility:',
                      value: appointment?.facility?.name || 'N/A',
                    }}
                  />
                  <StackedList
                    item={{
                      key: 'Facility Owner:',
                      value:
                        appointment?.facility?.facilityOwner?.name || 'N/A',
                    }}
                  />
                  <StackedList
                    item={{
                      key: 'Facility Type:',
                      value: 'N/A',
                    }}
                  />
                  <StackedList
                    item={{
                      key: 'District:',
                      value: appointment?.district || 'N/A',
                    }}
                  />
                </tbody>
              </table>
              <table className='h-fit'>
                <tbody>
                  <StackedList
                    item={{
                      key: 'Date:',
                      value:
                        (appointment &&
                          startDate &&
                          format(new Date(startDate), 'EEEE do MMMM yyyy')) ||
                        'N/A',
                    }}
                  />
                  <StackedList
                    item={{
                      key: 'Time Allocated:',
                      value:
                        startDate && endDate
                          ? `${format(
                              new Date(startDate),
                              'hh:mm a'
                            )} - ${format(new Date(endDate), 'hh:mm a')}`
                          : 'N/A',
                    }}
                  />
                </tbody>
              </table>
            </div>

            <h1 className='pl-2 text-md font-normal text-blue-dark mt-6'>
              DOCTOR INFORMATION
            </h1>
            <Divider />
            <div className='grid lg:grid-cols-2 sm:grid-cols-1 text-sm font-light gap-8 mt-6'>
              <div className='grid lg:grid-cols-2 sm:grid-cols-1'>
                <div className='w-24 h-24 overflow-hidden'>
                  <img
                    className='h-full w-full rounded-lg'
                    src={
                      appointment?.doctor?.profilePicture ||
                      'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y&d=mp'
                    }
                    alt=''
                  />
                </div>
                <div className='ml-4'>
                  <table className='w-full'>
                    <StackedList item={{ key: 'Name:', value: 'N/A' }} />
                    <StackedList item={{ key: 'Speciality:', value: 'N/A' }} />
                  </table>
                </div>
              </div>
              <Button
                kind='light'
                className='w-fit h-fit shadow-xl flex flex-col items-center hover:shadow-2xl'
                onClick={() => setOpen(true)}
              >
                <p className='text-red'>Cancel </p> Appointment
              </Button>
            </div>

            <h1 className='pl-2 text-md font-normal text-blue-dark mt-6'>
              DIRECTIONS TO FACILITY
            </h1>
            <Divider />
            <div className='grid lg:grid-cols-2 sm:grid-cols-1 text-sm font-light gap-8 mt-6'>
              <div>
                <form className='h-full flex flex-col justify-between py-4'>
                  <Input
                    required
                    label='Enter your current location'
                    placeholder='Enter location'
                  />
                  <Input
                    label='Your destination'
                    placeholder='Your destination'
                  />
                  <Button className='w-fit ml-auto'>Load Directions</Button>
                </form>
              </div>
              <div className='h-64 w-full overflow-hidden'>
                <GoogleMapsDirections />
              </div>
            </div>
          </div>
        </Card>
      </div>
      <Modal
        title='Cancel Visit'
        open={open}
        setOpen={setOpen}
        onOk={() => setOpen(false)}
        okText='Yes'
        onCancel={() => setOpen(false)}
        cancelText='No'
      >
        <div className='flex flex-col items-center font-light'>
          <QuestionMarkCircleIcon className='text-yellow-600 bg-yellow p-2 rounded-full w-24 h-24 mb-4' />
          <p>Are you sure you want to cancel the scheduled appointment?</p>
        </div>
      </Modal>
    </>
  );
}
