import React, { useEffect } from 'react';
import { Tab, Breadcrumb, Card } from 'components';
import AppointmentsList from './AppointmentsList';
import { listAppointments } from 'redux/actions/appointmentActions';
import { useDispatch, useSelector } from 'react-redux';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Appointments', href: '/app/appointments', current: true },
];
export default function Appointments() {
  const dispatch = useDispatch();
  const { appointments, loading } = useSelector(
    state => state.listAppointments
  );

  const {success} = useSelector(state => state.createAppointment)

  useEffect(() => {
    dispatch(listAppointments({ status: 'Scheduled' }));
  }, [dispatch, success]);

  const filterAppointments = type => {
    return appointments?.details?.filter(appointment => {
      const date = appointment.requestedAppointmentDetails
        ? appointment.scheduledAppointmentDetails?.requestedOn
        : appointment.scheduledAppointmentDetails?.startDate;
      if (type === 'upcoming') {
        return (
          new Date(date) > new Date() 
          || appointment.status !== 'Scheduled'
        );
      }
      return new Date(date) < new Date() && appointment.status === 'Scheduled';
    })?.reverse();
  };

  const tabs = [
    {
      title: 'Upcoming Appointments',
      content: (
        <AppointmentsList
          loading={loading}
          appointments={filterAppointments('upcoming')}
        />
      ),
    },
    {
      title: 'Past Appointments',
      content: (
        <AppointmentsList
          loading={loading}
          appointments={filterAppointments('past')}
        />
      ),
    },
  ];

  return (
    <div>
      <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
        <div className='py-6 md:flex md:items-center md:justify-between'>
          <div className='flex-1 min-w-0'>
            {/* Profile */}
            <div className='flex items-center'>
              <div>
                <div className='flex items-center'>
                  <i className='fas fa-bell text-blue-dark'></i>
                  <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                    Appointments
                  </h1>
                </div>
              </div>
            </div>
          </div>
          <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
            <Breadcrumb pages={pages} />
          </div>
        </div>
      </div>
      <Card
        header={
          <h3 className='text-lg leading-6 font-light text-light'>
            Appointments
          </h3>
        }
      >
        <Tab tabs={tabs} />
      </Card>
    </div>
  );
}
