import React, { useEffect, useState } from 'react';
import { Table, Button, Empty, Modal, Select, Input } from 'components';
import { SearchIcon } from '@heroicons/react/outline';
import { Link } from 'react-router-dom';
import {
  createAppointment,
  getAppointmentTypes,
  getTimeslots,
} from 'redux/actions/appointmentActions';
import { useDispatch, useSelector } from 'react-redux';
import { format } from 'date-fns';
import { Spinner } from 'components';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { searchFilter } from 'lib/searchFilter';

const validationSchema = Yup.object().shape({
  appointmentType: Yup.string().required('An appointment type is required'),
  timeSlot: Yup.string().required('A time slot is required'),
  reason: Yup.string().required('A reason is required'),
});

export default function AppointmentsList({ appointments = [], loading }) {
  const { appointmentTypes } = useSelector(state => state.getAppointmentTypes);
  const { timeslots } = useSelector(state => state.getTimeslots);

  const [appointmentsList, setAppointmentsList] = useState(appointments);

  const [open, setOpen] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAppointmentTypes({ limit: 100 }));
  }, [dispatch]);

  useEffect(() => {
    if (appointments) {
      setAppointmentsList(appointments);
    }
  }, [appointments]);

  const filterAppointments = str => {
    setAppointmentsList(searchFilter(str, appointments));
  };

  const formik = useFormik({
    initialValues: {
      appointmentType: '',
      reason: '',
      timeSlot: '',
    },
    validationSchema,
    onSubmit: values => {
      dispatch(
        createAppointment({
          appointmentType: values.appointmentType,
          reason: values.reason,
          timeSlot: values.timeSlot,
        })
      );
      setOpen(false);
    },
  });

  const columns = [
    {
      title: 'Date of Appointment',
      key: 'date',
      render: (record, _, i) => (
        <div key={i}>
          {record.requestedAppointmentDetails
            ? format(
                new Date(record.requestedAppointmentDetails?.requestedOn),
                'dd/MM/yyyy'
              )
            : format(
                new Date(record.scheduledAppointmentDetails?.startDate),
                'dd/MM/yyyy'
              )}
        </div>
      ),
    },
    {
      title: 'Facility Name',
      key: 'facilityName',
      render: (record, _, i) => (
        <div key={i}>{record?.facilityName || '-'}</div>
      ),
    },
    {
      title: 'Facility Type',
      key: 'facilityType',
      render: (record, _, i) => (
        <div key={i}>{record?.facilityType || '-'}</div>
      ),
    },
    {
      title: 'Doctor to Visit',
      key: 'doctorToVisit',
      render: (record, _, i) => (
        <div key={i}>
          {(record?.scheduledAppointmentDetails
            ? record?.scheduledAppointmentDetails?.provider
            : record?.requestedAppointmentDetails?.provider) || '-'}
        </div>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (record, _, i) => (
        <Link
          key={i}
          to={`/app/appointment/${record.uuid}`}
          onClick={e =>
            localStorage.setItem('appointment', JSON.stringify(record))
          }
          className=' flex justify-center'
        >
          <i className='fa-solid text-lg fa-circle-arrow-right text-blue-dark'></i>
        </Link>
      ),
    },
  ];
  return (
    <div>
      <div className='flex justify-between px-6 mt-8'>
        <Button kind='primary-outline' onClick={() => setOpen(true)}>
          Create Appointment
        </Button>

        <div className='mt-1 relative flex items-center'>
          <input
            type='text'
            name='search'
            id='search'
            placeholder='Search Appointments'
            onChange={e => filterAppointments(e.target.value)}
            className='shadow-sm search-table placeholder:italic focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-12 sm:text-sm border-gray-300 rounded-md'
          />
          <div className='absolute inset-y-0 left-0 flex py-1.5 pl-1.5'>
            <kbd className='inline-flex items-center rounded px-2 text-sm font-sans font-medium text-gray-400'>
              <SearchIcon className='h-5 w-5' />
            </kbd>
          </div>
        </div>
      </div>
      <div>
        <Table columns={columns} data={appointmentsList} kind='primary' />
        {loading && (
          <div className='w-full flex justify-center'>
            <Spinner />
          </div>
        )}
        {!loading && appointments?.length === 0 && (
          <Empty text='No appointments' />
        )}
      </div>
      <Modal
        open={open}
        setOpen={setOpen}
        onClose={() => setOpen(false)}
        onOk={formik.handleSubmit}
        title='Create Appointment'
        okText='Create Appointment'
      >
        <form className='space-y-8 divide-y divide-gray-200'>
          <Select
            placeholder='Appointment Type'
            name='appointmentType'
            onChange={e => {
              formik.handleChange(e);
              dispatch(
                getTimeslots({ appointmentType: e.target.value, limit: 100 })
              );
            }}
            error={
              formik.errors.appointmentType && formik.touched.appointmentType
                ? formik.errors.appointmentType
                : null
            }
          >
            {appointmentTypes?.map((type, i) => (
              <option key={i} value={type.uuid}>
                {type.name}
              </option>
            ))}
          </Select>
          <Select
            placeholder='Time Slot'
            name='timeSlot'
            onChange={formik.handleChange}
            error={
              formik.errors.timeSlot && formik.touched.timeSlot
                ? formik.errors.timeSlot
                : null
            }
          >
            {timeslots?.map((timeslot, i) => (
              <option key={i} value={timeslot.uuid}>
                {format(new Date(timeslot.startDate), 'dd/MM/yyyy hh:mm a')} -{' '}
                {format(new Date(timeslot.endDate), 'dd/MM/yyyy hh:mm a')}
              </option>
            ))}
          </Select>
          <Input
            type='textarea'
            placeholder='Reason for Appointment'
            name='reason'
            onChange={formik.handleChange}
            error={
              formik.errors.reason && formik.touched.reason
                ? formik.errors.reason
                : null
            }
          />
        </form>
      </Modal>
    </div>
  );
}
