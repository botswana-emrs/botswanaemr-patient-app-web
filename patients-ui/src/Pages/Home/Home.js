import React, { useEffect, useState } from 'react';
import { HomeIcon } from '@heroicons/react/solid';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { getUser } from 'redux/actions/userActions';
import { listAppointments } from 'redux/actions/appointmentActions';

export default function Home() {
  const { user } = useSelector(state => state.getUser);
  const { appointments, loading } = useSelector(
    state => state.listAppointments
  );

  const [upcomingAppointments, setUpcomingAppointments] = useState(0);

  const cards = [
    {
      name: 'Appointments',
      href: '/app/appointments',
      icon: 'fa-solid fa-kit-medical',
    },
    {
      name: 'Facilities',
      href: '/app/facilities',
      icon: 'fas fa-hospital',
    },
    {
      name: 'Ambulance',
      href: '/app/ambulance',
      icon: 'fa-solid fa-ambulance',
    },
    {
      name: 'Your Records',
      href: '/app/appointments',
      icon: 'fas fa-clipboard-list',
    },
  ];

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser());
    dispatch(listAppointments({ status: 'Scheduled' }));
  }, [dispatch]);

  const filterScheduledUpcomingAppointments = () => {
    return appointments?.details?.filter(appointment => {
      const date = appointment.scheduledAppointmentDetails?.startDate;
      const endDate = appointment.scheduledAppointmentDetails?.endDate;
      const isUpcoming =
        new Date(date) > new Date() && new Date(endDate) > new Date();
      return isUpcoming && appointment.status === 'Scheduled';
    });
  };

  useEffect(() => {
    if (appointments?.details) {
      setUpcomingAppointments(filterScheduledUpcomingAppointments().length);
    }
  }, [appointments]);

  return (
    <>
      <div className='bg-gradient-to-b from-lblue-light to-light home'>
        <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8 home1'>
          <div className='py-6 md:flex md:items-center md:justify-between'>
            <div className='flex-1 min-w-0'>
              {/* Profile */}
              <div className='flex items-center'>
                <div>
                  <div className='flex items-center'>
                    <h1 className='ml-3 text-2xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                      {user && `Welcome home ${user?.firstName}.`}
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
              <button
                type='button'
                className='inline-flex items-center px-4 py-5 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-light bg-blue-dark  focus:outline-none focus:ring-0  focus:ring-none'
              >
                {upcomingAppointments} Upcoming Visits
              </button>
              <button
                type='button'
                className='inline-flex items-center px-4 py-5 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-light bg-blue-dark  focus:outline-none focus:ring-0  focus:ring-none'
              >
                0 Results Out
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className='mt-8'>
        <div className='max-w-6xl mx-auto px-4 py-3 sm:px-6 lg:px-8 shadow bg-[white]'>
          <h2 className='text-lg leading-6 font-light text-gray-900'>
            Quick Links
          </h2>
          <div className='mt-2 grid grid-cols-1 gap-5 sm:grid-cols-2 lg:grid-cols-4'>
            {/* Card */}
            {cards.map(card => (
              <Link
                key={card.name}
                to={card.href}
                className='bg-white flex flex-col justify-center items-center py-5 overflow-hidden'
              >
                <div className='bg-[white] flex flex-col w-2/3 justify-center items-center py-5 overflow-hidden shadow rounded-lg'>
                  <i className={`${card.icon} text-5xl text-blue-dark`}></i>
                  <p className='font-light py-3'>{card.name}</p>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
