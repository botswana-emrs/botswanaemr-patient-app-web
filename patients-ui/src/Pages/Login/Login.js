import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { login } from 'redux/actions/userActions';
import useInputs from 'hooks/useInput';
import { Alert, Input, Button, CheckBox, Form, Spinner } from 'components';

export default function Login() {
  const { error, loading } = useSelector(state => state.userSignin);
  const dispatch = useDispatch();
  const [inputs, changeHandler] = useInputs({});

  const submitHandler = e => {
    e.preventDefault();
    dispatch(login(inputs));
  };
  return (
    <Form className='space-y-6 py-6 px-6 md:px-10' onSubmit={submitHandler}>
      <h1 className='text-3xl font-light text-center text-gray-900'>Welcome</h1>
      {error ? (
        <Alert kind='error' title='Error' message={error} />
      ) : (
        <p className='text-center font-light text-xs'>
          Please enter your email and password to access your account.
        </p>
      )}
      <Input
        name='emailAddress'
        type='email'
        autoComplete='off'
        placeholder='Email Address'
        label='Email Address'
        onChange={changeHandler}
        required
      />

      <Input
        name='password'
        type='password'
        autoComplete='off'
        onChange={changeHandler}
        required
        placeholder='password'
        label='Password'
      />
      <div className='flex items-center justify-between'>
        <CheckBox id='remember-me' name='remember-me' label='Remember me' />

        <div className='text-sm'>
          <Link
            to='/auth/password-reset'
            className='font-medium text-[gray] text-xs'
          >
            Forgot your password?
          </Link>
        </div>
      </div>

      <div>
        <Button type='submit' full disabled={loading}>
          {loading && <Spinner />} Sign in
        </Button>
      </div>
      <p className='text-xs text-[gray] text-center'>
        Don't have an account?{' '}
        <a href='/auth/register' className='font-semibold'>
          Sign up
        </a>
      </p>
    </Form>
  );
}
