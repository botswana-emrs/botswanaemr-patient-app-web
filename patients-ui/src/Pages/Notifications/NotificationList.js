import React from 'react';
import NotificationItem from 'components/NotificationItem';

export default function NotificationList({ data }) {
  return (
    <ul role='list' className='divide-y divide-gray-200'>
      {data?.map(item => (
        <NotificationItem key={item.id} notification={item} />
      ))}
    </ul>
  );
}
