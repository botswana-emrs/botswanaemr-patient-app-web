import React, { useEffect } from 'react';
import { Card, Breadcrumb } from 'components';
import NotificationTabs from './NotificationTabs';
import { useDispatch, useSelector } from 'react-redux';
import { getNotifications } from 'redux/actions/notificationActions';

const pages = [
  { name: 'Home', href: '/app/dashboard', current: false },
  { name: 'Notifications', href: '/app/notifications', current: true },
];
export default function Notifications() {
  const { notifications, loading } = useSelector(
    state => state.getNotifications
  );
  const { notification } = useSelector(state => state.updateNotification);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      getNotifications({
        pageSize: 10,
        pageNumber: 1,
        all: true,
      })
    );
  }, []);
  return (
    <div>
      <div className=''>
        <div className='px-4 sm:px-6 lg:max-w-6xl lg:mx-auto lg:px-8'>
          <div className='py-6 md:flex md:items-center md:justify-between'>
            <div className='flex-1 min-w-0'>
              {/* Profile */}
              <div className='flex items-center'>
                <div>
                  <div className='flex items-center'>
                    <i className='fas fa-bell text-blue-dark'></i>
                    <h1 className='ml-3 text-xl font-light leading-7 text-gray-900 sm:leading-9 sm:truncate'>
                      Notifications
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <div className='mt-6 flex space-x-3 md:mt-0 md:ml-4'>
              <Breadcrumb pages={pages} />
            </div>
          </div>
        </div>
      </div>
      <Card
        header={
          <h3 className='text-lg leading-6 font-light text-light'>
            All Notifications
          </h3>
        }
      >
        <NotificationTabs notifications={notifications} />
      </Card>
    </div>
  );
}
