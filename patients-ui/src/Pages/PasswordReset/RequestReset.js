import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { forgotPassword } from 'redux/actions/userActions';
import useInputs from 'hooks/useInput';
import { Alert, Input, Button, Form, Spinner } from 'components';

export default function RequestReset() {
  const { success, error, loading } = useSelector(
    state => state.forgotPassword
  );
  const dispatch = useDispatch();
  const [inputs, changeHandler] = useInputs({});

  const submitHandler = e => {
    e.preventDefault();
    dispatch(forgotPassword({ emailAddress: inputs.emailAddress }));
  };

  useEffect(() => {
   if (success) {
     setTimeout(() => {
       window.location.href = '/auth/reset-password';
     }, 3000);
   }
  }, [success]);


  return (
    <Form className='space-y-6 py-6 px-6 md:px-10' onSubmit={submitHandler}>
      {error ? (
        <Alert kind='error' title='Error' message={error} />
      ) : (
        <p className='text-center font-light text-xs'>
          Please enter your email address.
        </p>
      )}
      {success ? (
        <Alert kind='success' title='Success' message={success?.message} />
      ) : (
        <>
          <Input
            name='emailAddress'
            type='email'
            autoComplete='off'
            placeholder='Email Address'
            label='Email Address'
            onChange={changeHandler}
            required
          />

          <div>
            <Button type='submit' full disabled={loading}>
              {loading && <Spinner />} Submit
            </Button>
          </div>
        </>
      )}
      <p className='text-xs text-[gray] text-center'>
        <Link to='/auth/login' className='font-semibold'>
          Sign in
        </Link>
      </p>
    </Form>
  );
}
