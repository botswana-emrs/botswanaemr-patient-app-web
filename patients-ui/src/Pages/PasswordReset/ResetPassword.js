import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { resetPassword } from 'redux/actions/userActions';
import { Alert, Input, Button, Form, Spinner } from 'components';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
  emailAddress: Yup.string()
    .email('Invalid email address')
    .required('Required'),
  password: Yup.string().required('Required'),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password'), null],
    'Passwords must match'
  ),
  otpCode: Yup.string().required('Required'),
});

export default function ResetPassword() {
  const { success, error, loading } = useSelector(state => state.resetPassword);
  const dispatch = useDispatch();

  const formik = useFormik({
    initialValues: {
      emailAddress: '',
      password: '',
      confirmPassword: '',
      otpCode: '',
    },
    validationSchema,
    onSubmit: values => {
      dispatch(resetPassword(values));
    },
  });

  useEffect(() => {
    if (success) {
      setTimeout(() => {
        window.location.href = '/auth/login';
      }, 5000);
    }
  }, [success]);

  return (
    <Form
      className='space-y-6 py-6 px-6 md:px-10'
      onSubmit={formik.handleSubmit}
    >
      {error ? (
        <Alert kind='error' title='Error' message={error} />
      ) : (
        <p className='text-center font-light text-xs'>
          Fill in the form below to reset your password.
        </p>
      )}
      {success ? (
        <Alert kind='success' title='Success' message={success} />
      ) : (
        <>
          <Input
            name='emailAddress'
            type='email'
            autoComplete='off'
            placeholder='Email Address'
            label='Email Address'
            onChange={formik.handleChange}
            required
            error={
              formik.touched.emailAddress && formik.errors.emailAddress
                ? formik.errors.emailAddress
                : null
            }
          />

          <Input
            name='otpCode'
            type='text'
            autoComplete='off'
            placeholder='OTP Code'
            label='OTP Code'
            onChange={formik.handleChange}
            required
            error={
              formik.touched.otpCode && formik.errors.otpCode
                ? formik.errors.otpCode
                : null
            }
          />

          <Input
            name='password'
            type='password'
            autoComplete='off'
            onChange={formik.handleChange}
            required
            placeholder='password'
            label='Password'
            error={
              formik.touched.password && formik.errors.password
                ? formik.errors.password
                : null
            }
          />

          <Input
            name='confirmPassword'
            type='password'
            autoComplete='off'
            onChange={formik.handleChange}
            required
            placeholder='Confirm Password'
            label='Confirm Password'
            error={
              formik.touched.confirmPassword && formik.errors.confirmPassword
                ? formik.errors.confirmPassword
                : null
            }
          />

          <div>
            <Button type='submit' full disabled={loading}>
              {loading && <Spinner />} Reset Password
            </Button>
          </div>
        </>
      )}
      <p className='text-xs text-[gray] text-center'>
        <Link to='/auth/login' className='font-semibold'>
          Sign in
        </Link>
      </p>
    </Form>
  );
}
