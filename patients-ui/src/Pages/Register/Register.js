import React, { useState, useRef, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import RegisterForm from './RegisterForm';
import { register } from 'redux/actions/userActions';
import { Alert, Success, Button, Spinner } from 'components';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import uploadFile from 'lib/fileUpload';

const tabs = [
  { name: 'Add your personal details', href: '#' },
  {
    name: 'Add your contact & Identification details',
    href: '#',
  },
  { name: 'Set up your account', href: '#' },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const schema = Yup.object().shape({
  firstName: Yup.string().required('First name is required'),
  lastName: Yup.string().required('Last name is required'),
  emailAddress: Yup.string()
    .email('Invalid email address')
    .required('Email address is required'),
  // should not be greater than today's date
  dateOfBirth: Yup.date()
    .max(new Date(), 'Date of birth should not be greater than today')
    .required('Date of birth is required'),
  gender: Yup.string().required('Gender is required'),
  phoneNumber: Yup.string().required('Phone number is required'),
  patientIdentificationNo: Yup.string(),
  identificationType: Yup.string().required('Identification type is required'),
  nationalPassportNo: Yup.string().required(
    'National passport number is required'
  ),
  username: Yup.string().required('Username is required'),
  password: Yup.string().required('Password is required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Please confirm your password'),
});

export default function Register() {
  const [currentTab, setCurrentTab] = useState(0);
  const [errors, setErrors] = useState('');
  const { userInfo, error, loading } = useSelector(state => state.userSignup);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const formRef = useRef(null);

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      emailAddress: '',
      dateOfBirth: '',
      gender: '',
      phoneNumber: '',
      patientIdentificationNo: '',
      identificationType: '',
      nationalPassportNo: '',
      username: '',
      password: '',
      confirmPassword: '',
      imageUrl: '',
    },
    validationSchema: schema,
    onSubmit: async values => {
      if (!formik.values.imageUrl) {
        let errorMessage = 'Please upload your photo';

        setErrors(errorMessage);
        return;
      }

      const uploadedFile = await uploadFile(formik.values.imageUrl);
      dispatch(
        register({ ...values, imageUrl: uploadedFile?.fileDownloadUri })
      );
    },
    // clear errors on form change
    onChange: () => {
      setErrors('');
    }
  });

  const submitForm = async () => {
    const validateForm = await formik.validateForm();
    if (Object.keys(validateForm).length > 0) {
      formik.setTouched(validateForm);
      let errorMessage = 'Please fill in the required fields';
      setErrors(errorMessage);
      return;
    }
    formik.handleSubmit();
  };

  const handleTabClick = index => {
    setCurrentTab(index);
  };

  const handleBackClick = () => {
    setCurrentTab(currentTab - 1);
  };

  const handleNextClick = () => {
    setCurrentTab(currentTab + 1);
  };

  useEffect(() => {
    if (userInfo) {
      const timer = setTimeout(() => {
        formRef.current.reset();
        navigate('/auth/login');
      }, 5000);
      return () => clearTimeout(timer);
    }
  }, [userInfo]);

  return (
    <div>
      <div className='sm:hidden'>
        <label htmlFor='tabs' className='sr-only'>
          Select a tab
        </label>
        <select
          id='tabs'
          name='tabs'
          className='block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md'
          defaultValue={tabs[0].name}
        >
          {tabs.map(tab => (
            <option key={tab.name}>{tab.name}</option>
          ))}
        </select>
      </div>
      <div className='hidden sm:block'>
        <nav className='flex space-x-4 bg-lblue-light py-3' aria-label='Tabs'>
          {tabs.map((tab, i) => (
            <a
              key={tab.name}
              href={tab.href}
              className={classNames(
                currentTab === i
                  ? 'bg-indigo-100 text-indigo-700'
                  : 'text-[gray] hover:text-gray-700',
                'px-3 py-2 font-light text-xs rounded-md'
              )}
              aria-current={currentTab === i ? 'page' : undefined}
              onClick={() => handleTabClick(i)}
            >
              <button
                className={classNames(
                  currentTab === i
                    ? 'bg-blue-dark text-light py-1 px-2 rounded mr-1'
                    : 'bg-lblue text-light py-1 px-2 rounded mr-1',
                  'shadow'
                )}
              >
                {i + 1}
              </button>
              {tab.name}
            </a>
          ))}
        </nav>
      </div>
      <div id='tab1'>
        <div className='bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6'>
          <div className='mt-5 md:mt-0 md:col-span-2'>
            {(error || errors) && (
              <Alert title='Error!' message={error || errors} />
            )}
            {userInfo && (
              <Success
                title='Registration successful!'
                message='Please check your email to verify your account.'
              />
            )}
            <RegisterForm
              currentTab={currentTab}
              formRef={formRef}
              formik={formik}
            />
          </div>
          <div className='w-full my-6 border-t border-gray-300' />
          <div>
            <div className='flex items-center justify-between'>
              <p className='text-xs text-gray-500'>
                Already have an account?{' '}
                <Link to='/auth/login' className='font-semibold text-gray-500'>
                  Log In
                </Link>
              </p>
              <div className='flex'>
                {currentTab > 0 && (
                  <Button
                    className='mr-4'
                    onClick={handleBackClick}
                    size='small'
                    kind='light'
                  >
                    Back
                  </Button>
                )}
                {currentTab < 2 ? (
                  <Button size='small' onClick={handleNextClick}>
                    Next
                  </Button>
                ) : (
                  <Button
                    type='submit'
                    size='small'
                    onClick={submitForm}
                    disabled={loading}
                  >
                    {loading && <Spinner />} Finish
                  </Button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
