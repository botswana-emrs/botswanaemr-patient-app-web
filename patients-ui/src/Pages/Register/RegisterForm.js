import React, { useState, useEffect } from 'react';
import { Form, Input, Select } from 'components';
import camera from 'assets/img/camera.png';

export default function RegisterForm({ currentTab, formRef, formik }) {
  const [image, setImage] = useState(null);
  function focusDate(e) {
    e.target.type = 'date';
  }

  useEffect(() => {
    if (formik.values.imageUrl) {
      const reader = new FileReader();
      reader.readAsDataURL(formik.values.imageUrl);
      reader.onload = () => {
        setImage(reader.result);
      };
    }
  }, [formik.values.imageUrl]);


  return (
    <Form ref={formRef}>
      <div className='grid grid-cols-6 gap-6'>
        {currentTab === 0 && (
          <>
            <div className='col-span-6 flex items-center mb-6'>
              <div className='profile-pic-container shadow mr-3'>
                <input
                  type='file'
                  name='imageUrl'
                  onChange={e => {
                    formik.setFieldValue('imageUrl', e.target.files[0]);
                  }}
                  id='imageUrl'
                  className='my-4 profile-img-input'
                  accept='image/*'
                  required
                />
                <img src={image || camera} alt='profile' className='w-full' />
              </div>
              <label
                htmlFor='file'
                className={`${formik.errors.imageUrl && 'text-red'} text-sm`}
              >
                Set your profile picture
              </label>
            </div>
            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='firstName'
                label='First name'
                placeholder='First name'
                required
                onChange={formik.handleChange}
                value={formik.values.firstName}
                error={formik.touched.firstName && formik.errors.firstName}
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='lastName'
                required
                onChange={formik.handleChange}
                value={formik.values.lastName}
                placeholder='Last name'
                label='Last name'
                error={formik.touched.lastName && formik.errors.lastName}
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='dateOfBirth'
                onFocus={focusDate}
                onChange={formik.handleChange}
                id='dateOfBirth'
                placeholder='Date of birth'
                required
                value={formik.values.dateOfBirth}
                label='Date of birth'
                error={formik.touched.dateOfBirth && formik.errors.dateOfBirth}
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Select
                id='gender'
                name='gender'
                onChange={formik.handleChange}
                value={formik.values.gender}
                required
                label='Gender'
                placeholder='Select your gender'
                error={formik.touched.gender && formik.errors.gender}
              >
                <option value='Male'>Male</option>
                <option value='Female'>Female</option>
                <option value='Other'>Other</option>
              </Select>
            </div>
          </>
        )}

        {currentTab === 1 && (
          <>
            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='emailAddress'
                id='emailAddress'
                onChange={formik.handleChange}
                value={formik.values.emailAddress}
                label='Email address'
                placeholder='Email address'
                error={
                  formik.touched.emailAddress && formik.errors.emailAddress
                }
                required
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='tel'
                name='phoneNumber'
                id='phoneNumber'
                placeholder='Phone number'
                onChange={formik.handleChange}
                value={formik.values.phoneNumber}
                label='Phone number'
                error={formik.touched.phoneNumber && formik.errors.phoneNumber}
                required
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='patientIdentificationNo'
                id='patientIdentificationNo'
                placeholder='Patient identification number'
                onChange={formik.handleChange}
                value={formik.values.patientIdentificationNo}
                label='Patient Identification Number(PIN)'
                error={
                  formik.touched.patientIdentificationNo &&
                  formik.errors.patientIdentificationNo
                }
                required
              />
            </div>
            <div className='col-span-6 sm:col-span-6 p-3 rounded bg-light'>
              <h3 className='text-lg leading-6 font-light text-blue-dark'>
                Patient Identification
              </h3>
            </div>
            <div className='col-span-6 sm:col-span-3'>
              <Select
                id='identificationType'
                name='identificationType'
                onChange={formik.handleChange}
                value={formik.values.identificationType}
                error={
                  formik.touched.identificationType &&
                  formik.errors.identificationType
                }
                label='Identification Type'
                placeholder='Select identification type'
                required
              >
                <option value='National Id'>National ID</option>
                <option value='Passport'>Passport</option>
              </Select>
            </div>
            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='nationalPassportNo'
                onChange={formik.handleChange}
                value={formik.values.nationalPassportNo}
                id='nationalPassportNo'
                placeholder='National ID/Passport Number'
                error={
                  formik.touched.nationalPassportNo &&
                  formik.errors.nationalPassportNo
                }
                required
              />
            </div>
          </>
        )}
        {currentTab === 2 && (
          <>
            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='text'
                name='username'
                onChange={formik.handleChange}
                value={formik.values.username}
                label='Username'
                placeholder='Username'
                error={formik.touched.username && formik.errors.username}
                required
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='password'
                name='password'
                onChange={formik.handleChange}
                value={formik.values.password}
                id='password'
                placeholder='Password'
                label='Password'
                error={formik.touched.password && formik.errors.password}
                required
              />
            </div>

            <div className='col-span-6 sm:col-span-3'>
              <Input
                type='password'
                name='confirmPassword'
                id='confirmPassword'
                placeholder='Confirm password'
                onChange={formik.handleChange}
                value={formik.values.confirmPassword}
                label='Confirm password'
                error={
                  formik.touched.confirmPassword &&
                  formik.errors.confirmPassword
                }
                required
              />
            </div>
          </>
        )}
      </div>
    </Form>
  );
}
