import Ambulance from 'Pages/Ambulance/Ambulance';
import AmbulanceDetails from 'Pages/AmbulanceDetails/AmbulanceDetails';
import AppointmentDetails from 'Pages/Appointmentdetails/Appointmentdetails';
import Appointments from 'Pages/Appointments/Appointments';
import Facilities from 'Pages/Facilities/Facilities';
import Facility from 'Pages/Facility/Facility';
import Home from 'Pages/Home/Home';
import Notifications from 'Pages/Notifications/Notifications';

const routes = [
  {
    path: '/dashboard',
    element: <Home />,
    exact: true,
  },
  {
    path: '/facilities',
    element: <Facilities />,
  },
  {
    path: '/facilities/:id',
    element: <Facility />,
  },
  {
    path: '/notifications',
    element: <Notifications />,
  },
  {
    path: '/appointments',
    element: <Appointments />,
  },
  {
    path: '/appointment/:id',
    element: <AppointmentDetails />,
  },
  {
    path: '/ambulance',
    element: <Ambulance />,
  },
  {
    path: '/ambulance/:id',
    element: <AmbulanceDetails />,
  }
];

export default routes;
