import Login from 'Pages/Login/Login';
import RequestReset from 'Pages/PasswordReset/RequestReset';
import ResetPassword from 'Pages/PasswordReset/ResetPassword';
import Register from 'Pages/Register/Register';

const routes = [
  {
    path: '/login',
    component: Login,
    exact: true,
    title: 'Login',
  },
  {
    path: '/register',
    component: Register,
    exact: true,
    title: 'Create an account',
    width: 'auth-signup-form',
  },
  {
    path: '/password-reset',
    component: RequestReset,
    exact: true,
    title: 'Reset Password',
  },
  {
    path: '/reset-password',
    component: ResetPassword,
    exact: true,
    title: 'Reset Password',
  }
];

export default routes;
