import React from 'react';
import { XCircleIcon } from '@heroicons/react/solid';

export default function Alert({
  color = 'red',
  message,
  title,
  kind = 'error',
}) {
  const colorMap = {
    error: 'bg-red-light border-red',
    success: 'bg-green-50 border-green',
    warning: 'bg-yellow-50 border-yellow',
    info: 'bg-blue-50 border-blue',
  };

  const textColorMap = {
    error: 'red',
    success: 'green',
    warning: 'yellow',
    info: 'blue',
  };
  return (
    <div className={` border-l-4  p-4 mb-3 ${colorMap[kind]}`}>
      <div className='flex'>
        <div className='flex-shrink-0'>
          <XCircleIcon
            className={`h-5 w-5 text-${textColorMap[kind]}-800`}
            aria-hidden='true'
          />
        </div>
        <div className='ml-3'>
          <p className={`text-sm text-${textColorMap[kind]}-800`}>{message}</p>
        </div>
      </div>
    </div>
  );
}
