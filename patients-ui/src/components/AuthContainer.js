import React from 'react';
import logo from 'assets/img/botswana.png';

export default function AuthContainer({ title, children, width = 'auth-login-form' }) {
  return (
    <>
      <div className={`sm:mx-auto sm:w-full  ${width}`}>
        <img className='mx-auto w-36' src={logo} alt='Workflow' />
        <div className='bg-blue-dark'>
          <h5 className='mt-6 text-center text-xl font-light text-light py-3'>
            {title}
          </h5>
        </div>
      </div>

      <div className={`sm:mx-auto sm:w-full bg-[white] ${width}`}>
        <div className='bg-white shadow  '>{children}</div>
      </div>
    </>
  );
}
