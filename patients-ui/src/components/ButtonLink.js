import React from 'react';

export default function ButtonLink({ children, ...rest }) {
  return (
    <a
    {...rest}
    className={`flex items-center bg-light px-6 py-2 rounded-md text-xs mr-1 ${rest.className}`}
    >
      {children}
    </a>
  );
}
