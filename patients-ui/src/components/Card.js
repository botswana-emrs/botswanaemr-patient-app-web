export default function Card({ children, header, footer }) {
  return (
    <div className='max-w-6xl mx-auto shadow bg-[white]'>
      <div className='px-4 py-2 border-b border-gray-200 sm:px-6 bg-lblue'>
        {header}
      </div>
      <div className='px-4 py-2'>{children}</div>
      {footer && (
        <div className='px-4 py-2 border-b border-gray-200 sm:px-6 bg-light-100'>
          {footer}
        </div>
      )}
    </div>
  );
}
