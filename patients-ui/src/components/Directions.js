import React, { useEffect, useState, useRef } from 'react';

const GoogleMapsDirections = ({ destination }) => {
  const mapContainerRef = useRef(null);
  const defaultLocation = { lat: -24.654, lng: 25.908 };

  console.log(destination);

  useEffect(() => {
    // Replace YOUR_API_KEY with your actual API key
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}`;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);

    script.addEventListener('load', () => {
      const directionsService = new window.google.maps.DirectionsService();
      const directionsRenderer = new window.google.maps.DirectionsRenderer();
      const map = new window.google.maps.Map(mapContainerRef.current, {
        zoom: 7,
        center: defaultLocation,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false,
        scaleControl: false,

      });
      directionsRenderer.setMap(map);

      // Use the Geolocation API to get the user's current location
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(position => {
          const origin = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

          const request = {
            origin: origin,
            destination: { lat: Number(destination.lat), lng: Number(destination.lng)},
            travelMode: 'DRIVING',
          };

          directionsService.route(request, (result, status) => {
            if (status === 'OK') {
              directionsRenderer.setDirections(result);
            } else {
              console.error(`Error fetching directions: ${result}`);
            }
          });
        });
      } else {
        console.error('Geolocation is not supported by this browser.');
      }
    });
  }, [destination]);

  return <div ref={mapContainerRef} style={{ height: '400px' }} />;
};

export default GoogleMapsDirections;
