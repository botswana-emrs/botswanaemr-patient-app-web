import React, { useLayoutEffect, useRef, useState } from 'react';
import hospitalMarker from 'assets/img/hospital.png';

const defaultLocation = { lat: -24.654, lng: 25.908 };

const GoogleMap = ({ locations = [] }) => {
  const mapRef = useRef();

  useLayoutEffect(() => {
    const googleMapsScript = document.createElement('script');
    googleMapsScript.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}`;
    googleMapsScript.async = true;
    googleMapsScript.defer = true;
    document.body.appendChild(googleMapsScript);

    googleMapsScript.addEventListener('load', () => {
      const map = new window.google.maps.Map(mapRef.current, {
        zoom: 8,
        center: locations[0] || defaultLocation,
        mapTypeControl: false,
        fullscreenControl: false,
        streetViewControl: false,
      });

      locations?.forEach(hospital => {
        const marker = new window.google.maps.Marker({
          position: hospital,
          map,
          icon: {
            url: hospitalMarker,
            scaledSize: new window.google.maps.Size(30, 30),
          },
        });

        marker.addListener('click', () => {
          new window.google.maps.InfoWindow({
            content: `<a href="/app/facilities/${hospital.newFacilityCode}">
                ${hospital.facilityName}
              </a>`,
            position: hospital,
          }).open(map);
        });
      });
    });
  }, [locations]);

  return <div ref={mapRef} style={{ height: '100%', width: '100%' }} />;
};

export default GoogleMap;
