import React from 'react';

export default function Input(props) {
  const { type = 'text', error, className, ...rest } = props;
  return (
    <div className='mt-1'>
      {rest.label && (
        <label htmlFor={rest.name} className=''>
          {rest.label} {rest.required && <span className='text-red'>*</span>}
        </label>
      )}
      <>
        {type === 'textarea' ? (
          <textarea
            {...rest}
            className={`appearance-none block w-full px-3 py-2 border-light rounded-md shadow placeholder-gray-400 focus:outline-none focus:ring-blue-dark focus:border-blue-dark sm:text-sm ${className}`}
          />
        ) : (
          <input
            {...rest}
            type={type}
            className={`appearance-none block w-full px-3 py-2 border-light rounded-md shadow placeholder-gray-400 focus:outline-none focus:ring-blue-dark focus:border-blue-dark sm:text-sm ${className}`}
          />
        )}
      </>
      {error && <p className='mt-2 text-xs text-red'>{error}</p>}
    </div>
  );
}
