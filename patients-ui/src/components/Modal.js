import { Fragment, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';
import Button from './Button';

export default function Modal({
  title,
  open,
  setOpen,
  onOk,
  noCancel = false,
  okText = 'Ok',
  onCancel,
  cancelText = 'Cancel',
  children,
}) {
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as='div' className='relative z-[999]' onClose={setOpen}>
        <Transition.Child
          as={Fragment}
          enter='ease-out duration-300'
          enterFrom='opacity-0'
          enterTo='opacity-100'
          leave='ease-in duration-200'
          leaveFrom='opacity-100'
          leaveTo='opacity-0'
        >
          <div className='fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity' />
        </Transition.Child>

        <div className='fixed inset-0 z-[999] overflow-y-auto'>
          <div className='md:flex sm:flex xs:block min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0'>
            <Transition.Child
              as={Fragment}
              enter='ease-out duration-300'
              enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
              enterTo='opacity-100 translate-y-0 sm:scale-100'
              leave='ease-in duration-200'
              leaveFrom='opacity-100 translate-y-0 sm:scale-100'
              leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
            >
              <Dialog.Panel className='relative transform overflow-hidden bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg'>
                <div className='relative bg-blue-dark w-full flex p-3'>
                  <div className='absolute top-0 right-0 hidden pt-3 pr-4 sm:block'>
                    <button
                      type='button'
                      className='rounded-full h-6 w-6 bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-0 focus:ring-transparent focus:ring-offset-2'
                      onClick={() => setOpen(false)}
                    >
                      <span className='sr-only'>Close</span>
                      {/* <XMarkIcon className='h-6 w-6' aria-hidden='true' /> */}
                      X
                    </button>
                  </div>
                  <div className='sm:flex sm:items-start'>
                    <div className='text-center sm:mt-0 sm:ml-4 sm:text-left'>
                      <Dialog.Title
                        as='h3'
                        className='text-lg font-medium leading-6 text-white'
                      >
                        {title}
                      </Dialog.Title>
                    </div>
                  </div>
                </div>
                <div className='mt-2 px-6 py-4'>{children}</div>

                <div className='bg-gray-50 px-4 py-3 mt-5 sm:mt-4 sm:flex sm:flex-row-reverse'>
                  <Button
                    type='button'
                    className={`inline-flex  px-4 py-2 text-base font-medium sm:ml-3 sm:text-sm ${
                      noCancel ? 'w-full sm:w-full' : 'sm:w-auto'
                    }`}
                    onClick={onOk || (() => setOpen(false))}
                  >
                    {okText}
                  </Button>
                  {!noCancel && (
                    <button
                      type='button'
                      className='mt-3 inline-flex w-full justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-base font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:mt-0 sm:w-auto sm:text-sm'
                      onClick={onCancel || (() => setOpen(false))}
                    >
                      {cancelText}
                    </button>
                  )}
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
