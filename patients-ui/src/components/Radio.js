import React from 'react';

export default function Radio(props) {
  const {
    className = 'focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300',
    ...rest
  } = props;
  return (
    <div className='mt-4 space-y-4'>
      <div className='flex items-center'>
        <input type='radio' className={className} {...rest} />
        <label
          htmlFor='push-everything'
          className='ml-3 block text-sm font-medium text-gray-700'
        >
          {props.children}
        </label>
      </div>
    </div>
  );
}
