import React from 'react';

export default function Select(props) {
  const {
    className = 'shadow-sm focus:ring-1 focus:ring-gray focus:border-none block w-full sm:text-sm border-gray-300 rounded-sm',
    placeholder,
    error,
    ...rest
  } = props;

  return (
    <div className='mt-1'>
      <select defaultValue='' className={className} {...rest}>
        {placeholder && (
          <option value='' className='font-extralight' disabled>
            {placeholder}
          </option>
        )}
        {props.children}
      </select>
     {error && <p className='mt-2 text-xs text-red'>{error}</p>}
    </div>
  );
}
