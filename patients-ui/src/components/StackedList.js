import React from 'react';

export default function StackedList({ item, ...props }) {
  return (
    <tr className={props.className}>
      <td className='border border-collapse py-2 px-3'>{item.key}</td>
      <td className='border border-collapse py-2 px-3'>{item.value}</td>
    </tr>
  );
}
