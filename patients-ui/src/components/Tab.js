import React, { useState } from 'react';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Tab({ tabs }) {
  const [currentTab, setCurrentTab] = useState(0);
  return (
    <div>
      <div className='sm:hidden'>
        <label htmlFor='tabs' className='sr-only'>
          Select a tab
        </label>
        <select
          id='tabs'
          name='tabs'
          className='block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md'
          defaultValue={tabs.find((tab, index) => index === currentTab).name}
          onChange={e => {
            setCurrentTab(Number(e.target.selectedIndex));
          }}
        >
          {tabs.map(tab => (
            <option key={tab.title}>{tab.title}</option>
          ))}
        </select>
      </div>
      <div className='hidden sm:block'>
        <div className='border-b border-gray-200'>
          <nav className='-mb-px flex' aria-label='Tabs'>
            {tabs.map((tab, index) => (
              <p
                key={tab.title}
                className={classNames(
                  index === currentTab
                    ? 'border-blue-dark border-b-4 font-bold'
                    : 'border-blue-dark border-b text-gray-500 hover:font-bold',
                  'w-1/2 py-4 px-1 cursor-pointer text-center  font-medium text-sm text-blue-dark'
                )}
                onClick={() => setCurrentTab(index)}
                aria-current={index === currentTab ? 'page' : undefined}
              >
                {tab.title}
              </p>
            ))}
          </nav>
        </div>
      </div>
      {tabs.map((tab, index) => (
        <div key={tab.title}>{index === currentTab && tab.content}</div>
      ))}
    </div>
  );
}
