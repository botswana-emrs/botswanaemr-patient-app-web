import React from 'react';
import TableBody from './TableBody';
import Pagination from './Pagination';

export default function Table({ columns = [], data = [] }) {
  const len = 10;
  return (
    <Pagination
      data={data}
      RenderComponent={TableBody}
      pageLimit={Math.ceil(data?.length / len)}
      dataLimit={len}
      columns={columns}
    />
  );
}
