export const filterFacilities = (facilities, filter) => {
  return facilities.filter(facility => {
    // convert filter object to array of values to compare the names against facility object
    return Object.entries(filter).every(([key, value]) => {
      if (value && key !== 'facilityOwner') {
        return facility[key]?.name.toLowerCase() === value.toLowerCase();
      }
      return value && facility[key].toLowerCase() === value.toLowerCase();
    });
  });
};

export const searchFacilities = (facilities, search) => {
  return facilities?.filter(facility => {
    return facility.facilityName.toLowerCase().includes(search.toLowerCase());
  });
};
