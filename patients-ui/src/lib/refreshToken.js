import Cookies from 'js-cookie';

export const refreshToken = () => {
  Cookies.remove('userInfo');
  window.location.reload();
};
