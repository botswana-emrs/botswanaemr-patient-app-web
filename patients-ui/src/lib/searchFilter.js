export const searchFilter = (str, data) => {
  if (str) {
    const filteredData = data.filter(item => {
      return JSON.stringify(item).toLowerCase().includes(str.toLowerCase());
    });
    return filteredData;
  }
  return data;
};
