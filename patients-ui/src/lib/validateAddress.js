// check if lat and lng are valid and in the right format

export const validateAddress = (locations = []) => {
  const validLocations = [];
  locations.forEach(location => {
    if (location.lat && location.lng) {
      const lat = parseFloat(location.lat);
      const lng = parseFloat(location.lng);
      if (lat && lng) {
        validLocations.push({
          lat,
          lng,
          facilityName: location.facilityName,
          newFacilityCode: location.newFacilityCode,
        });
      }
    }
  });
  return validLocations;
};
