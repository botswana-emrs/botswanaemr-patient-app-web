import Axios from 'axios';
import Cookie from 'js-cookie';
import { refreshToken } from 'lib/refreshToken';
import {
  LIST_INCIDENTS_REQUEST,
  LIST_INCIDENTS_SUCCESS,
  LIST_INCIDENTS_FAIL,
  GET_INCIDENT_REQUEST,
  GET_INCIDENT_SUCCESS,
  GET_INCIDENT_FAIL,
  CREATE_INCIDENT_REQUEST,
  CREATE_INCIDENT_SUCCESS,
  CREATE_INCIDENT_FAIL,
  APPROVE_INCIDENT_REQUEST,
  APPROVE_INCIDENT_SUCCESS,
  APPROVE_INCIDENT_FAIL,
  REJECT_INCIDENT_REQUEST,
  REJECT_INCIDENT_SUCCESS,
  REJECT_INCIDENT_FAIL,
} from '../constants/ambulanceConstants';

const accessToken = Cookie.get('userInfo');
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Axios.defaults.headers.put['Accept'] = 'application/json';
if (accessToken) {
  const parsed = JSON.parse(accessToken);
  Axios.defaults.headers.common.Authorization = `Bearer ${parsed?.access_token}`;
}

// axios interceptor for handling errors
Axios.interceptors.response.use(
  response => response,
  error => {
    if (error?.response?.status === 401) {
      const data = JSON.parse(accessToken);
      refreshToken(data?.refresh_token);
    }
    return Promise.reject(error);
  }
);

export const listIncidents = params => async dispatch => {
  dispatch({ type: LIST_INCIDENTS_REQUEST });
  try {
    const { data } = await Axios.get(`/ambulance/api/v1/incident-request/`, {
      params,
    });
    dispatch({ type: LIST_INCIDENTS_SUCCESS, payload: data?.details });
  } catch (error) {
    dispatch({
      type: LIST_INCIDENTS_FAIL,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getIncident = id => async dispatch => {
  dispatch({ type: GET_INCIDENT_REQUEST });
  try {
    const { data } = await Axios.get(
      `/ambulance/api/v1/incident-request/${id}`
    );
    dispatch({ type: GET_INCIDENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_INCIDENT_FAIL,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const createIncident = incident => async dispatch => {
  dispatch({ type: CREATE_INCIDENT_REQUEST });
  try {
    const { data } = await Axios.post(
      `/ambulance/api/v1/incident-request/create`,
      incident
    );
    dispatch({ type: CREATE_INCIDENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CREATE_INCIDENT_FAIL,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const approveIncident = datas => async dispatch => {
  dispatch({ type: APPROVE_INCIDENT_REQUEST });
  try {
    const { data } = await Axios.post(
      '/ambulance/api/v1/incident-request/approve-request',
      datas
    );
    dispatch({ type: APPROVE_INCIDENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: APPROVE_INCIDENT_FAIL,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const rejectIncident = datas => async dispatch => {
  dispatch({ type: REJECT_INCIDENT_REQUEST });
  try {
    const { data } = await Axios.post(
      '/ambulance/api/v1/incident-request/reject-request',
      datas
    );
    dispatch({ type: REJECT_INCIDENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: REJECT_INCIDENT_FAIL,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};
