import Axios from 'axios';
import Cookie from 'js-cookie';
import { refreshToken } from 'lib/refreshToken';
import {
  LIST_APPOINTMENTS_REQUEST,
  LIST_APPOINTMENTS_SUCCESS,
  LIST_APPOINTMENTS_FAILURE,
  CREATE_APPPOINTMENT_REQUEST,
  CREATE_APPPOINTMENT_SUCCESS,
  CREATE_APPPOINTMENT_FAILURE,
  CANCEL_APPOINTMENT_REQUEST,
  CANCEL_APPOINTMENT_SUCCESS,
  CANCEL_APPOINTMENT_FAILURE,
  GET_APPOINTMENT_TYPES_REQUEST,
  GET_APPOINTMENT_TYPES_SUCCESS,
  GET_APPOINTMENT_TYPES_FAILURE,
  GET_TIMESLOTS_REQUEST,
  GET_TIMESLOTS_SUCCESS,
  GET_TIMESLOTS_FAILURE,
  GET_TIMESLOT_DETAILS_REQUEST,
  GET_TIMESLOT_DETAILS_SUCCESS,
  GET_TIMESLOT_DETAILS_FAILURE,
} from '../constants/appointmentConstants';

const accessToken = Cookie.get('userInfo');
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Axios.defaults.headers.put['Accept'] = 'application/json';
if (accessToken) {
  const parsed = JSON.parse(accessToken);
  Axios.defaults.headers.common.Authorization = `Bearer ${parsed?.access_token}`;
}

// axios interceptor for handling errors
Axios.interceptors.response.use(
  response => response,
  error => {
    if (error?.response?.status === 401) {
      const data = JSON.parse(accessToken);
      refreshToken(data?.refresh_token);
    }
    return Promise.reject(error);
  }
);

export const listAppointments = params => async dispatch => {
  dispatch({ type: LIST_APPOINTMENTS_REQUEST });
  try {
    const { data } = await Axios.get(`/appointments/api/v1/my-appointments`, {
      params,
    });
    dispatch({ type: LIST_APPOINTMENTS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: LIST_APPOINTMENTS_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const createAppointment = appointment => async dispatch => {
  dispatch({ type: CREATE_APPPOINTMENT_REQUEST });
  try {
    const { data } = await Axios.post(
      `/appointments/api/v1/request-appointment`,
      appointment
    );
    dispatch({ type: CREATE_APPPOINTMENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CREATE_APPPOINTMENT_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const cancelAppointment = id => async dispatch => {
  dispatch({ type: CANCEL_APPOINTMENT_REQUEST });
  try {
    const { data } = await Axios.post(
      `/appointments/api/v1/cancel-request-appointment/?appointmentId=${id}`
    );
    dispatch({ type: CANCEL_APPOINTMENT_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CANCEL_APPOINTMENT_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getAppointmentTypes = params => async dispatch => {
  dispatch({ type: GET_APPOINTMENT_TYPES_REQUEST });
  try {
    const { data } = await Axios.get(`/appointments/api/v1/types`, {
      params,
    });
    dispatch({ type: GET_APPOINTMENT_TYPES_SUCCESS, payload: data?.results });
  } catch (error) {
    dispatch({
      type: GET_APPOINTMENT_TYPES_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getTimeslots = params => async dispatch => {
  dispatch({ type: GET_TIMESLOTS_REQUEST });
  try {
    const { data } = await Axios.get(`/appointments/api/v1/timeslots`, {
      params,
    });
    dispatch({ type: GET_TIMESLOTS_SUCCESS, payload: data?.results });
  } catch (error) {
    dispatch({
      type: GET_TIMESLOTS_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
}

export const getTimeslotDetails = id => async dispatch => {
  dispatch({ type: GET_TIMESLOT_DETAILS_REQUEST });
  try {
    const { data } = await Axios.get(`/appointments/api/v1/timeslots/${id}`);
    dispatch({ type: GET_TIMESLOT_DETAILS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_TIMESLOT_DETAILS_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
}