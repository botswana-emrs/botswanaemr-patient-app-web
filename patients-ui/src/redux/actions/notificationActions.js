import Axios from 'axios';
import Cookie from 'js-cookie';
import {
  CREATE_NOTIFICATION_REQUEST,
  CREATE_NOTIFICATION_SUCCESS,
  CREATE_NOTIFICATION_FAILURE,
  GET_NOTIFICATIONS_REQUEST,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE,
  GET_NOTIFICATION_REQUEST,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_FAILURE,
  UPDATE_NOTIFICATION_REQUEST,
  UPDATE_NOTIFICATION_SUCCESS,
  UPDATE_NOTIFICATION_FAILURE,
} from '../constants/notificationConstants';
import { refreshToken } from 'lib/refreshToken';

const accessToken = Cookie.get('userInfo');
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Axios.defaults.headers.put['Accept'] = 'application/json';
if (accessToken) {
  const parsed = JSON.parse(accessToken);
  Axios.defaults.headers.common.Authorization = `Bearer ${parsed?.access_token}`;
}

// axios interceptor for handling errors
Axios.interceptors.response.use(
  response => response,
  error => {
    if (error?.response?.status === 401) {
      const data = JSON.parse(accessToken);
      refreshToken(data?.refresh_token);
    }
    return Promise.reject(error);
  }
);

export const createNotification = notification => async dispatch => {
  dispatch({ type: CREATE_NOTIFICATION_REQUEST });
  try {
    const { data } = await Axios.post(
      `/notification/api/v1/create`,
      notification
    );
    dispatch({ type: CREATE_NOTIFICATION_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: CREATE_NOTIFICATION_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getNotifications = params => async dispatch => {
  dispatch({ type: GET_NOTIFICATIONS_REQUEST });
  try {
    if (params.all) {
      const param = { pageSize: 20, pageNumber: 1 };
      const read = await Axios.get(`/notification/api/v1/user-notification`, {
        params: { ...param, isRead: true },
      });

      const unread = await Axios.get(`/notification/api/v1/user-notification`, {
        params: { ...param, isRead: false },
      });
      const sorted = [...read.data?.details, ...unread.data?.details]?.sort(
        (a, b) => {
          return new Date(b.createdAt) - new Date(a.createdAt);
        }
      );

      return dispatch({ type: GET_NOTIFICATIONS_SUCCESS, payload: sorted });
    }
    const { data } = await Axios.get(`/notification/api/v1/user-notification`, {
      params,
    });
    return dispatch({ type: GET_NOTIFICATIONS_SUCCESS, payload: data.details });
  } catch (error) {
    return dispatch({
      type: GET_NOTIFICATIONS_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const getNotification = id => async dispatch => {
  dispatch({ type: GET_NOTIFICATION_REQUEST });
  try {
    const { data } = await Axios.get(
      `/notification/api/v1/notification-details?notificationId=${id}`
    );
    dispatch({ type: GET_NOTIFICATION_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_NOTIFICATION_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const updateNotification = id => async dispatch => {
  dispatch({ type: UPDATE_NOTIFICATION_REQUEST });
  try {
    const { data } = await Axios.put(
      `/notification/api/v1/notification-status?notificationId=${id}&status=read`
    );
    dispatch({ type: UPDATE_NOTIFICATION_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: UPDATE_NOTIFICATION_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};
