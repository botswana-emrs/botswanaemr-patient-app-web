import Axios from 'axios';
import Cookie from 'js-cookie';
import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAILURE,
  USER_UPDATE_REQUEST,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE,
  GET_USER_REQUEST,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  VERIFY_EMAIL_REQUEST,
  VERIFY_EMAIL_SUCCESS,
  VERIFY_EMAIL_FAILURE,
  RESEND_VERIFICATION_LINK_REQUEST,
  RESEND_VERIFICATION_LINK_SUCCESS,
  RESEND_VERIFICATION_LINK_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE,
} from '../constants/userConstants';

// const baseUrl = process.env.REACT_APP_SERVER_URL;

const accessToken = Cookie.get('userInfo');
Axios.defaults.headers.post['Content-Type'] = 'application/json';
Axios.defaults.headers.put['Accept'] = 'application/json';
if (accessToken) {
  const parsed = JSON.parse(accessToken);
  Axios.defaults.headers.common.Authorization = `Bearer ${parsed?.access_token}`;
}

// axios interceptor for handling errors
Axios.interceptors.response.use(
  response => response,
  error => {
    return Promise.reject(error);
  }
);

export const login = logins => async dispatch => {
  dispatch({ type: USER_LOGIN_REQUEST });
  try {
    const { data } = await Axios.post(`/auths/api/v1/login`, {
      ...logins,
    });
    dispatch({ type: USER_LOGIN_SUCCESS, payload: data });
    if (data?.access_token) {
      Cookie.set('userInfo', JSON.stringify(data));
      Axios.defaults.headers.common.Authorization = `Bearer ${data?.access_token}`;
    }
  } catch (error) {
    dispatch({
      type: USER_LOGIN_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const refreshToken = refreshToken => async dispatch => {
  try {
    const { data } = await Axios.post(`/auths/api/v1/refresh-token`, {
      refreshToken,
    });
    dispatch({ type: REFRESH_TOKEN_SUCCESS, payload: data });
    if (data?.access_token) {
      Cookie.set('userInfo', JSON.stringify(data));
    }
  } catch (error) {
    dispatch({
      type: REFRESH_TOKEN_FAILURE,
      payload: error?.response?.data?.message || error?.message,
    });
  }
};

export const verifyEmail = email => async dispatch => {
  dispatch({ type: VERIFY_EMAIL_REQUEST });
  try {
    const { data } = await Axios.post(`/auths/api/v1/verify-email`, {
      email,
    });
    dispatch({ type: VERIFY_EMAIL_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: VERIFY_EMAIL_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const resendVerificationLink = email => async dispatch => {
  dispatch({ type: RESEND_VERIFICATION_LINK_REQUEST });
  try {
    const { data } = await Axios.post(`/auths/api/v1/resend`, {
      email,
    });
    dispatch({ type: RESEND_VERIFICATION_LINK_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: RESEND_VERIFICATION_LINK_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const forgotPassword = email => async dispatch => {
  dispatch({ type: FORGOT_PASSWORD_REQUEST });
  try {
    const { data } = await Axios.post(
      `/auths/api/v1/request-password-reset`,
      email
    );
    dispatch({ type: FORGOT_PASSWORD_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: FORGOT_PASSWORD_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const resetPassword = passwords => async dispatch => {
  dispatch({ type: RESET_PASSWORD_REQUEST });
  try {
    const { data } = await Axios.post(
      `/auths/api/v1/reset-password`,
      passwords
    );
    dispatch({ type: RESET_PASSWORD_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: RESET_PASSWORD_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const register = userData => async dispatch => {
  dispatch({ type: USER_REGISTER_REQUEST });
  try {
    const { data } = await Axios.post(`/auths/api/v1/register`, userData);
    dispatch({ type: USER_REGISTER_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: USER_REGISTER_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const logout = () => dispatch => {
  Cookie.remove('userInfo');
  dispatch({ type: USER_LOGOUT });
};

export const updateUser = userData => async dispatch => {
  dispatch({ type: USER_UPDATE_REQUEST });
  try {
    const { data } = await Axios.put(`/profile`, userData);
    dispatch({ type: USER_UPDATE_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: USER_UPDATE_FAILURE,
      payload: error.response.data.message,
    });
  }
};

export const getUser = () => async dispatch => {
  dispatch({ type: GET_USER_REQUEST });
  try {
    const { data } = await Axios.get(`/auths/api/v1/user/details`);
    dispatch({ type: GET_USER_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: GET_USER_FAILURE,
      payload: error.response.data.message,
    });
  }
};
