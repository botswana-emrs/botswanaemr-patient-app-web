import {
  LIST_INCIDENTS_REQUEST,
  LIST_INCIDENTS_SUCCESS,
  LIST_INCIDENTS_FAIL,
  GET_INCIDENT_REQUEST,
  GET_INCIDENT_SUCCESS,
  GET_INCIDENT_FAIL,
  CREATE_INCIDENT_REQUEST,
  CREATE_INCIDENT_SUCCESS,
  CREATE_INCIDENT_FAIL,
  APPROVE_INCIDENT_REQUEST,
  APPROVE_INCIDENT_SUCCESS,
  APPROVE_INCIDENT_FAIL,
  REJECT_INCIDENT_REQUEST,
  REJECT_INCIDENT_SUCCESS,
  REJECT_INCIDENT_FAIL,
} from '../constants/ambulanceConstants';

export const ambulanceListReducer = (state = { incidents: [] }, action) => {
  switch (action.type) {
    case LIST_INCIDENTS_REQUEST:
      return { loading: true, incidents: [] };
    case LIST_INCIDENTS_SUCCESS:
      return { loading: false, incidents: action.payload };
    case LIST_INCIDENTS_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const ambulanceGetReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_INCIDENT_REQUEST:
      return { loading: true, ...state };
    case GET_INCIDENT_SUCCESS:
      return { loading: false, incident: action.payload };
    case GET_INCIDENT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const ambulanceCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_INCIDENT_REQUEST:
      return { loading: true };
    case CREATE_INCIDENT_SUCCESS:
      return { loading: false, success: true };
    case CREATE_INCIDENT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const ambulanceApproveReducer = (state = {}, action) => {
  switch (action.type) {
    case APPROVE_INCIDENT_REQUEST:
      return { loading: true };
    case APPROVE_INCIDENT_SUCCESS:
      return { loading: false, success: true };
    case APPROVE_INCIDENT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const ambulanceRejectReducer = (state = {}, action) => {
  switch (action.type) {
    case REJECT_INCIDENT_REQUEST:
      return { loading: true };
    case REJECT_INCIDENT_SUCCESS:
      return { loading: false, success: true };
    case REJECT_INCIDENT_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
