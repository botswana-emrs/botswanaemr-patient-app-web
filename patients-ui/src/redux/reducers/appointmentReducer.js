import {
  LIST_APPOINTMENTS_REQUEST,
  LIST_APPOINTMENTS_SUCCESS,
  LIST_APPOINTMENTS_FAILURE,
  CREATE_APPPOINTMENT_REQUEST,
  CREATE_APPPOINTMENT_SUCCESS,
  CREATE_APPPOINTMENT_FAILURE,
  CANCEL_APPOINTMENT_REQUEST,
  CANCEL_APPOINTMENT_SUCCESS,
  CANCEL_APPOINTMENT_FAILURE,
  GET_APPOINTMENT_TYPES_REQUEST,
  GET_APPOINTMENT_TYPES_SUCCESS,
  GET_APPOINTMENT_TYPES_FAILURE,
  GET_TIMESLOTS_REQUEST,
  GET_TIMESLOTS_SUCCESS,
  GET_TIMESLOTS_FAILURE,
  GET_TIMESLOT_DETAILS_REQUEST,
  GET_TIMESLOT_DETAILS_SUCCESS,
  GET_TIMESLOT_DETAILS_FAILURE,
} from '../constants/appointmentConstants';

export const listAppointmentsReducer = (
  state = { appointments: [] },
  action
) => {
  switch (action.type) {
    case LIST_APPOINTMENTS_REQUEST:
      return { loading: true };
    case LIST_APPOINTMENTS_SUCCESS:
      return { loading: false, appointments: action.payload };
    case LIST_APPOINTMENTS_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const createAppointmentReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_APPPOINTMENT_REQUEST:
      return { loading: true };
    case CREATE_APPPOINTMENT_SUCCESS:
      return { loading: false, success: true };
    case CREATE_APPPOINTMENT_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const cancelAppointmentReducer = (state = {}, action) => {
  switch (action.type) {
    case CANCEL_APPOINTMENT_REQUEST:
      return { loading: true };
    case CANCEL_APPOINTMENT_SUCCESS:
      return { loading: false, success: true };
    case CANCEL_APPOINTMENT_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getAppointmentTypesReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_APPOINTMENT_TYPES_REQUEST:
      return { loading: true };
    case GET_APPOINTMENT_TYPES_SUCCESS:
      return { loading: false, appointmentTypes: action.payload };
    case GET_APPOINTMENT_TYPES_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getTimeslotsReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_TIMESLOTS_REQUEST:
      return { loading: true };
    case GET_TIMESLOTS_SUCCESS:
      return { loading: false, timeslots: action.payload };
    case GET_TIMESLOTS_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getTimeslotDetailsReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_TIMESLOT_DETAILS_REQUEST:
      return { loading: true };
    case GET_TIMESLOT_DETAILS_SUCCESS:
      return { loading: false, timeslot: action.payload };
    case GET_TIMESLOT_DETAILS_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
