import {
  GET_FACILITIES_REQUEST,
  GET_FACILITIES_SUCCESS,
  GET_FACILITIES_FAILURE,
  GET_FACILITY_REQUEST,
  GET_FACILITY_SUCCESS,
  GET_FACILITY_FAILURE,
  GET_DHMT_REQUEST,
  GET_DHMT_SUCCESS,
  GET_DHMT_FAILURE,
  GET_FACILITY_TYPE_REQUEST,
  GET_FACILITY_TYPE_SUCCESS,
  GET_FACILITY_TYPE_FAILURE,
  GET_FACILITY_OWNER_REQUEST,
  GET_FACILITY_OWNER_SUCCESS,
  GET_FACILITY_OWNER_FAILURE,
  GET_DISTRICT_REQUEST,
  GET_DISTRICT_SUCCESS,
  GET_DISTRICT_FAILURE,
  GET_FACILITY_CONSTITUENCY_REQUEST,
  GET_FACILITY_CONSTITUENCY_SUCCESS,
  GET_FACILITY_CONSTITUENCY_FAILURE,
} from '../constants/facilityContants';

export const getFacilitiesReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FACILITIES_REQUEST:
      return {
        loading: true,
      };
    case GET_FACILITIES_SUCCESS:
      return {
        loading: false,
        facilities: action.payload,
      };
    case GET_FACILITIES_FAILURE:
      return {
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export const getFacilityReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FACILITY_REQUEST:
      return { loading: true };
    case GET_FACILITY_SUCCESS:
      return { loading: false, facility: action.payload };
    case GET_FACILITY_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getDhmtReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_DHMT_REQUEST:
      return { loading: true };
    case GET_DHMT_SUCCESS:
      return { loading: false, dhmt: action.payload };
    case GET_DHMT_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getFacilityTypeReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FACILITY_TYPE_REQUEST:
      return { loading: true };
    case GET_FACILITY_TYPE_SUCCESS:
      return { loading: false, facilityType: action.payload };
    case GET_FACILITY_TYPE_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getFacilityOwnerReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FACILITY_OWNER_REQUEST:
      return { loading: true };
    case GET_FACILITY_OWNER_SUCCESS:
      return { loading: false, facilityOwner: action.payload };
    case GET_FACILITY_OWNER_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getDistrictReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_DISTRICT_REQUEST:
      return { loading: true };
    case GET_DISTRICT_SUCCESS:
      return { loading: false, district: action.payload };
    case GET_DISTRICT_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const getFacilityConstituencyReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_FACILITY_CONSTITUENCY_REQUEST:
      return { loading: true };
    case GET_FACILITY_CONSTITUENCY_SUCCESS:
      return { loading: false, facilityConstituency: action.payload };
    case GET_FACILITY_CONSTITUENCY_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};
