import { combineReducers } from 'redux';
import {
  userLoginReducer,
  userRegisterReducer,
  userUpdateReducer,
  getUserReducer,
  verifyEmailReducer,
  resendVerificationLinkReducer,
  forgotPasswordReducer,
  resetPasswordReducer,
} from './userReducer';
import {
  getFacilitiesReducer,
  getFacilityReducer,
  getDhmtReducer,
  getDistrictReducer,
  getFacilityConstituencyReducer,
  getFacilityOwnerReducer,
  getFacilityTypeReducer,
} from './facilityReducer';
import {
  createNotificationReducer,
  getNotificationsReducer,
  getNotificationReducer,
  updateNotificationReducer,
} from './notificationReducer';
import {
  listAppointmentsReducer,
  createAppointmentReducer,
  cancelAppointmentReducer,
  getAppointmentTypesReducer,
  getTimeslotsReducer,
  getTimeslotDetailsReducer,
} from './appointmentReducer';
import {
  ambulanceListReducer,
  ambulanceGetReducer,
  ambulanceCreateReducer,
  ambulanceApproveReducer,
  ambulanceRejectReducer,
} from './ambulanceReducer';

const reducer = combineReducers({
  userSignin: userLoginReducer,
  userSignup: userRegisterReducer,
  userUpdate: userUpdateReducer,
  getUser: getUserReducer,
  verifyEmail: verifyEmailReducer,
  resendVerificationLink: resendVerificationLinkReducer,
  forgotPassword: forgotPasswordReducer,
  resetPassword: resetPasswordReducer,
  getFacilities: getFacilitiesReducer,
  getFacility: getFacilityReducer,
  getDhmt: getDhmtReducer,
  getDistrict: getDistrictReducer,
  getFacilityConstituency: getFacilityConstituencyReducer,
  getFacilityOwner: getFacilityOwnerReducer,
  getFacilityType: getFacilityTypeReducer,
  createNotification: createNotificationReducer,
  getNotifications: getNotificationsReducer,
  getNotification: getNotificationReducer,
  updateNotification: updateNotificationReducer,
  listAppointments: listAppointmentsReducer,
  createAppointment: createAppointmentReducer,
  cancelAppointment: cancelAppointmentReducer,
  getAppointmentTypes: getAppointmentTypesReducer,
  getTimeslots: getTimeslotsReducer,
  getTimeslotDetails: getTimeslotDetailsReducer,
  listAmbulance: ambulanceListReducer,
  getAmbulance: ambulanceGetReducer,
  createAmbulance: ambulanceCreateReducer,
  approveAmbulance: ambulanceApproveReducer,
  rejectAmbulance: ambulanceRejectReducer,
});

export default reducer;
