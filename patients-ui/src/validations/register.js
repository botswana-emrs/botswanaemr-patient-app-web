const personal = ['firstName', 'lastName', 'dateOfBirth', 'gender'];
const contact = [
  'emailAddress',
  'phoneNumber',
  'patientIdentificationNo',
  'identificationType',
  'nationalPassportNo',
];
const setup = ['username', 'password', 'confirmPassword'];

const validatePersonal = inputs => {
  const errors = {};
  personal.forEach(field => {
    if (!inputs[field]) {
      errors[field] = 'This field is required!';
    }
  });
  return errors;
};

const validateContact = inputs => {
  const errors = {};
  contact.forEach(field => {
    if (!inputs[field]) {
      errors[field] = 'This field is required!';
    }
    if (
      field === 'emailAddress' &&
      Object.keys(inputs).includes('emailAddress')
    ) {
      // validate email using regular expression
      const emailRegex =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!emailRegex.test(inputs['emailAddress'])) {
        errors['emailAddress'] = 'Invalid email address!';
      }
    }
  });
  return errors;
};

const validateSetup = inputs => {
  const errors = {};
  setup.forEach(field => {
    if (!inputs[field]) {
      errors[field] = 'This field is required!';
    }

    if (Object.keys(inputs).includes('password')) {
      if (inputs.password.length < 6) {
        errors.password = 'Password must be at least 6 characters long';
      }
    }
    //   validate confirm password
    if (Object.keys(inputs).includes('confirmPassword')) {
      if (inputs['confirmPassword'] !== inputs.password) {
        errors['confirmPassword'] = 'Passwords do not match!';
      }
    }
  });
  return errors;
};

const validateAll = (inputs, index) => {
  if (index === 0) {
    return validatePersonal(inputs);
  }
  if (index === 1) {
    return validateContact(inputs);
  }
  if (index === 2) {
    return validateSetup(inputs);
  }
};

export default validateAll;
