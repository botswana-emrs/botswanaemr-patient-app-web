/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Urbanist', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        light: {
          DEFAULT: '#F8F9FA',
          300: '#E2E6EA',
          100: '#F4F7FF',
        },
        dark: {
          100: '#FFDE9E',
          DEFAULT: '#FFD074',
        },
        blue: {
          light: '#3COFF',
          DEFAULT: '#16C2FB',
          dark: '#0099CC',
        },
        lblue: {
          100: '#40D0FF12',
          200: '#A2E8FF',
          light: '#C3E3F1',
          DEFAULT: '#6FC4E1',
          dark: '#007d82',
        },
        dblue: {
          light: '#3c6381',
          DEFAULT: '#007095',
          dark: '#05875',
        },
        white: {
          DEFAULT: '#FFFFFF',
        },
        green: {
          50: '#e8f5e9',
          100: '#c8e6c9',
          200: '#a5d6a7',
          300: '#81c784',
          400: '#66bb6a',
          500: '#4caf50',
          600: '#43a047',
          700: '#388e3c',
          800: '#2e7d32',
          900: '#1b5e20',
          light: '#C4FICE',
          DEFAULT: '#218838',
        },
        indigo: {
          light: '#757de8',
          DEFAULT: '#3f51b5',
          dark: '#002984',
        },
        gray: {
          50: '#fafafa',
          100: '#f5f5f5',
          200: '#eeeeee',
          300: '#e0e0e0',
          400: '#bdbdbd',
          500: '#9e9e9e',
          600: '#757575',
          DEFAULT: '#616161',
        },

        red: {
          50: '#FFEBEE',
          400: '#ff7961',
          light: '#F6CDD1',
          DEFAULT: '#C82333',
          700: '#D32F2F',
          800: '#C62828',
          dark: '#ba000d',
        },

        yellow: {
          light: '#FFDE9E',
          DEFAULT: '#FFD074',
          dark: '#9F6E0F',
          300: '#E2AE49',
          400: '#CA9122',
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
};
